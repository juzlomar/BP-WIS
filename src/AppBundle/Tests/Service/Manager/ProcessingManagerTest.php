<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Service\Manager;


use AppBundle\Entity\Form;
use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialPart;
use AppBundle\Entity\PartProcessing;
use AppBundle\Entity\Processing;
use AppBundle\Entity\Warehouse;
use AppBundle\Entity\WayOfProcessing;
use AppBundle\Repository\FormRepository;
use AppBundle\Repository\MaterialRepository;
use AppBundle\Repository\ProcessingRepository;
use AppBundle\Repository\WayOfProcessingRepository;
use AppBundle\Service\Functionality\FormFunctionality;
use AppBundle\Service\Functionality\MaterialFunctionality;
use AppBundle\Service\Functionality\ProcessingFunctionality;
use AppBundle\Service\Manager\ProcessingManager;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\FormError;

class ProcessingManagerTest extends \PHPUnit_Framework_TestCase
{
    /** @var ProcessingManager */
    private $processingManager;
    private function createProcessingFunctionality(){
        $functinality = $this
            ->getMockBuilder(ProcessingFunctionality::class)
            ->disableOriginalConstructor()
            ->getMock();
        return $functinality;
    }

    private function createFormFunctionality(){
        $functinality = $this
            ->getMockBuilder(FormFunctionality::class)
            ->disableOriginalConstructor()
            ->getMock();
        return $functinality;
    }

    private function createMaterialFunctionality(){
        $functinality = $this
            ->getMockBuilder(MaterialFunctionality::class)
            ->disableOriginalConstructor()
            ->getMock();
        return $functinality;
    }

    private function createMaterialRepository(){
        $repository = $this
            ->getMockBuilder(MaterialRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        return $repository;
    }

    private function createFormRepository(){
        $repository = $this
            ->getMockBuilder(FormRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        return $repository;
    }

    private function createWayOfProcessingRepository(){
        $repository = $this
            ->getMockBuilder(WayOfProcessingRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        return $repository;
    }

    private function createProcessingRepository(){
        $repository = $this
            ->getMockBuilder(ProcessingRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        return $repository;
    }

    private function createForm(){
        $form = $this
            ->getMockBuilder(\Symfony\Component\Form\Form::class)
            ->disableOriginalConstructor()
            ->getMock();
        return $form;
    }

    public function validateProvider(){
        $partForm = $this->createForm();
        $inputForm = $this->createForm();
        $inputForm->expects($this->exactly(2))
            ->method('get')
            ->with('part')
            ->will($this->returnValue($partForm));
        $inputForm->expects($this->exactly(1))
            ->method('getData')
            ->will($this->returnValue(new PartProcessing()));

        $outputForm = $this->createForm();
        $processingForm = $this->createForm();
        $processingForm->expects($this->at(0))
            ->method('getData')
            ->will($this->returnValue(new Processing()));
        $processing = new Processing();
        $warehouse = new Warehouse();
        $processing->setWarehouse($warehouse);
        $wayOfProcessing = new WayOfProcessing();
        $wayOfProcessing->setOutputForm(new Form());
        $processing->setWayOfProcessing($wayOfProcessing);
        $processingForm->expects($this->at(1))
            ->method('getData')
            ->will($this->returnValue($processing));
        $processingForm->expects($this->at(2))
            ->method('get')
            ->with('inputs')
            ->will($this->returnValue(array($inputForm, $inputForm)));
        $processingForm->expects($this->at(3))
            ->method('get')
            ->with('outputs')
            ->will($this->returnValue(array($outputForm)));
        return array(
            array($processingForm, $inputForm, $partForm, $outputForm, $processing)
        );
    }

    /**
     * @dataProvider validateProvider
     */
    public function testValidate($processingForm, $inputForm, $partForm, $outputForm, $processing){
        $processingFunctionality = $this->createProcessingFunctionality();
        $processingFunctionality->expects($this->at(0))
            ->method('findPartToProcess')
            ->with($partForm,$processing->getWarehouse(), $processing->getWayOfProcessing()->getEntranceForms(), 'připraveno ke zpracování')
            ->will($this->returnValue(null));
        $processingFunctionality->expects($this->at(1))
            ->method('findPartToProcess')
            ->with($partForm,$processing->getWarehouse(), $processing->getWayOfProcessing()->getEntranceForms(), 'připraveno ke zpracování')
            ->will($this->returnValue(new MaterialPart()));
        $processingFunctionality->expects($this->exactly(1))
            ->method('checkMaterials')
            ->with($processing->getInputs(), $processingForm);
        $processingFunctionality->expects($this->exactly(1))
            ->method('checkOutput')
            ->with($outputForm, $processing->getWayOfProcessing()->getOutputForm());
        $this->processingManager = new processingManager(
            $this->createFormFunctionality(),
            $this->createMaterialFunctionality(),
            $processingFunctionality,
            $this->createFormRepository(),
            $this->createMaterialRepository(),
            $this->createProcessingRepository(),
            $this->createWayOfProcessingRepository(),
            array('processing' => 'připraveno ke zpracování')
        );
        $this->assertEmpty($this->processingManager->validate($processingForm));
        $this->processingManager->validate($processingForm);
    }

    public function saveProvider(){
        $part1 = new MaterialPart();
        $part2 = new MaterialPart();
        $part3 = new MaterialPart();
        $material1 = new Material();
        $material1->setId(1);
        $material2 = new Material();
        $material2->setId(2);
        $part1->setMaterial($material1);
        $part2->setMaterial($material2);
        $part3->setMaterial($material2);

        $partProcessing1 = new PartProcessing();
        $partProcessing1->setPart($part1);
        $partProcessing2 = new PartProcessing();
        $partProcessing2->setPart($part2);
        $partProcessing3 = new PartProcessing();
        $partProcessing3->setPart($part3);

        $part4 = new MaterialPart();
        $processing = new Processing();
        $processing->addInput($partProcessing1)->addInput($partProcessing2)->addInput($partProcessing3);
        $processing->addOutput($part4);

        $newMaterial = new Material();
         return array(
             array($processing, $newMaterial)
         );
    }

    /**
     * @dataProvider saveProvider
     */
    public function testSave(Processing $processing, Material $newMaterial){
        $materialFunctionality = $this->createMaterialFunctionality();
        $materialFunctionality->expects($this->exactly(1))
            ->method('merge')
            ->will($this->returnValue($newMaterial));
        $materialFunctionality->expects($this->exactly(1))
            ->method('createPart')
            ->with($newMaterial, $processing->getOutputs()->first(), $processing)
            ->will($this->returnValue(new MaterialPart()));
        $materialFunctionality->expects($this->exactly(3))
            ->method('processPart');

        $processingRepository = $this->createProcessingRepository();
        $processingRepository->expects($this->exactly(1))
            ->method('save')
            ->with($processing);

        $this->processingManager = new processingManager(
            $this->createFormFunctionality(),
            $materialFunctionality,
            $this->createProcessingFunctionality(),
            $this->createFormRepository(),
            $this->createMaterialRepository(),
            $processingRepository,
            $this->createWayOfProcessingRepository(),
            array('processing' => 'připraveno ke zpracování')
        );
        $this->processingManager->save($processing);
    }

    public function validateWayProvider(){
        $form1 = new Form();
        $form2 = new Form();
        $form3 = new Form();
        $form3->addFinalForm($form3)->addFinalForm($form1)->addFinalForm($form2);

        $processing1 = new WayOfProcessing();
        $processing1->addEntranceForm($form1);
        $processing1->setOutputForm($form2);
        $form1->addWayOfProcessing($processing1);
        $form2->addOutputOfProcessing($processing1);

        return array(
            array($form1, $form2, $form3, new Form())
        );
    }

    /**
     * @dataProvider validateWayProvider
     */
    public function testValidateWay(Form $form1, Form $form2, Form $form3, Form $form4){
        $forms = new ArrayCollection();
        $forms->add($form1);
        $forms->add($form2);
        $forms->add($form3);
        $forms->add($form4);

        $formForm = $this->createForm();
        $formForm->expects($this->exactly(1))
            ->method('addError')
            ->with(new FormError('Nesprávné vstupní a výstupní podoby.'));

        $formRepository = $this->createFormRepository();
        $formRepository->expects($this->exactly(1))
            ->method('findAll')
            ->will($this->returnValue($forms));
        $formFunctionality = $this->createFormFunctionality();
        $formFunctionality->expects($this->at(0))
            ->method('setFinalForms')
            ->with($form1)
            ->will($this->returnCallback(function($form1){
                $form1->addFinalForm($form1);
                $form2 = $form1->getWayOfProcessing()->first()->getOutputForm();
                $form2->addFinalForm($form2);
                $form1->addFinalForm($form2);
                return $form1;
            }));
        $formFunctionality->expects($this->at(1))
            ->method('setFinalForms')
            ->with($form3)
            ->will($this->returnCallback(function($form3){
                $form3->addFinalForm($form3);
                return $form3;
            }));

        $formFunctionality->expects($this->at(2))
            ->method('setFinalForms')
            ->with($form4)
            ->will($this->returnValue(null));

        $this->processingManager = new processingManager(
            $formFunctionality,
            $this->createMaterialFunctionality(),
            $this->createProcessingFunctionality(),
            $formRepository,
            $this->createMaterialRepository(),
            $this->createProcessingRepository(),
            $this->createWayOfProcessingRepository(),
            array('processing' => 'připraveno ke zpracování')
        );

        $this->processingManager->validateWay($formForm);

    }
}
