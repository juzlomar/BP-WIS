<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Service\Manager;


use AppBundle\Entity\Bringing;
use AppBundle\Entity\Business;
use AppBundle\Entity\Form;
use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialPart;
use AppBundle\Entity\Transfer;
use AppBundle\Entity\Warehouse;
use AppBundle\Entity\WayOfStoring;
use AppBundle\Repository\MaterialRepository;
use AppBundle\Repository\TransportRepository;
use AppBundle\Service\Functionality\MaterialFunctionality;
use AppBundle\Service\Functionality\FormFunctionality;
use AppBundle\Service\Functionality\TransportFunctionality;
use AppBundle\Service\Manager\TransportManager;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\FormError;

class TransportManagerTest extends \PHPUnit_Framework_TestCase
{
    /** @var  TransportManager */
    private $transportManager;

    private function createTransportFunctionality(){
        $functinality = $this
            ->getMockBuilder(TransportFunctionality::class)
            ->disableOriginalConstructor()
            ->getMock();
        return $functinality;
    }

    private function createFormFunctionality(){
        $functinality = $this
            ->getMockBuilder(FormFunctionality::class)
            ->disableOriginalConstructor()
            ->getMock();
        return $functinality;
    }

    private function createMaterialFunctionality(){
        $functinality = $this
            ->getMockBuilder(MaterialFunctionality::class)
            ->disableOriginalConstructor()
            ->getMock();
        return $functinality;
    }

    private function createMaterialRepository(){
        $repository = $this
            ->getMockBuilder(MaterialRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        return $repository;
    }

    private function createTransportRepository(){
        $repository = $this
            ->getMockBuilder(TransportRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        return $repository;
    }


    private function createForm(){
        $form = $this
            ->getMockBuilder(\Symfony\Component\Form\Form::class)
            ->disableOriginalConstructor()
            ->getMock();
        return $form;
    }

    private function getMaterials(){
        $material1 = new Material();
        $materials = new ArrayCollection();
        $materials->add($material1);
        return $materials;
    }


   public function testFindMaterial(){
       $warehouse = new Warehouse();
       $materials = $this->getMaterials();
       $materialRepository = $this->createMaterialRepository();
       $materialRepository->expects($this->exactly(2))
           ->method('findByWarehousesStates')
           ->with($warehouse, 'připraveno k expedici')
           ->will($this->returnValue($materials));
       $materialFunctionality = $this->createMaterialFunctionality();
       $materialFunctionality->expects($this->exactly(1))
           ->method('findMaterialsToTransfer')
           ->with($materials)
           ->will($this->returnValue($materials));
        $this->transportManager = new TransportManager(
            $this->createFormFunctionality(),
            $materialFunctionality,
            $this->createTransportFunctionality(),
            $materialRepository,
            $this->createTransportRepository(),
            array('expedition' => 'připraveno k expedici')
        );
        $this->assertEquals($materials, $this->transportManager->findMaterialsToInternalTransport($warehouse));
        $this->assertEquals($materials, $this->transportManager->findMaterialsToTransfer($warehouse));
   }


    public function validateTransferProvider()
    {
        $transport = new Transfer();
        $warehouse1 = new Warehouse();
        $transport->setFrom($warehouse1);
        $business1 = new Business();
        $material1 = new Material();
        $business1->addMaterial($material1);
        $material2 = new Material();
        $part2 = new MaterialPart();
        $material2->addPart($part2);

        $transportForm = $this->createForm();
        $businessForm = $this->createForm();
        $materialForm = $this->createForm();
        $partForm = $this->createForm();
        $partForms = array($partForm);
        $materialForm->expects($this->exactly(2))
            ->method('get')
            ->with('parts')
            ->will($this->returnValue($partForms));
        $materialForm->expects($this->exactly(2))
            ->method('getData')
            ->will($this->returnValue($material1));
        $materialForms = array($materialForm);

        $businessForm->expects($this->exactly(2))
            ->method('getData')
            ->will($this->returnValue($business1));
        $businessForm->expects($this->exactly(3))
            ->method('get')
            ->with('materials')
            ->will($this->returnValue($materialForms));
        $businessForms = array($businessForm);

        $transportForm->expects($this->exactly(6))
            ->method('getData')
            ->will($this->returnValue($transport));
        $transportForm->expects($this->exactly(3))
            ->method('get')
            ->with('materials')
            ->will($this->returnValue($businessForms));
        return array(
            array($transportForm, $materialForm, $partForm, $warehouse1, $material2, $part2)
        );
    }

    /**
     * @dataProvider validateTransferProvider
     */
    public function testValidateTransfer($transportForm, $materialForm, $partForm, $warehouse1, $material2, $part2){
        $transportFunctionality = $this->createTransportFunctionality();
        $transportFunctionality->expects($this->at(0))
            ->method('checkMaterials')
            ->with($warehouse1, $materialForm)
            ->will($this->returnValue(null));
        $transportFunctionality->expects($this->at(1))
            ->method('checkMaterials')
            ->with($warehouse1, $materialForm)
            ->will($this->returnValue($material2));
        $transportFunctionality->expects($this->at(2))
            ->method('checkPart')
            ->with($partForm, $material2, 'transfer', 'připraveno k expedici')
            ->will($this->returnValue($part2));
        $transportFunctionality->expects($this->at(3))
            ->method('checkMaterials')
            ->with($warehouse1, $materialForm)
            ->will($this->returnValue($material2));
        $transportFunctionality->expects($this->at(4))
            ->method('checkPart')
            ->with($partForm, $material2, 'transfer', 'připraveno k expedici')
            ->will($this->returnValue(null));
        $materialFunctionality = $this->createMaterialFunctionality();
        $materialFunctionality->expects($this->exactly(1))
            ->method('divide')
            ->with($material2, array())
            ->will($this->returnValue($material2));
        $this->transportManager = new TransportManager(
            $this->createFormFunctionality(),
            $materialFunctionality,
            $transportFunctionality,
            $this->createMaterialRepository(),
            $this->createTransportRepository(),
            array('expedition' => 'připraveno k expedici')
        );
        $this->transportManager->validate($transportForm);
        $this->transportManager->validate($transportForm);
        $this->transportManager->validate($transportForm);
    }

    public function validateBringingProvider()
    {
        $transport = new Bringing();
        $business1 = new Business();
        $material1 = new Material();
        $business1->addMaterial($material1);
        $part1 = new MaterialPart();
        $material1->addPart($part1);
        $form = new Form();
        $form->setTitle('Drt');
        $wayOfStoring = new WayOfStoring();
        $wayOfStoring->setTitle('V bagu');


        $formForm = $this->createForm();
        $formForm->expects($this->exactly(4))
            ->method('getData')
            ->will($this->returnValue($form));
        $formForm->expects($this->exactly(1))
            ->method('addError')
            ->with(new FormError('Podoba Drt se nedá zpracovat na Drt.'));


        $storingForm = $this->createForm();
        $storingForm->expects($this->exactly(2))
            ->method('getData')
            ->will($this->returnValue($wayOfStoring));
        $storingForm->expects($this->exactly(1))
            ->method('addError')
            ->with(new FormError('Podoba Drt se nedá uskladnit způsobem V bagu.'));

        $partForm = $this->createForm();
        $partForm->expects($this->exactly(2))
            ->method('getData')
            ->will($this->returnValue($part1));
        $partForm->expects($this->exactly(3))
            ->method('get')
            ->with('wayOfStoring')
            ->will($this->returnValue($storingForm));
        $partForms = array($partForm);

        $materialForm = $this->createForm();
        $materialForm->expects($this->at(3))
            ->method('get')
            ->with('parts')
            ->will($this->returnValue($partForms));
        $materialForm->expects($this->at(6))
            ->method('get')
            ->with('parts')
            ->will($this->returnValue($partForms));
        $materialForm->expects($this->at(1))
            ->method('get')
            ->with('currentForm')
            ->will($this->returnValue($formForm));
        $materialForm->expects($this->at(2))
            ->method('get')
            ->with('currentForm')
            ->will($this->returnValue($formForm));
        $materialForm->expects($this->at(5))
            ->method('get')
            ->with('currentForm')
            ->will($this->returnValue($formForm));
        $materialForm->expects($this->at(0))
            ->method('get')
            ->with('finalForm')
            ->will($this->returnValue($formForm));
        $materialForm->expects($this->at(4))
            ->method('get')
            ->with('finalForm')
            ->will($this->returnValue($formForm));
        $materialForms = array($materialForm);

        $businessForm = $this->createForm();
        $businessForm->expects($this->exactly(2))
            ->method('get')
            ->with('materials')
            ->will($this->returnValue($materialForms));
        $businessForms = array($businessForm);

        $transportForm = $this->createForm();
        $transportForm->expects($this->exactly(2))
            ->method('getData')
            ->will($this->returnValue($transport));
        $transportForm->expects($this->exactly(2))
            ->method('get')
            ->with('materials')
            ->will($this->returnValue($businessForms));
        return array(
            array($transportForm, $form, $wayOfStoring)
        );
    }

    /**
     * @dataProvider validateBringingProvider
     */
    public function testValidateBringing($transportForm, $form, $wayOfStoring){
        $formFunctionality = $this->createFormFunctionality();
        $formFunctionality->expects($this->at(0))
            ->method('canBeFinal')
            ->with($form, $form)
            ->will($this->returnValue(false));
        $formFunctionality->expects($this->at(1))
            ->method('canBeStored')
            ->with($form, $wayOfStoring)
            ->will($this->returnValue(false));
        $formFunctionality->expects($this->at(2))
            ->method('canBeFinal')
            ->with($form, $form)
            ->will($this->returnValue(true));
        $formFunctionality->expects($this->at(3))
            ->method('canBeStored')
            ->with($form, $wayOfStoring)
            ->will($this->returnValue(true));

        $this->transportManager = new TransportManager(
            $formFunctionality,
            $this->createMaterialFunctionality(),
            $this->createTransportFunctionality(),
            $this->createMaterialRepository(),
            $this->createTransportRepository(),
            array('expedition' => 'připraveno k expedici')
        );
        $this->transportManager->validate($transportForm);
        $this->transportManager->validate($transportForm);
    }


}
