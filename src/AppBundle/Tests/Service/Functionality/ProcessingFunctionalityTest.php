<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Service\Functionality;


use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialPart;
use AppBundle\Entity\PartProcessing;
use AppBundle\Entity\Processing;
use AppBundle\Entity\Warehouse;
use AppBundle\Entity\WayOfProcessing;
use AppBundle\Entity\WayOfStoring;
use AppBundle\Repository\MaterialPartRepository;
use AppBundle\Service\Functionality\FormFunctionality;
use AppBundle\Service\Functionality\MaterialFunctionality;
use AppBundle\Service\Functionality\ProcessingFunctionality;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;

class ProcessingFunctionalityTest extends \PHPUnit_Framework_TestCase
{
    /** @var  ProcessingFunctionality */
    private $processingFunctionality;

    private function createFormFunctionality(){
        $formFunctionality = $this
            ->getMockBuilder(FormFunctionality::class)
            ->disableOriginalConstructor()
            ->getMock();
        return $formFunctionality;
    }

    private function createMaterialFunctionality(){
        $materialFunctionality = $this
            ->getMockBuilder(MaterialFunctionality::class)
            ->disableOriginalConstructor()
            ->getMock();
        return $materialFunctionality;
    }

    private function createPartRepository(){
        $partRepository = $this
            ->getMockBuilder(MaterialPartRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        return $partRepository;
    }

    private function createForm($data = null, $error = null){
        $form = $this
            ->getMockBuilder(Form::class)
            ->disableOriginalConstructor()
            ->getMock();
        if ($data){
            $form->expects($this->exactly(1))
                ->method('getData')
                ->will($this->returnValue($data));
        }
        if ($error){
            $form->expects($this->exactly(1))
                ->method('addError')
                ->with($this->equalTo(new FormError($error)));
        }else{
            $form->expects($this->exactly(0))
                ->method('addError');
        }

        return $form;
    }

    private function getProcessings(){
        $warehouse1 = new Warehouse();
        $way1 = new WayOfProcessing();
        $processing1 = new Processing();
        $processing1->setDate(new \DateTime('2015-01-01'));
        $processing1->setWarehouse($warehouse1);
        $processing1->setWayOfProcessing($way1);
        $processing2 = new Processing();
        $processing2->setDate(new \DateTime('2016-01-01'));
        $processing2->setWarehouse($warehouse1);
        $processing1->setWayOfProcessing(new WayOfProcessing());
        $processing3 = new Processing();
        $processing3->setDate(new \DateTime('2014-01-01'));
        $processing3->setWarehouse(new Warehouse());
        $processing1->setWayOfProcessing($way1);
        $processings = new ArrayCollection();
        $processings->add($processing1);
        $processings->add($processing2);
        $processings->add($processing3);
        return $processings;
    }

    private function getProcessing(){
        $material1 = new Material();
        $material1->setId(1);
        $material2 = new Material();
        $material2->setId(2);
        $part1 = new MaterialPart();
        $part1->setMaterial($material1);
        $part2 = new MaterialPart();
        $part2->setMaterial($material2);
        $partProcessing1 = new PartProcessing();
        $partProcessing1->setPart($part1);
        $partProcessing2 = new PartProcessing();
        $partProcessing2->setPart($part2);
        $processing = new Processing();
        $processing->addInput($partProcessing1)->addInput($partProcessing2);
        return $processing;
    }

    private function getPart(){
        $form1 = new \AppBundle\Entity\Form();
        $form1->setTitle('Drt');
        $storing1 = new WayOfStoring();
        $storing1->setTitle('V bagu');
        $warehouse1 = new Warehouse();
        $material1 = new Material();
        $material1->setWarehouse($warehouse1);
        $part1 = new MaterialPart();
        $part1->setId(1);
        $part1->setCurrentForm($form1);
        $part1->setWayOfStoring($storing1);
        $part1->setMaterial($material1);
        $part1->setState('připraveno ke zpracování');
        return $part1;
    }

    public function setUp(){
        $this->processingFunctionality = new ProcessingFunctionality($this->createFormFunctionality(), $this->createMaterialFunctionality(), $this->createPartRepository());
    }

    public function testCheckMaterials(){
        $processing = $this->getProcessing();
        $material1 = $processing->getInputs()->first()->getPart()->getMaterial();
        $material2 = $processing->getInputs()->get(1)->getPart()->getMaterial();

        $materialFunctionality = $this->createMaterialFunctionality();
        $materialFunctionality->expects($this->exactly(1))
            ->method('canBeMerged')
            ->with($this->equalTo($material1), $this->equalTo($material2))
            ->will($this->returnValue(false));
        $form = $this->createForm(null, 'Materiály s ID ' . $material1->getId() . ' a ' . $material2->getId() . ' spolu nemohou být zpracovány');
        $this->processingFunctionality = new ProcessingFunctionality($this->createFormFunctionality(), $materialFunctionality, $this->createPartRepository());
        $this->processingFunctionality->checkMaterials($processing->getInputs(), $form);
    }


    public function testFindPartToProcess(){
        $form2 = new \AppBundle\Entity\Form();
        $warehouse2 = new Warehouse();
        $warehouse2->setName('Plzen');
        $part1 = $this->getPart();

        $partRepository = $this->createPartRepository();
        $partRepository->expects($this->exactly(1))
            ->method('findById')
            ->will($this->throwException(new \Doctrine\ORM\NoResultException));
        $form = $this->createForm($part1, 'Část s tímto neexistuje');
        $forms = new ArrayCollection();
        $forms->add($form2);
        $this->processingFunctionality = new ProcessingFunctionality($this->createFormFunctionality(), $this->createMaterialFunctionality(), $partRepository);
        $this->assertEquals(null, $this->processingFunctionality->findPartToProcess($form, $warehouse2, $forms, 'připraveno ke zpracování'));

        $partRepository = $this->createPartRepository();
        $partRepository->expects($this->exactly(3))
            ->method('findById')
            ->will($this->returnValue($part1));
        $form = $this->createForm(null, 'Část není ve vhodné formě na tento způsob zpracování');
        $formPart = $this->createForm($part1);
        $formPart
            ->method('get')
            ->with('currentForm')
            ->will($this->returnValue($form));
        $this->processingFunctionality = new ProcessingFunctionality($this->createFormFunctionality(), $this->createMaterialFunctionality(), $partRepository);
        $this->assertEquals($part1, $this->processingFunctionality->findPartToProcess($formPart, $part1->getMaterial()->getWarehouse(), $forms, 'připraveno ke zpracování'));

        $forms->add($part1->getCurrentForm());
        $form = $this->createForm(null, 'Část není ve stavu ke zpracování');
        $formPart = $this->createForm($part1, 'Část není na provozovně '.$warehouse2->getName());
        $formPart
            ->method('get')
            ->with('state')
            ->will($this->returnValue($form));
        $this->assertEquals($part1, $this->processingFunctionality->findPartToProcess($formPart, $warehouse2, $forms, 'zastaveno'));
        $form = $this->createForm($part1);
        $this->assertEquals($part1, $this->processingFunctionality->findPartToProcess($form, $part1->getMaterial()->getWarehouse(), $forms, 'připraveno ke zpracování'));
    }

    public function testCheckOutput(){
        $part1 = $this->getPart();


        $formFunctionality = $this->createFormFunctionality();
        $formFunctionality->expects($this->exactly(1))
            ->method('canBeStored')
            ->with($part1->getCurrentForm(), $part1->getWayOfStoring())
            ->will($this->returnValue(false));
        $form = $this->createForm(null, 'Podoba ' . $part1->getCurrentForm()->getTitle() . ' se nedá uskladnit způsobem ' . $part1->getWayOfStoring()->getTitle() . '.');
        $formPart = $this->createForm($part1);
        $formPart
            ->method('get')
            ->with('wayOfStoring')
            ->will($this->returnValue($form));
        $this->processingFunctionality = new ProcessingFunctionality($formFunctionality, $this->createMaterialFunctionality(), $this->createPartRepository());
        $this->processingFunctionality->checkOutput($formPart, $part1->getCurrentForm());

        $formFunctionality = $this->createFormFunctionality();
        $formFunctionality->expects($this->exactly(1))
            ->method('canBeStored')
            ->with($part1->getCurrentForm(), $part1->getWayOfStoring())
            ->will($this->returnValue(true));
        $formPart = $this->createForm($part1);
        $this->processingFunctionality = new ProcessingFunctionality($formFunctionality, $this->createMaterialFunctionality(), $this->createPartRepository());
        $this->processingFunctionality->checkOutput($formPart, $part1->getCurrentForm());
    }
    
    public function testCheckDates(){
        $processings = $this->getProcessings();
        $this->assertEquals(1, $this->processingFunctionality->checkDates($processings, new \DateTime('2014-12-01'), new \DateTime('2015-12-01'))->count());
    }

    public function testGetMaterials(){
        $processing1 = $this->getProcessing();
        $part3 = new MaterialPart();
        $part3->setMaterial($processing1->getInputs()->first()->getPart()->getMaterial());
        $partProcessing3 = new PartProcessing();
        $partProcessing3->setPart($part3);
        $processing1->addInput($partProcessing3);
        $this->assertEquals(2, count($this->processingFunctionality->getMaterials($processing1)));
    }



    public function testFindByWarehouseWayOfProcessing(){
        $processings = $this->getProcessings();
        $this->assertEquals(1, $this->processingFunctionality->findByWarehouseWayOfProcessing(
            $processings,
            $processings->first()->getWarehouse(),
            $processings->first()->getWayOfProcessing())->count());
    }

}
