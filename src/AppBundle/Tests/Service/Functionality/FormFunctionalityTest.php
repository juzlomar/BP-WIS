<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Service\Functionality;


use AppBundle\Entity\Form;
use AppBundle\Entity\Processing;
use AppBundle\Entity\WayOfProcessing;
use AppBundle\Entity\WayOfStoring;
use AppBundle\Service\Functionality\FormFunctionality;


class FormFunctionalityTest extends \PHPUnit_Framework_TestCase
{
    /** @var  FormFunctionality */
    private $formFunctionality;

    public function setUp(){
        $this->formFunctionality = new FormFunctionality();
    }

    private function getForms(){
        $form1 = new Form();
        $form1->addFinalForm($form1);

        $form2 = new Form();
        $form2->addFinalForm($form2);

        $form3 = new Form();
        $form3->addFinalForm($form3);

        $form1->addFinalForm($form2);
        $form3->addFinalForm($form1);
        $form3->addFinalForm($form2);

        $storing = new WayOfStoring();
        $form1->addWayOfStoring($storing);
        $form2->addWayOfStoring(new WayOfStoring());


    }

    public function testCanBeFinal(){
        $form1 = new Form();
        $form1->addFinalForm($form1);
        $form2 = new Form();
        $this->assertFalse($this->formFunctionality->canBeFinal($form1, $form2));
        $form1->addFinalForm($form2);
        $this->assertTrue($this->formFunctionality->canBeFinal($form1, $form2));
    }

    public function testCanBeStored(){
        $form1 = new Form();
        $storing = new WayOfStoring();
        $form1->addWayOfStoring($storing);
        $this->assertFalse($this->formFunctionality->canBeStored($form1, new WayOfStoring()));
        $this->assertTrue($this->formFunctionality->canBeStored($form1, $storing));
    }

    public function testSetFinalForm(){
        $form1 = new Form();
        $form2 = new Form();
        $form3 = new Form;
        $processing1 = new WayOfProcessing();
        $processing1->addEntranceForm($form1);
        $form1->addWayOfProcessing($processing1);
        $processing1->setOutputForm($form2);
        $form2->addOutputOfProcessing($processing1);
        $processing2 = new WayOfProcessing();
        $processing2->addEntranceForm($form2);
        $form2->addWayOfProcessing($processing2);
        $processing2->setOutputForm($form3);
        $form3->addOutputOfProcessing($processing2);

        $result = $this->formFunctionality->setFinalForms($form1);
        $this->assertEquals(3, $form1->getFinalForms()->count());
        $this->assertEquals(2, $form2->getFinalForms()->count());
    }
}
