<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Service\Functionality;


use AppBundle\Entity\Bringing;
use AppBundle\Entity\BusinessPartner;
use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialPart;
use AppBundle\Entity\Transfer;
use AppBundle\Entity\Warehouse;
use AppBundle\Repository\MaterialRepository;
use AppBundle\Service\Functionality\TransportFunctionality;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;

class TransportFunctionalityTest extends  \PHPUnit_Framework_TestCase
{
    /** @var  TransportFunctionality */
    private $transportFunctionality;

    public function setUp(){
        $materialRepository = $this->createRepository();
        $this->transportFunctionality = new TransportFunctionality($materialRepository);
    }

    private function createForm($data, $error = null, $dataCall = 1){
        $form = $this
            ->getMockBuilder(Form::class)
            ->disableOriginalConstructor()
            ->getMock();
        $form->expects($this->exactly($dataCall))
            ->method('getData')
            ->will($this->returnValue($data));
        if ($error){
            $form->expects($this->exactly(1))
                ->method('addError')
                ->with($this->equalTo(new FormError($error)));
        }else{
            $form->expects($this->exactly(0))
                ->method('addError');
        }

        return $form;
    }

    public function createRepository($error = false, $material = null){
        $materialRepository = $this
            ->getMockBuilder(MaterialRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        if ($material){
            $materialRepository->expects($this->exactly(2))
                ->method('findCurrentById')
                ->will($this->returnValue($material));
        }elseif($error){
            $materialRepository->expects($this->once())
                ->method('findCurrentById')
                ->will($this->throwException(new \Doctrine\ORM\NoResultException));
        }
        return $materialRepository;
    }

    public function testCheckMaterial(){
        $material = new Material();
        $material->setId(1);
        $warehouse1 = new Warehouse();
        $warehouse1->setId(1);
        $warehouse1->setName('Plzen');
        $warehouse2 = new Warehouse();
        $warehouse2->setId(2);
        $warehouse2->setName('Praha');
        $material->setWarehouse($warehouse1);

        $materialRepository = $this->createRepository(true);
        $form = $this->createForm($material, 'Materiál s tímto ID neexistuje');
        $this->transportFunctionality = new TransportFunctionality($materialRepository);
        $this->assertEquals(null, $this->transportFunctionality->checkMaterials($warehouse2, $form));

        $materialRepository = $this->createRepository(false, $material);
        $form = $this->createForm($material, 'Materiál není na provozovně '.$warehouse2->getName());
        $this->transportFunctionality = new TransportFunctionality($materialRepository);
        $this->assertEquals($material, $this->transportFunctionality->checkMaterials($warehouse2, $form));

        $form = $this->createForm($material);
        $this->assertEquals($material, $this->transportFunctionality->checkMaterials($warehouse1, $form));
    }

    public function testCheckPart(){
        $material = new Material();
        $material->setId(1);
        $form1 = new \AppBundle\Entity\Form();
        $form1->setTitle('Regranulat');
        $form2 = new \AppBundle\Entity\Form();
        $form2->setTitle('Drt');
        $material->setFinalForm($form1);
        $part1 = new MaterialPart();
        $part1->setId(1);
        $part2 = new MaterialPart();
        $part2->setId(2);
        $material->addPart($part1);
        $form = $this->createForm($part2, 'Materiál s ID '.$material->getId().' nemá část s tímto ID');
        $this->assertEquals(null, $this->transportFunctionality->checkPart($form, $material, 'transfer', 'připraveno k expedici'));
        $part1->setState('zastaveno');
        $part1->setCurrentForm($form1);
        $form = $this->createForm($part1, 'Část není ve stavu k převozu', 2);
        $this->assertEquals($part1, $this->transportFunctionality->checkPart($form, $material, 'transfer', 'připraveno k expedici'));
        $part1->setState('připraveno k expedici');
        $part1->setCurrentForm($form2);
        $form = $this->createForm($part1, 'Část není ve finální podobě', 2);
        $this->assertEquals($part1, $this->transportFunctionality->checkPart($form, $material, 'transfer', 'připraveno k expedici'));
        $part1->setCurrentForm($form1);
        $form = $this->createForm($part1, null, 2);
        $this->assertEquals($part1, $this->transportFunctionality->checkPart($form, $material, 'transfer', 'připraveno k expedici'));
        $form = $this->createForm($part1, null, 3);
        $this->assertEquals($part1, $this->transportFunctionality->checkPart($form, $material, 'internal_transport', 'připraveno k expedici'));
    }

    public function testCheckDates(){
        $transport1 = new Bringing();
        $transport1->setDate(new \DateTime('2015-01-01'));
        $transport2 = new Bringing();
        $transport2->setDate(new \DateTime('2016-01-01'));
        $transport3 = new Bringing();
        $transport3->setDate(new \DateTime('2014-01-01'));
        $transports = new ArrayCollection();
        $transports->add($transport1);
        $transports->add($transport2);
        $transports->add($transport3);
        $this->assertEquals(1, $this->transportFunctionality->checkDates($transports, new \DateTime('2014-12-01'), new \DateTime('2015-12-01'))->count());
    }


    public function testFindByFromTo(){
        $partner1 = new BusinessPartner();
        $partner2 = new BusinessPartner();
        $warehouse1 = new Warehouse();
        $warehouse2 = new Warehouse();
        $transport1 = new Bringing();
        $transport1->setFrom($partner1);
        $transport1->setTo($warehouse1);
        $transport2 = new Bringing();
        $transport2->setFrom($partner2);
        $transport2->setTo($warehouse1);
        $transport3 = new Bringing();
        $transport3->setFrom($partner1);
        $transport3->setTo($warehouse2);
        $transport4 = new Transfer();
        $transport4->setFrom($warehouse1);
        $transport4->setTo($partner2);
        $transports = new ArrayCollection();
        $transports->add($transport1);
        $transports->add($transport2);
        $transports->add($transport3);
        $transports->add($transport4);
        $this->assertEquals(1, $this->transportFunctionality->findByFromTo($transports, $partner1, $warehouse1)->count());
    }
}
