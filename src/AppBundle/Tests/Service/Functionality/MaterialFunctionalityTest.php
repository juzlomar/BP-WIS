<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Service\Functionality;


use AppBundle\Entity\Bringing;
use AppBundle\Entity\Business;
use AppBundle\Entity\BusinessPartner;
use AppBundle\Entity\Form;
use AppBundle\Entity\InternalTransport;
use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialPart;
use AppBundle\Entity\PartProcessing;
use AppBundle\Entity\Processing;
use AppBundle\Entity\Transfer;
use AppBundle\Entity\Warehouse;
use AppBundle\Repository\MaterialPartRepository;
use AppBundle\Repository\MaterialRepository;
use AppBundle\Service\Functionality\MaterialFunctionality;
use Doctrine\Common\Collections\ArrayCollection;

class MaterialFunctionalityTest extends \PHPUnit_Framework_TestCase
{
    /** @var  MaterialFunctionality */
    protected $materialFunctionality;

    private function createRepository(){
        $materialRepository = $this
            ->getMockBuilder(MaterialRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        return $materialRepository;
    }

    private function createPartRepository(){
        $partRepository = $this
            ->getMockBuilder(MaterialPartRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        return $partRepository;
    }

    public function setUp(){
        $this->materialFunctionality = new MaterialFunctionality($this->createRepository(), $this->createPartRepository(), array('transferred' => 'odvezeno'));
    }

    private function getMaterials(){
        $form1 = new Form();
        $form2 = new Form();
        $part1 = new MaterialPart();
        $part1->setCurrentForm($form1);
        $part1->setWeightNetto(100);
        $part1->setWeightBrutto(200);
        $part2 = new MaterialPart();
        $part2->setCurrentForm($form2);
        $part2->setWeightNetto(100);
        $part3 = new MaterialPart();
        $part3->setCurrentForm($form1);
        $part3->setWeightNetto(100);

        $material1 = new Material();
        $material1->setFinalForm($form1);
        $material1->setPlastics('PP');
        $material1->setSpecification('Bedynky');
        $material1->setColor('modra');
        $material1->setSpecificationEng('Boxes');
        $material1->setColorEng('blue');
        $material1->addPart($part1)->addPart($part2);

        $material2 = new Material();
        $material2->setFinalForm($form2);
        $material2->setPlastics('PP');
        $material2->setSpecification('Bedynky');
        $material2->setColor('cervena');
        $material2->setSpecificationEng('Boxes');
        $material2->setColorEng('red');
        $material2->addPart($part3);

        $materials = new ArrayCollection();
        $materials->add($material1);
        $materials->add($material2);
        return $materials;
    }

    public function testFindMaterialsToTransfer(){
        $result = $this->materialFunctionality->findMaterialsToTransfer($this->getMaterials());
        $this->assertEquals(1, $result->count());
        $this->assertEquals(1, $result->first()->getParts()->count());
    }



    public function testTransportMaterial(){
        $warehouse = new Warehouse();
        $bringing = new Bringing();
        $bringing->setTo($warehouse);
        $material = $this->getMaterials()->first();

        $materialRepository = $this->createRepository();
        $materialRepository->expects($this->exactly(2))
            ->method('persist')
            ->with($material);
        $this->materialFunctionality = new MaterialFunctionality($materialRepository, $this->createPartRepository(), array('transferred' => 'odvezeno'));
        $this->materialFunctionality->transportMaterial($material, $bringing);
        $this->assertEquals($warehouse, $material->getWarehouse());
        $this->assertEquals(200, $material->getParts()->first()->getWeightBrutto());
        $this->assertEquals(100, $material->getParts()->get(1)->getWeightBrutto());

        $material->setWarehouse(null);
        $transfer = new Transfer();
        $business1 = new Business();
        $business1->setTransport($bringing);
        $business2 = new Business();
        $business2->setTransport($transfer);
        $material->addBusiness($business1)->addBusiness($business2);
        $this->materialFunctionality->transportMaterial($material, $transfer);
        $this->assertEquals(null, $material->getWarehouse());
        $this->assertEquals('odvezeno', $material->getParts()->first()->getState());
        $this->assertEquals(1, $business2->getMaterials()->count());
        $this->assertEquals(2, $business1->getMaterials()->count());
        $material->setId(1);
        $this->materialFunctionality->transportMaterial($material, $transfer);
    }

    public function testCanBeMarged(){
        $materials = $this->getMaterials();
        $this->assertFalse($this->materialFunctionality->canBeMerged($materials->get(0), $materials->get(1)));
        $materials->get(0)->setFinalForm($materials->get(1)->getFinalForm());
        $this->assertTrue($this->materialFunctionality->canBeMerged($materials->get(0), $materials->get(1)));
    }

    public function testMerge(){
        $materials = $this->getMaterials();
        $materialRepository = $this->createRepository();
        $materialRepository->expects($this->exactly(1))
            ->method('persist');
        $this->materialFunctionality = new MaterialFunctionality($materialRepository, $this->createPartRepository(), array('transferred' => 'odvezeno'));
        $newMaterial = $this->materialFunctionality->merge($materials->toArray());
        $this->assertEquals('mix', $newMaterial->getColor());
        $this->assertEquals($newMaterial->getFinalForm(), $materials->get(0)->getFinalForm());
    }

    public function testDivide(){
        $material = $this->getMaterials()->first();
        $newMaterial = $this->materialFunctionality->divide($material, array($material->getParts()->first()));
        $this->assertEquals(1, $material->getParts()->count());
        $this->assertEquals(1, $newMaterial->getParts()->count());
        $this->assertEquals($material->getFinalForm(), $newMaterial->getFinalForm());
    }

    public function testCreatePart(){
        $material = new Material();
        $part = new MaterialPart();
        $part->setWeightNetto(100);
        $processing = new Processing();
        $partRepository = $this->createPartRepository();
        $partRepository->expects($this->once())
            ->method('persist')
            ->with($part);
        $this->materialFunctionality = new MaterialFunctionality($this->createRepository(), $partRepository, array('transferred' => 'odvezeno'));
        $this->materialFunctionality->createPart($material, $part, $processing);
        $this->assertEquals(1, $material->getParts()->count());
        $this->assertEquals($processing, $part->getOutputOfProcessing());
        $this->assertEquals(100, $part->getWeightBrutto());
    }

    public function testFindMaterialByPartners(){
        $partner1 = new BusinessPartner();
        $bringing = new Bringing();
        $bringing->setDate(new \DateTime('2015-01-01'));
        $bringing->setFrom($partner1);
        $transfer = new Transfer();
        $transfer->setDate(new \DateTime('2016-01-01'));
        $transfer->setTo($partner1);
        $business1 = new Business();
        $business1->setTransport($bringing);
        $business1->setMaterialPrice(10);
        $business2 = new Business();
        $business2->setTransport($transfer);
        $business2->setMaterialPrice(20);
        $part = new MaterialPart();
        $part->setState('odvezeno');
        $material = new Material();
        $material->addPart($part);
        $material->addBusiness($business1)->addBusiness($business2);
        $materials = new ArrayCollection();
        $materials->add($material);
        $partners = new ArrayCollection();
        $partners->add($partner1);
        $result = $this->materialFunctionality->findMaterialByPartners($materials, $partners, $partners);
        $this->assertEquals(1, count($result));
        $this->assertEquals(20, $result[0]['transport']['last_transport_prize']);

        $transfer->setTo(new BusinessPartner());
        $part->setState('připraveno k expedici');
        $this->assertEquals(1, count($this->materialFunctionality->findMaterialByPartners($materials, $partners, $partners)));

        $bringing->setFrom(new BusinessPartner());
        $this->assertEquals(0, count($this->materialFunctionality->findMaterialByPartners($materials, $partners, $partners)));
    }

    public function testFindByForms(){
        $materials = $this->getMaterials();
        $materials->add(new Material());
        $forms = new ArrayCollection();
        $forms->add($materials->get(0)->getFinalForm());
        $result = $this->materialFunctionality->findByForms($materials, $forms);
        $this->assertEquals(2, $result->count());
        $this->assertEquals(1, $result->get(0)->getParts()->count());
    }

    public function testFindByFinalForms(){
        $materials = $this->getMaterials();
        $forms = new ArrayCollection();
        $forms->add($materials->get(0)->getFinalForm());
        $result = $this->materialFunctionality->findByFinalForms($materials, $forms);
        $this->assertEquals(1, $result->count());
    }

    private function getTransportMaterials($warehouse1){
        $transport1 = new InternalTransport();
        $transport1->setTo($warehouse1);
        $transport1->setFrom($warehouse1);
        $transport2 = new InternalTransport();
        $transport2->setTo(new Warehouse());
        $transport2->setFrom(new Warehouse());
        $business1 = new Business();
        $business1->setTransport($transport1);
        $business2 = new Business();
        $business2->setTransport($transport2);

        $material1 = new Material();
        $material1->addBusiness($business1);
        $material1->addBusiness($business2);
        $material2 = new Material();
        $material2->addBusiness($business2);

        $materials = new ArrayCollection();
        $materials->add($material1);
        $materials->add($material2);
        return $materials;
    }

    public function testFindByWarehouseTo(){
        $warehouse1 = new Warehouse();
        $materials = $this->getTransportMaterials($warehouse1);
        $result = $this->materialFunctionality->findByWarehouseFrom($materials, $warehouse1);
        $this->assertEquals(1, count($result));
        $this->assertEquals(1, $result->first()->getBusinesses()->count());
    }

    public function testFindByWarehouseFrom(){
        $warehouse1 = new Warehouse();
        $materials = $this->getTransportMaterials($warehouse1);
        $result = $this->materialFunctionality->findByWarehouseTo($materials, $warehouse1);
        $this->assertEquals(1, count($result));
        $this->assertEquals(1, $result->first()->getBusinesses()->count());
    }

    private function getProcessingMaterial(){
        $processing1 = new Processing();
        $processing1->setDate(new \DateTime('2016-01-01'));
        $partProcessing1 = new PartProcessing();
        $partProcessing1->setProcessing($processing1);
        $processing2 = new Processing();
        $processing2->setDate(new \DateTime('2014-01-01'));
        $partProcessing2 = new PartProcessing();
        $partProcessing2->setProcessing($processing2);

        $part1 = new MaterialPart();
        $part1->setInputOfProcessing($partProcessing2);
        $part2 = new MaterialPart();
        $part2->setOutputOfProcessing($processing2);
        $part2->setInputOfProcessing($partProcessing1);
        $part3 = new MaterialPart();
        $part3->setOutputOfProcessing($processing1);

        $material1 = new Material();
        $material1->addPart($part1)->addPart($part2)->addPart($part3);

        return $material1;
    }

    public function testFindPartsByDate(){
        $material = $this->getProcessingMaterial();
        $part = $material->getParts()->get(1);
        $result = $this->materialFunctionality->findPartsByDate($material, new \DateTime('2015-01-01'));
        $this->assertEquals(1, $result->getParts()->count());
        $this->assertEquals($part, $result->getParts()->first());
    }

    public function testSortCz(){
        $materials = $this->getMaterials();
        $plastics = $this->materialFunctionality->sortByPlastics($materials);
        $this->assertEquals(1, count($plastics));
        $this->assertEquals(2, count($plastics['PP']));
        $specification = $this->materialFunctionality->sortBySpecification($plastics['PP']);
        $this->assertEquals(1, count($specification));
        $this->assertEquals(2, count($specification['Bedynky']));
        $colors = $this->materialFunctionality->sortByColor($specification['Bedynky']);
        $this->assertEquals(2, count($colors));
        $this->assertEquals(1, count($colors['cervena']));
    }

    public function testSortEng(){
        $materials = $this->getMaterials();
        $plastics = $this->materialFunctionality->sortByPlastics($materials);
        $this->assertEquals(1, count($plastics));
        $this->assertEquals(2, count($plastics['PP']));
        $specification = $this->materialFunctionality->sortBySpecificationEng($plastics['PP']);
        $this->assertEquals(1, count($specification));
        $this->assertEquals(2, count($specification['Boxes']));
        $colors = $this->materialFunctionality->sortByColorEng($specification['Boxes']);
        $this->assertEquals(2, count($colors));
        $this->assertEquals(1, count($colors['red']));
    }

    public function testCountWeight(){
        $materials = $this->getMaterials();
        $this->assertEquals(200, $this->materialFunctionality->countWeights($materials->first()));
    }


}
