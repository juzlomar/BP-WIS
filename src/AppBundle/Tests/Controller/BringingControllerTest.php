<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Controller;


use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\FunctionalTestBundle\Test\WebTestCase;

class BringingControllerTest extends WebTestCase
{
    /** @var  ReferenceRepository */
    private $fixtures;

    public function setUp(){
        $this->fixtures = $this->loadFixtures(array(
            'AppBundle\DataFixtures\ORM\Test\LoadUserData',
            'AppBundle\DataFixtures\ORM\Test\LoadWayOfStoringData',
            'AppBundle\DataFixtures\ORM\Test\LoadFormData',
            'AppBundle\DataFixtures\ORM\Test\LoadWayOfProcessingData',
            'AppBundle\DataFixtures\ORM\Test\LoadAddressData',
            'AppBundle\DataFixtures\ORM\Test\LoadWarehouseData',
            'AppBundle\DataFixtures\ORM\Test\LoadBusinessPartnerData',
            'AppBundle\DataFixtures\ORM\Test\LoadMaterialData',
            'AppBundle\DataFixtures\ORM\Test\LoadPartData',
            'AppBundle\DataFixtures\ORM\Test\LoadBringingData',
        ))->getReferenceRepository();
        $this->loginAs($this->fixtures->getReference('marketa'), 'main');
    }



    private function getMaterial()
    {
        $materialArray = array(
            'plastics' => 'PP',
            'specification' => 'Bedynky',
            'specification_eng' => 'Boxes',
            'color' => 'mix',
            'color_eng' => 'mix',
            'finalForm' => 'Drt',
            'currentForm' => 'Drt',
            'parts' => array()
        );

        $partArray = array(
            'number' => 1,
            'weightNetto' => 800,
            'weightBrutto' => 821,
            'wayOfStoring' => 'v bagu',
            'state' => 'připraveno ke zpracování',
            'note' => '',
        );
        array_push($materialArray['parts'], $partArray);

        $businessArray = array(
            'materialPrice' => '30',
            'materials' => array(
                $materialArray,
            ));
        return $businessArray;
    }

    private function merge($materials, $form){
        $values = array_merge_recursive(
            $form->getPhpValues(),
            array(
                'bringing' => array(
                    'materials' => $materials
                )
            )
        );
        return $values;
    }

    public function testNewGet()
    {
        $client = static::makeClient();
        $crawler = $client->request('GET', '/navoz/novy');

        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertEquals(1, $crawler->filter('form')->count());
        $this->assertEquals(1, $crawler->filter('form[name="bringing"]')->count());
        $this->assertEquals(1, $crawler->filter('input[name="bringing[price]"]')->count());
        $this->assertEquals(1, $crawler->filter('input[name="bringing[date]"]')->count());
        $this->assertEquals(1, $crawler->filter('select[name="bringing[from]"]')->count());
        $this->assertEquals(1, $crawler->filter('select[name="bringing[to]"]')->count());
        $this->assertEquals(1, $crawler->filter('button[type="submit"]')->count());
    }


    public function testNewInvalid()
    {
        $client = static::makeClient();
        $crawler = $client->request('GET', '/navoz/novy');
        $form = $crawler->selectButton('bringing_save')->form();
        $form['bringing[price]'] = '-1000';
        $form['bringing[from]'] = $this->fixtures->getReference('Dura Blatna')->getId();
        $values = $this->merge(array(), $form);
        $values['bringing']['to'] = '3';
        $crawler = $client->request($form->getMethod(), $form->getUri(), $values, $form->getPhpFiles());

        $this->assertContains('Přeprava musí obsahovat alespoň jeden materiál', $client->getResponse()->getContent());
        $this->assertContains('Cena dopravy musí být větší než nula.', $crawler->filter('#bringing_price')->parents()->parents()->html());
        $this->assertContains('Neexistující, nebo neaktivní provozovna.', $crawler->filter('#bringing_to')->parents()->parents()->html());

        $form['bringing[price]'] = '';
        $businessArray = $this->getMaterial();
        $businessArray['materials'][0]['currentForm'] = 'Regranulat';
        $businessArray['materials'][0]['parts'][0]['wayOfStoring'] = 'volne';
        $businessArray['materials'][0]['parts'][0]['weightBrutto'] = 700;
        $businessArray['materials'][0]['parts'][0]['state'] = 'odvezeno';

        $values = $this->merge(array($businessArray), $form);
        $crawler = $client->request($form->getMethod(), $form->getUri(), $values, $form->getPhpFiles());
        $this->assertContains('Přeprava musí mít cenu dopravy.', $crawler->filter('#bringing_price')->parents()->parents()->html());
        $this->assertContains('Podoba Regranulat se nedá uskladnit způsobem volne.', $client->getResponse()->getContent());
        $this->assertContains('Podoba Regranulat se nedá zpracovat na Drt.', $client->getResponse()->getContent());
        $this->assertContains('Část nemůže být v tomto stavu.', $crawler->filter('#bringing_materials_0_materials_0_parts_0_state')->parents()->parents()->html());
    }

    public function testNewValid(){
        $client = static::makeClient();
        $crawler = $client->request('GET', '/navoz/novy');
        $form = $crawler->selectButton('bringing_save')->form();
        $form['bringing[price]'] = '1000';
        $form['bringing[from]'] = $this->fixtures->getReference('Dura Blatna')->getId();
        $form['bringing[to]'] = '1';
        $businessArray = $this->getMaterial();

        $values = $this->merge(array($businessArray), $form);
        $crawler = $client->request($form->getMethod(), $form->getUri(), $values, $form->getPhpFiles());
        $crawler = $client->followRedirect();
        $this->assertContains('Návoz uložen', $client->getResponse()->getContent());
    }

    public function testShowGet(){
        $client = static::makeClient();
        $crawler = $client->request('GET', '/navoz');

        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertEquals(1, $crawler->filter('form')->count());
        $this->assertEquals(1, $crawler->filter('form[name="bringing_filter"]')->count());
        $this->assertEquals(1, $crawler->filter('input[name="bringing_filter[start_date]"]')->count());
        $this->assertEquals(1, $crawler->filter('select[name="bringing_filter[from]"]')->count());
        $this->assertEquals(1, $crawler->filter('select[name="bringing_filter[to]"]')->count());
        $this->assertEquals(1, $crawler->filter('button[type="submit"]')->count());
    }


    public function testShowPost(){
        $client = static::makeClient();
        $crawler = $client->request('GET', '/navoz');
        $formButton = $crawler->selectButton('bringing_filter_show');
        $form = $formButton->form();
        $form['bringing_filter[start_date]'] = '01.01.2015';
        $form['bringing_filter[end_date]'] = '01.02.2015';
        $form['bringing_filter[from]'] = $this->fixtures->getReference('Dura Blatna')->getId();
        $form['bringing_filter[to]'] = '2';
        $crawler = $client->submit($form);

        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertEquals(1, $crawler->filter('form')->count());
        $this->assertEquals(1, $crawler->filter('table')->count());
        $this->assertContains("1000", $crawler->filter('table')->parents()->html());
        $this->assertContains("Dura Blatna", $crawler->filter('table')->parents()->html());
        $this->assertContains("Praha - Letnany", $crawler->filter('table')->html());
        $this->assertGreaterThan(0, $crawler->filter('table')->filter('a')->count());
    }
}
