<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Controller;


use AppBundle\Entity\MaterialPart;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\FunctionalTestBundle\Test\WebTestCase;


class ProcessingControllerTest extends WebTestCase
{
    /** @var  ReferenceRepository */
    private $fixtures;

    public function setUp(){
        $this->fixtures = $this->loadFixtures(array(
            'AppBundle\DataFixtures\ORM\Test\LoadWayOfStoringData',
            'AppBundle\DataFixtures\ORM\Test\LoadFormData',
            'AppBundle\DataFixtures\ORM\Test\LoadWayOfProcessingData',
            'AppBundle\DataFixtures\ORM\Test\LoadAddressData',
            'AppBundle\DataFixtures\ORM\Test\LoadWarehouseData',
            'AppBundle\DataFixtures\ORM\Test\LoadBusinessPartnerData',
            'AppBundle\DataFixtures\ORM\Test\LoadMaterialData',
            'AppBundle\DataFixtures\ORM\Test\LoadPartData',
            'AppBundle\DataFixtures\ORM\Test\LoadUserData',
            'AppBundle\DataFixtures\ORM\Test\LoadShowMaterialData',
            'AppBundle\DataFixtures\ORM\Test\LoadShowProcessingData',
        ))->getReferenceRepository();
        $this->loginAs($this->fixtures->getReference('marketa'), 'main');
    }

    private function getInputs($parts){
        $inputs = array();

        foreach($parts as $part){
            $partArray = $this->getPart($part);
            $partArray['state'] = 'připraveno k expedici';
            $partProcessing = array(
                'price' => '40',
                'note' => 'zpracovavam',
                'part' => $partArray,
            );
            array_push($inputs, $partProcessing);
        }
        return $inputs;
    }

    private function getOutputs($parts){
        $outputs = array();

        foreach($parts as $part){
            $partArray = $this->getPart($part);
            unset($partArray['id']);
            unset($partArray['currentForm']);
            $partArray['state'] = 'připraveno k expedici';
            array_push($outputs, $partArray);
        }
        return $outputs;
    }

    private function merge($inputs, $outputs, $form){
        $values = array_merge_recursive(
            $form->getPhpValues(),
            array(
                'processing' => array(
                    'wayOfProcessing' => 'Regranulace',
                    'inputs' => $inputs,
                    'outputs' => $outputs
                )
            )
        );
        return $values;
    }

    private function getPart(MaterialPart $part){
        $partArray = array(
            'id' => $part->getId(),
            'number' => $part->getNumber(),
            'weightNetto' => $part->getWeightNetto(),
            'weightBrutto' => $part->getWeightBrutto(),
            'currentForm' => $part->getCurrentForm()->getTitle(),
            'wayOfStoring' => $part->getWayOfStoring()->getTitle(),
            'state' => $part->getState(),
            'note' => $part->getNote(),
        );
        return $partArray;
    }

    public function testNewGet(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/zpracovani/novy');
        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertEquals(1, $crawler->filter('form[name="processing"]')->count(), 'Jmeno formulare neni processing');
        $this->assertEquals(1, $crawler->filter('input[name="processing[date]"]')->count());
        $this->assertEquals(1, $crawler->filter('select[name="processing[warehouse]"]')->count());
        $this->assertEquals(1, $crawler->filter('select[name="processing[wayOfProcessing]"]')->count());
        $this->assertEquals(1, $crawler->filter('select[name="processing[processor]"]')->count());
        $this->assertEquals(1, $crawler->filter('button[type="submit"]')->count());
        $this->assertEquals(1, $crawler->filter('button#processing_choose')->count());
        $this->assertEquals(0, $crawler->filter('.content')->filter('li')->count());
    }

    public function testMaterials(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/zpracovani/novy');
        $form = $crawler->selectButton('processing_choose')->form();
        $form['processing[warehouse]'] = '1';
        $values = $this->merge(array(), array(), $form);
        $crawler = $client->request('POST', '/zpracovani/material', $values, array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ));
        $this->assertTrue($client->getResponse()->isSuccessful(), 'Neuspech ajax zadosti');
        $this->assertContains('připraveno ke zpracování', $client->getResponse()->getContent());
    }

    public function testNewInvalid(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/zpracovani/novy');
        $form = $crawler->selectButton('processing_save')->form();
        $form['processing[warehouse]'] = '2';
        $values = $this->merge(array(), array(), $form);
        $crawler = $client->request($form->getMethod(), $form->getUri(), $values, $form->getPhpFiles());

        $this->assertContains('Na provozovně nemůže být materiál takto zpracován', $client->getResponse()->getContent());
        $this->assertContains('Zpracování musí obsahovat alespoň jednu část', $client->getResponse()->getContent());
        $this->assertContains('Zpracovaním musí vzniknout alespoň jedna část', $client->getResponse()->getContent());

        $part = $this->fixtures->getReference('part-divide-expedice');
        $inputs = $this->getInputs(array($part, $part));
        $inputs[0]['part']['id'] = $part->getId() + 100;
        $inputs[1]['part']['number'] = 100;
        $outputs =  $this->getOutputs(array($part));
        $outputs[0]['wayOfStoring'] = 'volne';
        $values = $this->merge($inputs, $outputs, $form);
        $values['processing']['wayOfProcessing'] = 'Drceni';

        $crawler = $client->request($form->getMethod(), $form->getUri(), $values, $form->getPhpFiles());
        $this->assertContains('Část není ve vhodné formě na tento způsob zpracování', $client->getResponse()->getContent());
        $this->assertContains('Část není na provozovně Praha - Letnany', $client->getResponse()->getContent());
        $this->assertContains('Část není ve stavu ke zpracování', $client->getResponse()->getContent());
        $this->assertContains('Část s tímto neexistuje', $client->getResponse()->getContent());
        $this->assertContains('Podoba Drt se nedá uskladnit způsobem volne.', $client->getResponse()->getContent());
    }

    public function testNewValid()
    {
         $client = static::makeClient();
        $crawler = $client->request('GET', '/zpracovani/novy');
        $form = $crawler->selectButton('processing_save')->form();

        $form['processing[warehouse]'] = '1';
        $part = $this->fixtures->getReference('part-zpracovani');
        $inputs = $this->getInputs(array($part));
        $outputs = $this->getOutputs(array($part));
        $values = $this->merge($inputs, $outputs, $form);

        $crawler = $client->request($form->getMethod(), $form->getUri(), $values, $form->getPhpFiles());
        $crawler = $client->followRedirect();
        $this->assertContains('Zpracování uloženo', $client->getResponse()->getContent());
    }

    public function testNewMerge(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/zpracovani/novy');
        $form = $crawler->selectButton('processing_save')->form();

        $form['processing[warehouse]'] = '1';
        $part = $this->fixtures->getReference('part-zpracovani');
        $part2 = $this->fixtures->getReference('part-divide-zpracovani');
        $inputs = $this->getInputs(array($part, $part2));
        $outputs = $this->getOutputs(array($part));
        $values = $this->merge($inputs, $outputs, $form);
        $crawler = $client->request($form->getMethod(), $form->getUri(), $values, $form->getPhpFiles());

        $this->assertContains('Materiály s ID '.$part->getMaterial()->getId().' a '.$part2->getMaterial()->getId().' spolu nemohou být zpracovány', $client->getResponse()->getContent());

        $part2 = $this->fixtures->getReference('part-zpracovani-nezpracovano');
        $inputs = $this->getInputs(array($part, $part2));
        $values = $this->merge($inputs, $outputs, $form);
        $crawler = $client->request($form->getMethod(), $form->getUri(), $values, $form->getPhpFiles());
        $crawler = $client->followRedirect();
        $this->assertContains('Zpracování uloženo', $client->getResponse()->getContent());
        $crawler = $client->request('GET', '/material/detail/'.$part2->getMaterial()->getId());
        $this->assertEquals(2, $crawler->filter('table.cast')->first()->filter('tr')->count());
    }

    public function testShowGet(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/zpracovani');

        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertEquals(1, $crawler->filter('form')->count());
        $this->assertEquals(1, $crawler->filter('form[name="processing_filter"]')->count());
        $this->assertEquals(1, $crawler->filter('input[name="processing_filter[start_date]"]')->count());
        $this->assertEquals(1, $crawler->filter('select[name="processing_filter[warehouse]"]')->count());
        $this->assertEquals(1, $crawler->filter('select[name="processing_filter[wayOfProcessing]"]')->count());
        $this->assertEquals(1, $crawler->filter('button[type="submit"]')->count());
    }

    public function testShowPost(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/zpracovani');
        $formButton = $crawler->selectButton('processing_filter_show');
        $form = $formButton->form();
        $form['processing_filter[start_date]'] = '01.01.2015';
        $form['processing_filter[end_date]'] = '01.02.2015';
        $form['processing_filter[warehouse]'] = '1';
        $form['processing_filter[wayOfProcessing]'] = 'Regranulace';
        $crawler = $client->submit($form);

        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertEquals(1, $crawler->filter('form')->count());
        $this->assertEquals(1, $crawler->filter('table')->count());
        $this->assertContains("Regranulace", $crawler->filter('table')->html());
        $this->assertContains("Plzen", $crawler->filter('table')->html());
        $this->assertGreaterThan(0, $crawler->filter('table')->filter('a')->count());
    }

    public function testDetail(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/zpracovani');
        $form = $crawler->selectButton('processing_filter_show')->form();
        $form['processing_filter[start_date]'] = '01.01.2015';
        $form['processing_filter[end_date]'] = '01.02.2015';
        $crawler = $client->submit($form);
        $processing = $this->fixtures->getReference('processing-show-1');
        $link = $crawler->filter('.content')->selectLink($processing->getId())->link();
        $crawler = $client->click($link);
        $this->assertContains($processing->getId(), $crawler->filter('table td:first-child')->html());
        $this->assertContains($processing->getWarehouse()->getName(), $crawler->filter('table')->html());
        $this->assertContains($this->fixtures->getReference('material-show')->getId(), $crawler->filter('.content')->filter('a')->html());
        $this->assertContains($this->fixtures->getReference('part-show-1')->getId(), $crawler->filter('table.cast')->eq(0)->filter('td:nth-child(1)')->html() );
        $this->assertContains($this->fixtures->getReference('part-show-2')->getId(), $crawler->filter('table.cast')->eq(1)->filter('td:first-child')->html() );
    }

    public function testWayShow(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/zpracovani/zpusob');
        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertEquals(1, $crawler->filter('.content')->filter('h1')->count());
        $this->assertEquals(2, $crawler->filter('.content')->filter('dl')->count());
        $this->assertContains('Název', $crawler->filter('.content')->filter('dl')->first()->filter('dt')->html());
        $this->assertContains('Drceni', $crawler->filter('.content')->filter('dl')->first()->filter('dd')->html());
        $this->assertContains('Upravit', $crawler->filter('.content')->filter('dl')->first()->filter('a')->html());
        $this->assertContains('Nový způsob', $crawler->filter('.content')->filter('a')->html());
        $newLink = $crawler->selectLink('Nový způsob')->link();
        $crawler = $client->click($newLink);
        $this->assertEquals(1, $crawler->filter('form[name="way_of_processing"]')->count());
        $crawler = $client->request('GET', '/zpracovani/zpusob');
        $newLink = $crawler->filter('dl')->selectLink('Upravit')->link();
        $crawler = $client->click($newLink);
        $this->assertEquals(1, $crawler->filter('form[name="way_of_processing"]')->count());
    }

    public function testWayNew(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/zpracovani/zpusob/novy');
        $form = $crawler->selectButton('way_of_processing_save')->form();
        $form['way_of_processing[title]'] = 'Regranulace';
        $crawler = $client->submit($form);
        $this->assertContains('Způsob zpracování s tímto názvem už existuje.', $client->getResponse()->getContent());
        $form = $crawler->selectButton('way_of_processing_save')->form();
        $form['way_of_processing[title]'] = 'Lisovani';
        $form['way_of_processing[entranceForms]'][0]->tick();
        $form['way_of_processing[outputForm]']->select('Kusovka');
        $crawler = $client->submit($form);
        $this->assertContains('Nesprávné vstupní a výstupní podoby', $client->getResponse()->getContent());
        $form['way_of_processing[entranceForms]'][0]->tick();
        $form['way_of_processing[entranceForms]'][1]->untick();
        $form['way_of_processing[outputForm]']->select('Regranulat');
        $crawler = $client->submit($form);
        $crawler = $client->followRedirect();
        $this->assertContains('Způsob zpracování uložen', $client->getResponse()->getContent());
        $this->assertContains('Lisovani', $client->getResponse()->getContent());
        $this->assertContains('Regranulace', $client->getResponse()->getContent());
    }

    public function testWayUpdate(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/zpracovani/zpusob/upravit/Regranulace');

        $form = $crawler->selectButton('way_of_processing_save')->form();
        $form['way_of_processing[entranceForms]'][2]->tick();
        $crawler = $client->submit($form);

        $this->assertContains('Výstupní podoba nesmí být stejná jako vstupní', $client->getResponse()->getContent());
        $form['way_of_processing[entranceForms]'][2]->untick();
        $crawler = $client->submit($form);
        $crawler = $client->followRedirect();
        $this->assertContains('Způsob zpracování uložen', $client->getResponse()->getContent());
        $this->assertContains('Regranulace', $crawler->filter('.content')->filter('dl')->eq(1)->html());
        $this->assertNotContains('Kusovka', $crawler->filter('.content')->filter('dl')->eq(1)->html());
        $this->assertContains('Drceni', $client->getResponse()->getContent());
    }
}
