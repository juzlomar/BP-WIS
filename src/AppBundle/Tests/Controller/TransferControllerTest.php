<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Controller;


use AppBundle\Entity\Material;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\FunctionalTestBundle\Test\WebTestCase;

class TransferControllerTest extends WebTestCase
{
    /** @var  ReferenceRepository */
    private $fixtures;

    public function setUp(){
        $this->fixtures = $this->loadFixtures(array(
            'AppBundle\DataFixtures\ORM\Test\LoadWayOfStoringData',
            'AppBundle\DataFixtures\ORM\Test\LoadFormData',
            'AppBundle\DataFixtures\ORM\Test\LoadWayOfProcessingData',
            'AppBundle\DataFixtures\ORM\Test\LoadAddressData',
            'AppBundle\DataFixtures\ORM\Test\LoadWarehouseData',
            'AppBundle\DataFixtures\ORM\Test\LoadBusinessPartnerData',
            'AppBundle\DataFixtures\ORM\Test\LoadMaterialData',
            'AppBundle\DataFixtures\ORM\Test\LoadPartData',
            'AppBundle\DataFixtures\ORM\Test\LoadTransferData',
            'AppBundle\DataFixtures\ORM\Test\LoadUserData',
        ))->getReferenceRepository();
        $this->loginAs($this->fixtures->getReference('marketa'), 'main');
    }

    private function getMaterial(Material $material, $parts)
    {
        $materialArray = array(
            'id' => $material->getId(),
            'plastics' => $material->getPlastics(),
            'specification' => $material->getSpecification(),
            'color' => $material->getColor(),
            'finalForm' => $material->getFinalForm()->getTitle(),
            'parts' => array()
        );

        foreach($parts as $part){
            $partArray = array(
                'id' => $part->getId(),
                'number' => $part->getNumber(),
                'weightNetto' => $part->getWeightNetto(),
                'weightBrutto' => $part->getWeightBrutto(),
                'currentForm' => $part->getCurrentForm()->getTitle(),
                'wayOfStoring' => $part->getWayOfStoring()->getTitle(),
                'state' => $part->getState(),
                'note' => $part->getNote(),
            );
            array_push($materialArray['parts'], $partArray);
        }

        $businessArray = array(
            'materialPrice' => '30',
            'materials' => array(
                $materialArray,
            ));
        return $businessArray;
    }

    private function merge($materials, $form){
        $values = array_merge_recursive(
            $form->getPhpValues(),
            array(
                'transfer' => array(
                    'materials' => $materials
                )
            )
        );
        return $values;
    }

    public function testNewGet(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/odvoz/novy');
        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertEquals(1, $crawler->filter('form')->count());
        $this->assertEquals(1, $crawler->filter('form[name="transfer"]')->count());
        $this->assertEquals(1, $crawler->filter('input[name="transfer[price]"]')->count());
        $this->assertEquals(1, $crawler->filter('input[name="transfer[date]"]')->count());
        $this->assertEquals(1, $crawler->filter('select[name="transfer[from]"]')->count());
        $this->assertEquals(1, $crawler->filter('select[name="transfer[to]"]')->count());
        $this->assertEquals(1, $crawler->filter('button[type="submit"]')->count());
        $this->assertEquals(1, $crawler->filter('button#transfer_choose')->count());
        $this->assertEquals(0, $crawler->filter('.content')->filter('li')->count());
    }

    public function testMaterials(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/odvoz/novy');
        $formButton = $crawler->selectButton('transfer_choose');
        $form = $formButton->form();
        $crawler = $client->request('POST', '/preprava/material/transfer', $form->getPhpValues(), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ));
        $this->assertTrue($client->getResponse()->isSuccessful(), 'Neuspech ajax zadosti');
        $part = $this->fixtures->getReference('part-preprava');
        $this->assertContains($part->getId(), $crawler->filter('#materials_to_transfer')->html());
    }

    public function testNewInvalid(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/odvoz/novy');
        $form = $crawler->selectButton('transfer_save')->form();
        $form['transfer[price]'] = '-1000';
        $form['transfer[from]'] = '2';
        $values = $this->merge(array(), $form);
        $values['transfer']['to'] = $this->fixtures->getReference('Dura Blatna')->getId() + 10;
        $crawler = $client->request($form->getMethod(), $form->getUri(), $values, $form->getPhpFiles());

        $this->assertContains('Přeprava musí obsahovat alespoň jeden materiál', $client->getResponse()->getContent());
        $this->assertContains('Cena dopravy musí být větší než nula.', $crawler->filter('#transfer_price')->parents()->parents()->html());
        $this->assertContains('Neexistující partner.', $crawler->filter('#transfer_to')->parents()->parents()->html());

        $material = $this->fixtures->getReference('material-preprava');
        $material->setId($material->getId() + 10);
        $values = $this->merge(array($this->getMaterial($material, array())), $form);
        $crawler = $client->request($form->getMethod(), $form->getUri(), $values, $form->getPhpFiles());

        $this->assertContains('Materiál musí obsahovat alespoň jednu část', $client->getResponse()->getContent());
        $this->assertContains('Materiál s tímto ID neexistuje', $client->getResponse()->getContent());

        $form['transfer[price]'] = '';
        $material->setId($material->getId() - 10);
        $part = $this->fixtures->getReference('part-divide-expedice');
        $part1 = $this->fixtures->getReference('part-zastaveno');
        $material1 = $this->fixtures->getReference('material-zastaveno');
        $materials = array(
            $this->getMaterial($material, array($part)),
            $this->getMaterial($material1, array($part1)),
        );
        $values = $this->merge($materials, $form);
        $crawler = $client->request($form->getMethod(), $form->getUri(), $values, $form->getPhpFiles());

        $this->assertContains('Přeprava musí mít cenu dopravy.', $crawler->filter('#transfer_price')->parents()->parents()->html());
        $this->assertContains('Materiál s ID '.$material->getId().' nemá část s tímto ID', $client->getResponse()->getContent());
        $this->assertContains('Část není ve stavu k převozu', $client->getResponse()->getContent());
        $this->assertContains('Materiál není na provozovně Praha - Letnany', $client->getResponse()->getContent());
        $this->assertContains('Část není ve finální podobě', $client->getResponse()->getContent());
    }

    public function testNewValid(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/odvoz/novy');
        $form = $crawler->selectButton('transfer_save')->form();
        $form['transfer[price]'] = '1000';
        $form['transfer[from]'] = '1';
        $form['transfer[to]'] = $this->fixtures->getReference('Dura Blatna')->getId();
        $material = $this->fixtures->getReference('material-preprava');
        $part = $this->fixtures->getReference('part-preprava');

        $values = $this->merge(array($this->getMaterial($material, array($part))), $form);

        $crawler = $client->request($form->getMethod(), $form->getUri(), $values, $form->getPhpFiles());
        $crawler = $client->followRedirect();
        $this->assertContains('Odvoz uložen', $client->getResponse()->getContent());

        $crawler = $client->request('GET', '/material/detail/'.$material->getId());
        $this->assertEquals(2, $crawler->filter('table.cast')->first()->filter('tr')->count());
        $this->assertContains('odvezeno', $crawler->filter('table.cast')->first()->html());
    }

    public function testNewDivide(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/odvoz/novy');
        $form = $crawler->selectButton('transfer_save')->form();
        $form['transfer[price]'] = '1000';
        $form['transfer[from]'] = '1';
        $form['transfer[to]'] = $this->fixtures->getReference('Dura Blatna')->getId();
        $material = $this->fixtures->getReference('material-divide');
        $part = $this->fixtures->getReference('part-divide-zpracovani');

        $values = $this->merge(array($this->getMaterial($material, array($part))), $form);

        $crawler = $client->request($form->getMethod(), $form->getUri(), $values, $form->getPhpFiles());
        $this->assertStatusCode(200, $client);

        $this->assertContains('není ve stavu k převozu', $client->getResponse()->getContent());
        $this->assertNotContains('není ve finální podobě', $client->getResponse()->getContent());
        $this->assertNotContains('Odvoz uložen', $client->getResponse()->getContent());

        $part1 = $this->fixtures->getReference('part-divide-expedice');
        $values = $this->merge(array($this->getMaterial($material, array($part1))), $form);

        $crawler = $client->request($form->getMethod(), $form->getUri(), $values, $form->getPhpFiles());
        $crawler = $client->followRedirect();
        $this->assertNotContains('není ve stavu k převozu', $client->getResponse()->getContent());
        $this->assertContains('Odvoz uložen', $client->getResponse()->getContent());
        $crawler = $client->request('GET', '/material/detail/'.$material->getId());

        $this->assertContains('připraveno ke zpracování', $client->getResponse()->getContent());
        $this->assertEquals(2, $crawler->filter('table.cast')->first()->filter('tr')->count());
    }

    public function testShowGet(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/odvoz');

        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertEquals(1, $crawler->filter('form')->count());
        $this->assertEquals(1, $crawler->filter('form[name="transfer_filter"]')->count());
        $this->assertEquals(1, $crawler->filter('input[name="transfer_filter[start_date]"]')->count());
        $this->assertEquals(1, $crawler->filter('select[name="transfer_filter[from]"]')->count());
        $this->assertEquals(1, $crawler->filter('select[name="transfer_filter[to]"]')->count());
        $this->assertEquals(1, $crawler->filter('button[type="submit"]')->count());
    }


    public function testShowPost(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/odvoz');
        $formButton = $crawler->selectButton('transfer_filter_show');
        $form = $formButton->form();
        $form['transfer_filter[start_date]'] = '01.01.2015';
        $form['transfer_filter[end_date]'] = '01.02.2015';
        $form['transfer_filter[from]'] = '2';
        $form['transfer_filter[to]'] = $this->fixtures->getReference('Dura Blatna')->getId();
        $crawler = $client->submit($form);

        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertEquals(1, $crawler->filter('form')->count());
        $this->assertEquals(1, $crawler->filter('table')->count());
        $this->assertContains("1000", $crawler->filter('table')->parents()->html());
        $this->assertContains("Dura Blatna", $crawler->filter('table')->parents()->html());
        $this->assertContains("Praha - Letnany", $crawler->filter('table')->html());
        $this->assertGreaterThan(0, $crawler->filter('table')->filter('a')->count());
    }
}
