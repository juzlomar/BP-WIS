<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class WarehouseControllerTest extends WebTestCase
{
    /** @var  ReferenceRepository */
    private $fixtures;

    public function setUp(){
        $this->fixtures = $this->loadFixtures(array(
            'AppBundle\DataFixtures\ORM\Test\LoadFormData',
            'AppBundle\DataFixtures\ORM\Test\LoadWayOfStoringData',
            'AppBundle\DataFixtures\ORM\Test\LoadWayOfProcessingData',
            'AppBundle\DataFixtures\ORM\Test\LoadAddressData',
            'AppBundle\DataFixtures\ORM\Test\LoadWarehouseData',
            'AppBundle\DataFixtures\ORM\Test\LoadUserData',
        ))->getReferenceRepository();
        $this->loginAs($this->fixtures->getReference('marketa'), 'main');
    }

    public function testShow(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/provozovna');
        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertEquals(1, $crawler->filter('.content')->filter('h1')->count());
        $this->assertEquals(4, $crawler->filter('.content')->filter('dl')->count());
        $this->assertContains('ID', $crawler->filter('.content')->filter('dl')->first()->filter('dt')->html());
        $this->assertContains('Drceni', $crawler->filter('.content')->filter('dl')->first()->filter('dd')->eq(7)->html());
        $this->assertContains('Mesto', $crawler->filter('.content')->filter('dl')->first()->filter('dl')->html());
        $this->assertContains('Deaktivovat', $crawler->filter('.content')->filter('dl')->eq(0)->filter('a')->eq(0)->html());
        $this->assertContains('Upravit', $crawler->filter('.content')->filter('dl')->eq(0)->filter('a')->eq(1)->html());
        $this->assertContains('Nová provozovna', $crawler->filter('.content')->filter('a')->html());
        $newLink = $crawler->filter('.content')->filter('dl')->first()->selectLink('Upravit')->link();
        $crawler = $client->click($newLink);
        $this->assertEquals(1, $crawler->filter('form[name="warehouse"]')->count());
        $crawler = $client->request('GET', '/provozovna');
        $newLink = $crawler->selectLink('Nová provozovna')->link();
        $crawler = $client->click($newLink);
        $this->assertEquals(1, $crawler->filter('form[name="warehouse"]')->count());
    }

    public function testNew(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/provozovna/novy');
        $form = $crawler->selectButton('warehouse_save')->form();
        $form['warehouse[id]'] = '2';
        $form['warehouse[name]'] = 'Plzen';
        $form['warehouse[address][town]'] = 'Mesto';
        $form['warehouse[address][psc]'] = '98765';
        $form['warehouse[address][street]'] = 'Ulice';
        $form['warehouse[address][cp]'] = '90';
        $crawler = $client->submit($form);
        $this->assertContains('Provozovna s tímto ID už existuje', $client->getResponse()->getContent());
        $this->assertContains('Provozovna s tímto názvem už existuje', $client->getResponse()->getContent());
        $form['warehouse[id]'] = '3';
        $form['warehouse[name]'] = 'Praha - Zliciv';
        $crawler = $client->submit($form);
        $crawler = $client->followRedirect();
        $this->assertContains('Provozovna uložena', $client->getResponse()->getContent());
        $this->assertContains('98765', $client->getResponse()->getContent());
        $this->assertContains('Plzen', $client->getResponse()->getContent());
    }

    public function testUpdate(){
        $client = static::makeClient();
        $crawler = $client->request('GET', '/provozovna/upravit/1');
        $this->assertEquals(1, $crawler->filter('form[name="warehouse"]')->count());
        $form = $crawler->selectButton('warehouse_save')->form();
        $form['warehouse[name]'] = 'Plzen - zapad';
        $crawler = $client->submit($form);
        $crawler = $client->followRedirect();
        $this->assertContains('Provozovna uložena', $client->getResponse()->getContent());
        $this->assertContains('Plzen - zapad', $client->getResponse()->getContent());
        $this->assertContains('Praha - Letnany', $client->getResponse()->getContent());
    }

    public function testRemove(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/provozovna');
        $newLink = $crawler->filter('dl')->first()->selectLink('Deaktivovat')->link();
        $crawler = $client->click($newLink);
        $crawler = $client->followRedirect();
        $this->assertContains('Provozovna deaktivována',$client->getResponse()->getContent());
        $this->assertContains('Aktivovat',$crawler->filter('dl')->first()->html());
    }

    public function testActivate(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/provozovna/smazat/1');
        $crawler = $client->followRedirect();
        $newLink = $crawler->filter('.content')->filter('dl')->first()->selectLink('Aktivovat')->link();
        $crawler = $client->click($newLink);
        $crawler = $client->followRedirect();
        $this->assertContains('Provozovna aktivována',$client->getResponse()->getContent());
        $this->assertContains('Deaktivovat',$crawler->filter('.content')->filter('dl')->first()->html());
    }
}
