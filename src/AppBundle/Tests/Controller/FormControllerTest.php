<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class FormControllerTest extends WebTestCase
{
    /** @var  ReferenceRepository */
    private $fixtures;

    public function setUp(){
        $this->fixtures = $this->loadFixtures(array(
            'AppBundle\DataFixtures\ORM\Test\LoadUserData',
            'AppBundle\DataFixtures\ORM\Test\LoadWayOfStoringData',
            'AppBundle\DataFixtures\ORM\Test\LoadFormData',
            'AppBundle\DataFixtures\ORM\Test\LoadWayOfProcessingData',
        ))->getReferenceRepository();
        $this->loginAs($this->fixtures->getReference('marketa'), 'main');
    }

    public function testShow(){
        $client = static::makeClient();
        $crawler = $client->request('GET', '/podoba');
        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertEquals(1, $crawler->filter('.content')->filter('h1')->count());
        $this->assertEquals(3, $crawler->filter('.content')->filter('dl')->count());
        $this->assertContains('Název', $crawler->filter('.content')->filter('dl')->first()->filter('dt')->html());
        $this->assertContains('Drt', $crawler->filter('.content')->filter('dl')->first()->filter('dd')->html());
        $this->assertContains('Upravit', $crawler->filter('.content')->filter('dl')->first()->filter('a')->html());
        $this->assertContains('Nová podoba', $crawler->filter('.content')->filter('a')->eq(0)->html());
        $newLink = $crawler->selectLink('Nová podoba')->link();
        $crawler = $client->click($newLink);
        $this->assertEquals(1, $crawler->filter('form[name="form"]')->count());
        $crawler = $client->request('GET', '/podoba');
        $newLink = $crawler->filter('.content')->filter('dl')->selectLink('Upravit')->link();
        $crawler = $client->click($newLink);
        $this->assertEquals(1, $crawler->filter('form[name="form"]')->count());
    }

    public function testNew(){
        $client = static::makeClient();
        $crawler = $client->request('GET', '/podoba/novy');
        $form = $crawler->selectButton('form_save')->form();

        $form['form[title]'] = 'Drt';
        $form['form[title_eng]'] = 'Drt';
        $crawler = $client->submit($form);
        $this->assertContains('Podoba s tímto názvem už existuje.', $client->getResponse()->getContent());
        $form['form[title]'] = 'Balik';
        $form['form[title_eng]'] = 'Balik_eng';
        $form['form[wayOfProcessing]'][0]->tick();
        $form['form[wayOfStoring]'][0]->tick();
        $crawler = $client->submit($form);
        $crawler = $client->followRedirect();
        $this->assertContains('Podoba uložena', $client->getResponse()->getContent());
        $this->assertContains('Drceni',  $crawler->filter('.content')->filter('dl')->eq(0)->html());

        $this->assertContains('v bagu',  $crawler->filter('.content')->filter('dl')->eq(0)->html());
        $this->assertContains('Kusovka', $client->getResponse()->getContent());

    }
    public function testUpdate(){
        $client = static::makeClient();
        $crawler = $client->request('GET', '/podoba/upravit/Regranulat');
        $form = $crawler->selectButton('form_save')->form();

        $form['form[wayOfProcessing]'][0]->tick();
        $crawler = $client->submit($form);

        $this->assertContains('Nesprávné způsoby zpracování.', $client->getResponse()->getContent());
        $form['form[wayOfProcessing]'][0]->untick();
        $form['form[wayOfStoring]'][1]->tick();
        $crawler = $client->submit($form);
        $crawler = $client->followRedirect();
        $this->assertContains('Podoba uložena', $client->getResponse()->getContent());
        $this->assertContains('volne', $crawler->filter('.content')->filter('dl')->eq(2)->html());
        $this->assertNotContains('Drceni', $crawler->filter('.content')->filter('dl')->eq(2)->html());
        $this->assertContains('Kusovka', $client->getResponse()->getContent());
    }

}
