<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Controller;


use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\FunctionalTestBundle\Test\WebTestCase;

class MaterialControllerTest extends WebTestCase
{
    /** @var  ReferenceRepository */
    private $fixtures;

    public function setUp(){
        $this->fixtures = $this->loadFixtures(array(
            'AppBundle\DataFixtures\ORM\Test\LoadWayOfStoringData',
            'AppBundle\DataFixtures\ORM\Test\LoadFormData',
            'AppBundle\DataFixtures\ORM\Test\LoadWayOfProcessingData',
            'AppBundle\DataFixtures\ORM\Test\LoadAddressData',
            'AppBundle\DataFixtures\ORM\Test\LoadWarehouseData',
            'AppBundle\DataFixtures\ORM\Test\LoadBusinessPartnerData',
            'AppBundle\DataFixtures\ORM\Test\LoadUserData',
            'AppBundle\DataFixtures\ORM\Test\LoadMaterialData',
            'AppBundle\DataFixtures\ORM\Test\LoadPartData',
            'AppBundle\DataFixtures\ORM\Test\LoadShowMaterialData',
            'AppBundle\DataFixtures\ORM\Test\LoadShowTransportData',
            'AppBundle\DataFixtures\ORM\Test\LoadShowProcessingData',
        ))->getReferenceRepository();
        $this->loginAs($this->fixtures->getReference('marketa'), 'main');
    }

    public function testShow(){
        $client = static::makeClient();
        $crawler = $client->request('GET', '/material');
        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertEquals(1, $crawler->filter('form[name="material_filter"]')->count(), 'Jmeno formulare neni spravne');
        $this->assertEquals(2, $crawler->filter('input[name="material_filter[warehouse][]"]')->count());
        $this->assertEquals(0, $crawler->filter('table')->filter('tr')->count());
        $form = $crawler->selectButton('material_filter_show')->form();
        $form['material_filter[warehouse]'][0]->tick();
        $form['material_filter[from]']->select($this->fixtures->getReference('Dura Blatna')->getId());
        $form['material_filter[to]']->select($this->fixtures->getReference('Partner')->getId());;
        $form['material_filter[currentForm]'][2]->tick();
        $form['material_filter[wayOfStoring]'][0]->tick();
        foreach( $form['material_filter[state]'] as $state){
            $state->untick();
        }
        $form['material_filter[state]'][4]->tick();
        $form['material_filter[finalForm]'][2]->tick();
        $crawler = $client->submit($form);
        $this->assertEquals(1, $crawler->filter('form[name="material_filter"]')->count(), 'Jmeno formulare neni spravne');
        $material = $this->fixtures->getReference('material-show');
        $part =  $this->fixtures->getReference('part-show-3');
        $this->assertContains($material->getId(), $crawler->filter('.content')->filter('a')->html());
        $this->assertContains($part->getId(), $crawler->filter('table.cast')->first()->filter('td:first-child')->html());
        $this->assertEquals(1, $crawler->filter('#materials_table .part_table tbody')->filter('tr')->count());

        $link = $crawler->selectLink($material->getId())->link();
        $crawler1 = $client->click($link);
        $this->assertCount(2, $crawler->filter('table.cast')->eq(0)->filter('tr'));
    }

    public function testDetail(){
        $client = static::makeClient();
        $material = $this->fixtures->getReference('material-show');
        $part3 = $this->fixtures->getReference('part-show-3');
        $crawler = $client->request('GET', '/material/detail/'.$material->getId());

        $this->assertCount(2, $crawler->filter('table.cast')->eq(0)->filter('tr'));
        $this->assertContains($part3->getId(), $crawler->filter('table.cast')->filter('tr')->eq(1)->filter('td')->html());
        $this->assertSame($this->fixtures->getReference('transfer-show')->getId(), $crawler->filter('div.transport table')->filter('tr')->eq(3)->filter('td')->filter('a')->html());
        $this->assertSame($this->fixtures->getReference('processing-show-1')->getId(), $crawler->filter('h2')->eq(1)->siblings()->filter('a')->html());
        $this->assertSame($this->fixtures->getReference('part-show-1')->getId(), $crawler->filter('.processing_table')->filter('.part_table')->filter('tbody td:first-child')->html());

        $this->assertCount(2, $crawler->filter('.processing_table')->filter('.part_table')->eq(0)->filter('tr'));

        $bringing = $this->fixtures->getReference('bringing-show');
        $link = $crawler->filter('div.transport')->selectLink($bringing->getId())->link();
        $crawler1 = $client->click($link);
        $this->assertContains($bringing->getId(), $crawler1->filter('table td:first-child')->html());
        $processing = $this->fixtures->getReference('processing-show-1');
        $link = $crawler->filter('h2')->eq(1)->siblings()->selectLink($processing->getId())->link();
        $crawler = $client->click($link);
        $this->assertContains($processing->getId(), $crawler->filter('table td:first-child')->html());
    }

    public function testReportProcessing(){
        $client = static::makeClient();
        $crawler = $client->request('GET', '/material/vykaz');
        $this->assertCount(0, $crawler->filter('#material_report_table'));
        $form = $crawler->selectButton('report_show')->form();
        $form['report[start_date]'] = '01.01.2015';
        $form['report[end_date]'] = '01.02.2015';
        $form['report[type]']->select('zpracovaný materiál');
        $form['report[warehouse]']->select(1);
        $crawler = $client->submit($form);

        $this->assertCount(1, $crawler->filter('#material_report_table'));
        $material = $this->fixtures->getReference('material-show');
        $this->assertSame($material->getId(), $crawler->filter('#material_report_table')->filter('a')->html());
        $this->assertCount(3, $crawler->filter('#material_report_table')->filter('table.part_table')->filter('tr'));
        $this->assertContains('04.01.2015', $crawler->filter('#material_report_table')->filter('table.part_table')->html());
    }

    public function testReportBringing(){
        $client = static::makeClient();
        $crawler = $client->request('GET', '/material/vykaz');
        $this->assertCount(0, $crawler->filter('#material_report_table'));
        $form = $crawler->selectButton('report_show')->form();
        $form['report[start_date]'] = '01.01.2015';
        $form['report[end_date]'] = '01.02.2015';
        $form['report[type]']->select('navezený materiál');
        $form['report[warehouse]']->select(1);
        $crawler = $client->submit($form);
        $this->assertCount(1, $crawler->filter('#material_report_table'));
        $material = $this->fixtures->getReference('material-show');
        $this->assertSame($material->getId(), $crawler->filter('#material_report_table')->filter('a')->html());
        $this->assertCount(2, $crawler->filter('#material_report_table')->filter('table.part_table')->filter('tr'));
        $this->assertContains('zpracováno', $crawler->filter('#material_report_table')->filter('table.part_table')->html());
        $this->assertContains('Praha - Letnany', $crawler->filter('#material_report_table')->html());
        $this->assertContains('03.01.2015', $crawler->filter('#material_report_table')->html());
    }

    public function testReportTransfer(){
        $client = static::makeClient();
        $crawler = $client->request('GET', '/material/vykaz');
        $this->assertCount(0, $crawler->filter('#material_report_table'));
        $form = $crawler->selectButton('report_show')->form();
        $form['report[start_date]'] = '01.01.2015';
        $form['report[end_date]'] = '01.02.2015';
        $form['report[type]']->select('odvezený materiál');
        $form['report[warehouse]']->select(1);
        $crawler = $client->submit($form);
        $this->assertCount(1, $crawler->filter('#material_report_table'));
        $material = $this->fixtures->getReference('material-show');
        $this->assertSame($material->getId(), $crawler->filter('#material_report_table')->filter('a')->html());
        $this->assertCount(2, $crawler->filter('#material_report_table')->filter('table.part_table')->filter('tr'));
        $this->assertContains('odvezeno', $crawler->filter('#material_report_table')->filter('table.part_table')->html());
        $this->assertContains('Partner', $crawler->filter('#material_report_table')->html());
        $this->assertContains('05.01.2015', $crawler->filter('#material_report_table')->html());
    }

    public function testState(){
        $client = static::makeClient();
        $crawler = $client->request('GET', '/stav');
        $this->assertCount(0, $crawler->filter('table'));
        $form = $crawler->selectButton('state_show')->form();
        $form['state[form]']->select('Drt');
        $form['state[warehouse]']->select('1');
        $form['state[type]']->select('česky');
        $crawler = $client->submit($form);
        $this->assertCount(1, $crawler->filter('table'));
        $this->assertCount(9, $crawler->filter('table')->filter('tr'));
        $this->assertSame('4000', $crawler->filter('table')->filter('tr')->last()->filter('td:last-child')->html());
        $this->assertContains('Celkem : PP+PE', $crawler->filter('table')->html());
        $this->assertContains('2400', $crawler->filter('table')->html());
    }

}
