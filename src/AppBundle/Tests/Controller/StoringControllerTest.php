<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class StoringControllerTest extends WebTestCase
{
    /** @var  ReferenceRepository */
    private $fixtures;

    public function setUp(){
        $this->fixtures = $this->loadFixtures(array(
            'AppBundle\DataFixtures\ORM\Test\LoadWayOfStoringData',
            'AppBundle\DataFixtures\ORM\Test\LoadFormData',
            'AppBundle\DataFixtures\ORM\Test\LoadUserData',
        ))->getReferenceRepository();
        $this->loginAs($this->fixtures->getReference('marketa'), 'main');
    }

    public function testShow(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/uskladneni');
        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertEquals(1, $crawler->filter('.content')->filter('h1')->count());
        $this->assertEquals(2, $crawler->filter('.content')->filter('dl')->count());
        $this->assertContains('Název', $crawler->filter('.content')->filter('dl')->first()->filter('dt')->html());
        $this->assertContains('v bagu', $crawler->filter('.content')->filter('dl')->first()->filter('dd')->html());
        $this->assertContains('Upravit', $crawler->filter('.content')->filter('dl')->first()->filter('a')->html());
        $this->assertContains('Nový způsob uskladnění', $crawler->filter('.content')->filter('a')->html());
        $newLink = $crawler->selectLink('Nový způsob uskladnění')->link();
        $crawler = $client->click($newLink);
        $this->assertEquals(1, $crawler->filter('form[name="way_of_storing"]')->count());
        $crawler = $client->request('GET', '/uskladneni');
        $newLink = $crawler->filter('.content')->filter('dl')->selectLink('Upravit')->link();
        $crawler = $client->click($newLink);
        $this->assertEquals(1, $crawler->filter('form[name="way_of_storing"]')->count());
    }

    public function testNew(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/uskladneni/novy');
        $form = $crawler->selectButton('way_of_storing_save')->form();
        $form['way_of_storing[title]'] = 'volne';
        $crawler = $client->submit($form);
        $this->assertContains('Způsob uskladnění s tímto názvem už existuje.', $client->getResponse()->getContent());
        $form = $crawler->selectButton('way_of_storing_save')->form();
        $form['way_of_storing[title]'] = 'v oktabinu';
        $form['way_of_storing[forms]'][0]->tick();
        $crawler = $client->submit($form);
        $crawler = $client->followRedirect();
        $this->assertContains('Uskladnění uloženo', $client->getResponse()->getContent());
        $this->assertContains('v oktabinu', $client->getResponse()->getContent());
        $this->assertContains('v bagu', $client->getResponse()->getContent());
    }
    public function testUpdate(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/uskladneni/upravit/volne');
        $form = $crawler->selectButton('way_of_storing_save')->form();
        $form['way_of_storing[forms]'][0]->tick();
        $form['way_of_storing[forms]'][1]->untick();
        $crawler = $client->submit($form);
        $crawler = $client->followRedirect();

        $this->assertContains('Uskladnění uloženo', $client->getResponse()->getContent());
        $this->assertContains('Drt', $crawler->filter('.content')->filter('dl')->eq(1)->html());
        $this->assertNotContains('Kusovka', $crawler->filter('.content')->filter('dl')->eq(1)->html());
        $this->assertContains('v bagu', $client->getResponse()->getContent());
    }
}
