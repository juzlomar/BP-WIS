<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class PartnerControllerTest extends WebTestCase
{
    /** @var  ReferenceRepository */
    private $fixtures;

    public function setUp(){
        $this->fixtures = $this->loadFixtures(array(
            'AppBundle\DataFixtures\ORM\Test\LoadUserData',
            'AppBundle\DataFixtures\ORM\Test\LoadAddressData',
            'AppBundle\DataFixtures\ORM\Test\LoadBusinessPartnerData',
        ))->getReferenceRepository();
        $this->loginAs($this->fixtures->getReference('marketa'), 'main');
    }

    public function testShow(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/partner');
        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertEquals(1, $crawler->filter('.content')->filter('h1')->count());
        $this->assertContains('ID', $crawler->filter('.content')->filter('dl')->first()->filter('dt')->html());
        $this->assertContains('Dura Blatna', $crawler->filter('.content')->filter('dl')->first()->filter('dd')->eq(1)->html());
        $this->assertContains('Mesto', $crawler->filter('.content')->filter('dl')->first()->filter('dl')->html());
        $this->assertNotContains('Kontakty', $crawler->filter('.content')->html());
        $this->assertContains('Upravit', $crawler->filter('.content')->filter('dl')->first()->filter('a')->html());
        $this->assertContains('Nový partner', $crawler->filter('.content')->filter('a')->html());
        $newLink = $crawler->filter('.content')->filter('dl')->first()->selectLink('Upravit')->link();
        $crawler = $client->click($newLink);
        $this->assertEquals(1, $crawler->filter('form[name="business_partner"]')->count());
        $crawler = $client->request('GET', '/partner');
        $newLink = $crawler->selectLink('Nový partner')->link();
        $crawler = $client->click($newLink);
        $this->assertEquals(1, $crawler->filter('form[name="business_partner"]')->count());
    }

    public function testNew(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/partner/novy');
        $form = $crawler->selectButton('business_partner_save')->form();
        $form['business_partner[name]'] = 'Dura Blatna';
        $form['business_partner[address][town]'] = 'Mesto';
        $form['business_partner[address][psc]'] = '9876';
        $form['business_partner[address][street]'] = 'Ulice';
        $form['business_partner[address][cp]'] = '90';
        $crawler = $client->submit($form);
        $this->assertContains('PSČ musí být pětimístné celé číslo.', $client->getResponse()->getContent());
        $form['business_partner[address][psc]'] = '98765';
        $crawler = $client->submit($form);
        $this->assertContains('Partner s tímto názvem už existuje.', $client->getResponse()->getContent());
        $form['business_partner[name]'] = 'Novy partner';
        $values = $this->merge($form, $this->getContact());
        $crawler = $client->request($form->getMethod(), $form->getUri(), $values, $form->getPhpFiles());
        $crawler = $client->followRedirect();
        $this->assertContains('Partner uložen', $client->getResponse()->getContent());
        $this->assertContains('98765', $client->getResponse()->getContent());
        $this->assertContains('Dura Blatna', $client->getResponse()->getContent());
    }

    private function getContact(){
        $contact = array(
            'firstname' => 'Marketa',
            'lastname' => 'Juzlova',
            'phoneNumber' => '123456789',
            'email' => 'email@email.com'
        );
        return $contact;
    }

    private function merge($form, $contact){
        $values = array_merge_recursive(
            $form->getPhpValues(),
            array(
                'business_partner' => array(
                    'contacts' => array(
                        $contact,
                    )
                )
            )
        );
        return $values;
    }
    public function testUpdate(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/partner');
        $link = $crawler->filter('.content')->filter('dl')->first()->selectLink('Upravit')->link();
        $crawler = $client->click($link);
        $form = $crawler->selectButton('business_partner_save')->form();
        $values = $this->merge($form, $this->getContact());
        $crawler = $client->request($form->getMethod(), $form->getUri(), $values, $form->getPhpFiles());
        $crawler = $client->followRedirect();
        $this->assertContains('Partner uložen', $client->getResponse()->getContent());
        $this->assertContains('Dura Blatna', $client->getResponse()->getContent());
        $this->assertContains('Kontakty', $client->getResponse()->getContent());
    }
}
