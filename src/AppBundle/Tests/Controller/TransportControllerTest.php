<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Controller;


use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\FunctionalTestBundle\Test\WebTestCase;

class TransportControllerTest extends WebTestCase
{
    /** @var  ReferenceRepository */
    private $fixtures;

    public function setUp(){
        $this->fixtures = $this->loadFixtures(array(
            'AppBundle\DataFixtures\ORM\Test\LoadWayOfStoringData',
            'AppBundle\DataFixtures\ORM\Test\LoadFormData',
            'AppBundle\DataFixtures\ORM\Test\LoadWayOfProcessingData',
            'AppBundle\DataFixtures\ORM\Test\LoadAddressData',
            'AppBundle\DataFixtures\ORM\Test\LoadWarehouseData',
            'AppBundle\DataFixtures\ORM\Test\LoadBusinessPartnerData',
            'AppBundle\DataFixtures\ORM\Test\LoadUserData',
            'AppBundle\DataFixtures\ORM\Test\LoadShowMaterialData',
            'AppBundle\DataFixtures\ORM\Test\LoadShowTransportData',
            'AppBundle\DataFixtures\ORM\Test\LoadShowProcessingData',
        ))->getReferenceRepository();
        $this->loginAs($this->fixtures->getReference('marketa'), 'main');
    }

    public function testBringingDetail(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/navoz');
        $form = $crawler->selectButton('bringing_filter_show')->form();
        $form['bringing_filter[start_date]'] = '01.01.2015';
        $form['bringing_filter[end_date]'] = '01.02.2015';
        $crawler = $client->submit($form);
        $bringing = $this->fixtures->getReference('bringing-show');
        $link = $crawler->filter('.content')->selectLink($bringing->getId())->link();
        $crawler = $client->click($link);
        $this->assertContains($bringing->getId(), $crawler->filter('table td:first-child')->html());
        $this->assertContains($bringing->getFrom()->getName(), $crawler->filter('.content')->filter('table')->html());
        $this->assertContains($this->fixtures->getReference('material-show')->getId(), $crawler->filter('.content')->filter('a')->html());
        $ids = $crawler->filter('table.cast td:first-child')->each(function($node, $i){
            return $node->html();
        });
        $stringIds = implode($ids);
        $this->assertContains($this->fixtures->getReference('part-show-1')->getId(), $stringIds );
        $this->assertNotContains($this->fixtures->getReference('part-show-2')->getId(), $stringIds );
        $this->assertNotContains($this->fixtures->getReference('part-show-3')->getId(), $stringIds );
    }

    public function testInternalTransportDetail(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/prevoz');
        $form = $crawler->selectButton('internal_transport_filter_show')->form();
        $form['internal_transport_filter[start_date]'] = '01.01.2015';
        $form['internal_transport_filter[end_date]'] = '01.02.2015';
        $crawler = $client->submit($form);
        $transport = $this->fixtures->getReference('internal-show');
        $link = $crawler->filter('.content')->selectLink($transport->getId())->link();
        $crawler = $client->click($link);

        $this->assertContains($transport->getId(), $crawler->filter('.content')->filter('table td:first-child')->html());
        $this->assertContains($transport->getFrom()->getName(), $crawler->filter('.content')->filter('table')->html());
        $this->assertContains($this->fixtures->getReference('material-show')->getId(), $crawler->filter('.content')->filter('a')->html());
        $ids = $crawler->filter('table.cast td:first-child')->each(function($node, $i){
            return $node->html();
        });
        $stringIds = implode($ids);
        $this->assertContains($this->fixtures->getReference('part-show-2')->getId(), $stringIds );
        $this->assertNotContains($this->fixtures->getReference('part-show-3')->getId(), $stringIds );
        $this->assertNotContains($this->fixtures->getReference('part-show-1')->getId(), $stringIds );
    }

    public function testTransferDetail(){
         $client = static::makeClient();
        $crawler = $client->request('GET', '/odvoz');
        $form = $crawler->selectButton('transfer_filter_show')->form();
        $form['transfer_filter[start_date]'] = '01.01.2015';
        $form['transfer_filter[end_date]'] = '01.02.2015';
        $crawler = $client->submit($form);
        $transfer = $this->fixtures->getReference('transfer-show');
        $link = $crawler->selectLink($transfer->getId())->link();
        $crawler = $client->click($link);
        $this->assertContains($transfer->getId(), $crawler->filter('.content')->filter('table td:first-child')->html());
        $this->assertContains($transfer->getFrom()->getName(), $crawler->filter('.content')->filter('table')->html());
        $this->assertContains($this->fixtures->getReference('material-show')->getId(), $crawler->filter('.content')->filter('a')->html());
        $ids = $crawler->filter('.content')->filter('table.cast td:first-child')->each(function($node, $i){
            return $node->html();
        });
        $stringIds = implode($ids);
        $this->assertContains($this->fixtures->getReference('part-show-3')->getId(), $stringIds );
        $this->assertNotContains($this->fixtures->getReference('part-show-1')->getId(), $stringIds );
        $this->assertNotContains($this->fixtures->getReference('part-show-2')->getId(), $stringIds );
    }
}
