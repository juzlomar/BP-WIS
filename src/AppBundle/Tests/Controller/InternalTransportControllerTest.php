<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Controller;


use AppBundle\Entity\Material;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\FunctionalTestBundle\Test\WebTestCase;

class InternalTransportControllerTest extends WebTestCase
{
    /** @var  ReferenceRepository */
    private $fixtures;

    public function setUp(){
        $this->fixtures = $this->loadFixtures(array(
            'AppBundle\DataFixtures\ORM\Test\LoadUserData',
            'AppBundle\DataFixtures\ORM\Test\LoadWayOfStoringData',
            'AppBundle\DataFixtures\ORM\Test\LoadFormData',
            'AppBundle\DataFixtures\ORM\Test\LoadWayOfProcessingData',
            'AppBundle\DataFixtures\ORM\Test\LoadAddressData',
            'AppBundle\DataFixtures\ORM\Test\LoadWarehouseData',
            'AppBundle\DataFixtures\ORM\Test\LoadBusinessPartnerData',
            'AppBundle\DataFixtures\ORM\Test\LoadMaterialData',
            'AppBundle\DataFixtures\ORM\Test\LoadPartData',
            'AppBundle\DataFixtures\ORM\Test\LoadInternalTransportData',
        ))->getReferenceRepository();
        $this->loginAs($this->fixtures->getReference('marketa'), 'main');
    }

    private function getMaterial(Material $material, $parts)
    {
        $materialArray = array(
            'id' => $material->getId(),
            'plastics' => $material->getPlastics(),
            'specification' => $material->getSpecification(),
            'color' => $material->getColor(),
            'finalForm' => $material->getFinalForm()->getTitle(),
            'parts' => array()
        );

        foreach($parts as $part){
            $partArray = array(
                'id' => $part->getId(),
                'number' => $part->getNumber(),
                'weightNetto' => $part->getWeightNetto(),
                'weightBrutto' => $part->getWeightBrutto(),
                'currentForm' => $part->getCurrentForm()->getTitle(),
                'wayOfStoring' => $part->getWayOfStoring()->getTitle(),
                'state' => $part->getState(),
                'note' => $part->getNote(),
            );
            array_push($materialArray['parts'], $partArray);
        }

        $businessArray = array(
            'materialPrice' => '30',
            'materials' => array(
                $materialArray,
            ));
        return $businessArray;
    }

    private function merge($materials, $form){
        $values = array_merge_recursive(
            $form->getPhpValues(),
            array(
                'internal_transport' => array(
                    'materials' => $materials
                )
            )
        );
        return $values;
    }

    public function testNewGet(){
        $client = static::makeClient();
        $crawler = $client->request('GET', '/prevoz/novy');
        $this->assertStatusCode(200, $client);

        $this->assertEquals(1, $crawler->filter('form[name="internal_transport"]')->count());
        $this->assertEquals(1, $crawler->filter('input[name="internal_transport[price]"]')->count());
        $this->assertEquals(1, $crawler->filter('input[name="internal_transport[date]"]')->count());
        $this->assertEquals(1, $crawler->filter('select[name="internal_transport[from]"]')->count());
        $this->assertEquals(1, $crawler->filter('select[name="internal_transport[to]"]')->count());
        $this->assertEquals(1, $crawler->filter('button[type="submit"]')->count());
        $this->assertEquals(1, $crawler->filter('button#internal_transport_choose')->count());
        $this->assertEquals(0, $crawler->filter('.content')->filter('li')->count());
    }

    public function testMaterials(){
        $client =static::makeClient();
        $crawler = $client->request('GET', '/prevoz/novy');
        $formButton = $crawler->selectButton('internal_transport_choose');
        $form = $formButton->form();
        $crawler = $client->request('POST', '/preprava/material/internal_transport', $form->getPhpValues(), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ));
        $this->assertTrue($client->getResponse()->isSuccessful(), 'Neuspech ajax zadosti');
        $part = $this->fixtures->getReference('part-preprava');
        $this->assertContains($part->getId(), $crawler->filter('#materials_to_transfer')->html());
    }

    public function testNewInvalid(){
        $client =static::makeClient();
        $crawler = $client->request('GET', '/prevoz/novy');
        $form = $crawler->selectButton('internal_transport_save')->form();
        $form['internal_transport[price]'] = '-1000';
        $form['internal_transport[from]'] = '2';
        $values = $this->merge(array(), $form);
        $values['internal_transport']['to'] = '3';
        $crawler = $client->request($form->getMethod(), $form->getUri(), $values, $form->getPhpFiles());
        $this->assertContains('Přeprava musí obsahovat alespoň jeden materiál', $client->getResponse()->getContent());
        $this->assertContains('Cena dopravy musí být větší než nula.', $crawler->filter('#internal_transport_price')->parents()->parents()->html());
        $this->assertContains('Neexistující, nebo neaktivní provozovna.', $crawler->filter('#internal_transport_to')->parents()->parents()->html());

        $material = $this->fixtures->getReference('material-preprava');
        $material->setId($material->getId() + 10);
        $values = $this->merge(array($this->getMaterial($material, array())), $form);
        $crawler = $client->request($form->getMethod(), $form->getUri(), $values, $form->getPhpFiles());
        $this->assertContains('Materiál musí obsahovat alespoň jednu část', $client->getResponse()->getContent());
        $this->assertContains('Materiál s tímto ID neexistuje', $client->getResponse()->getContent());

        $form['internal_transport[price]'] = '';
        $material->setId($material->getId() - 10);
        $part = $this->fixtures->getReference('part-divide-expedice');
        $part1 = $this->fixtures->getReference('part-zastaveno');
        $material1 = $this->fixtures->getReference('material-zastaveno');
        $materials = array(
            $this->getMaterial($material, array($part)),
            $this->getMaterial($material1, array($part1)),
        );
        $values = $this->merge($materials, $form);
        $crawler = $client->request($form->getMethod(), $form->getUri(), $values, $form->getPhpFiles());
        $this->assertContains('Přeprava musí mít cenu dopravy.', $crawler->filter('#internal_transport_price')->parents()->parents()->html());
        $this->assertContains('Materiál s ID '.$material->getId().' nemá část s tímto ID', $client->getResponse()->getContent());
        $this->assertContains('Část není ve stavu k převozu', $client->getResponse()->getContent());
        $this->assertContains('Materiál není na provozovně Praha - Letnany', $client->getResponse()->getContent());
    }

    public function testNewValid(){
        $client =static::makeClient();
        $crawler = $client->request('GET', '/prevoz/novy');
        $form = $crawler->selectButton('internal_transport_save')->form();
        $form['internal_transport[price]'] = '1000';
        $form['internal_transport[from]'] = '1';
        $form['internal_transport[to]'] = '2';
        $material = $this->fixtures->getReference('material-preprava');
        $part = $this->fixtures->getReference('part-preprava');
        $part->setState('připraveno ke zpracování');
        $values = $this->merge(array($this->getMaterial($material, array($part))), $form);

        $crawler = $client->request($form->getMethod(), $form->getUri(), $values, $form->getPhpFiles());
        $crawler = $client->followRedirect();
        $this->assertContains('Mezistředisková přeprava uložena', $client->getResponse()->getContent());

        $crawler = $client->request('GET', '/material/detail/'.$material->getId());
        $this->assertContains('připraveno ke zpracování', $client->getResponse()->getContent());
        $this->assertContains('Praha - Letnany', $crawler->filter('table')->html());
        $this->assertEquals(2, $crawler->filter('table.cast')->first()->filter('tr')->count());
        $this->assertContains('připraveno ke zpracování', $crawler->filter('table.cast')->first()->html());
    }

    public function testNewDivideMaterial()
    {
        $client =static::makeClient();
        $crawler = $client->request('GET', '/prevoz/novy');
        $form = $crawler->selectButton('internal_transport_save')->form();
        $form['internal_transport[price]'] = '1000';
        $form['internal_transport[from]'] = '1';
        $form['internal_transport[to]'] = '1';
        $material = $this->fixtures->getReference('material-divide');
        $part = $this->fixtures->getReference('part-divide-zpracovani');

        $values = $this->merge(array($this->getMaterial($material, array($part))), $form);

        $crawler = $client->request($form->getMethod(), $form->getUri(), $values, $form->getPhpFiles());
        $this->assertStatusCode(200, $client);

        $this->assertContains('není ve stavu k převozu', $client->getResponse()->getContent());
        $this->assertNotContains('není ve finální podobě', $client->getResponse()->getContent());
        $this->assertNotContains('Mezistředisková přeprava uložena', $client->getResponse()->getContent());

        $part1 = $this->fixtures->getReference('part-divide-expedice');
        $values = $this->merge(array($this->getMaterial($material, array($part1))), $form);

        $crawler = $client->request($form->getMethod(), $form->getUri(), $values, $form->getPhpFiles());
        $crawler = $client->followRedirect();
        $this->assertNotContains('není ve stavu k převozu', $client->getResponse()->getContent());
        $this->assertContains('Mezistředisková přeprava uložena', $client->getResponse()->getContent());

        $crawler = $client->request('GET', '/material/detail/'.$material->getId());

        $this->assertContains('připraveno ke zpracování', $client->getResponse()->getContent());
        $this->assertEquals(2, $crawler->filter('table.cast')->first()->filter('tr')->count());
    }

    public function testShowGet(){
        $client =static::makeClient();
        $crawler = $client->request('GET', '/prevoz');

        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertEquals(1, $crawler->filter('form')->count());
        $this->assertEquals(1, $crawler->filter('form[name="internal_transport_filter"]')->count());
        $this->assertEquals(1, $crawler->filter('input[name="internal_transport_filter[start_date]"]')->count());
        $this->assertEquals(1, $crawler->filter('select[name="internal_transport_filter[from]"]')->count());
        $this->assertEquals(1, $crawler->filter('select[name="internal_transport_filter[to]"]')->count());
        $this->assertEquals(1, $crawler->filter('button[type="submit"]')->count());
    }


    public function testShowPost(){
        $client =static::makeClient();
        $crawler = $client->request('GET', '/prevoz');
        $formButton = $crawler->selectButton('internal_transport_filter_show');
        $form = $formButton->form();
        $form['internal_transport_filter[start_date]'] = '01.01.2015';
        $form['internal_transport_filter[end_date]'] = '01.02.2015';
        $form['internal_transport_filter[from]'] = '2';
        $form['internal_transport_filter[to]'] = '1';
        $crawler = $client->submit($form);

        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertEquals(1, $crawler->filter('form')->count());
        $this->assertEquals(1, $crawler->filter('table')->count());
        $this->assertContains("1000", $crawler->filter('table')->parents()->html());
        $this->assertContains("Plzen", $crawler->filter('table')->parents()->html());
        $this->assertContains("Praha - Letnany", $crawler->filter('table')->html());
        $this->assertGreaterThan(0, $crawler->filter('table')->filter('a')->count());
    }
}
