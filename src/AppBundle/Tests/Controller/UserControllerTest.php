<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Controller;


use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\FunctionalTestBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    /** @var  ReferenceRepository */
    private $fixtures;

    public function setUp(){
        $this->fixtures = $this->loadFixtures(array(
            'AppBundle\DataFixtures\ORM\Test\LoadUserData',
        ))->getReferenceRepository();
      //
    }

    public function testLogin(){
        $client = static::makeClient();
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form();
        $form['_username'] = 'marketa';
        $form['_password'] = 'heslo';
        $crawler = $client->submit($form);
        $crawler = $client->followRedirect();
        $this->assertContains('Neplatné přihlašovací údaje.', $client->getResponse()->getContent());
        $form['_username'] = 'juzlomar';
        $crawler = $client->submit($form);
        $this->assertTrue($client->getResponse()->isRedirect());
        $crawler = $client->followRedirect();
        $this->assertGreaterThan(0, $crawler->filter('.panel-primary')->count());
        return $client;
    }

    public function testShow(){
        $this->loginAs($this->fixtures->getReference('marketa'), 'main');
        $client = static::makeClient();
        $crawler = $client->request('GET', '/uzivatele');

        $this->assertCount(3, $crawler->filter('.list'));
        $this->assertContains('Nový uživatel' , $crawler->filter('.container')->filter('a')->html());
        $this->assertContains('Změnit role' , $crawler->filter('.list')->eq(0)->filter('a')->eq(0)->html());
        $this->assertContains('Správce' , $crawler->filter('.list')->eq(0)->html());
        $this->assertNotContains('Skladník' , $crawler->filter('.list')->eq(0)->html());

        $this->loginAs($this->fixtures->getReference('tomas'), 'main');
        $client = static::makeClient();
        $crawler = $client->request('GET', '/uzivatele');
        $this->assertNotContains('Nový uživatel' , $client->getResponse()->getContent());
        $this->assertNotContains('Změnit role' , $client->getResponse()->getContent());
    }

    public function testRole(){
        $this->loginAs($this->fixtures->getReference('tomas'), 'main');
        $client = static::makeClient();
        $crawler = $client->request('GET', '/uzivatele/simandl/role');
        $this->assertEquals(403, $client->getResponse()->getStatusCode());

        $this->loginAs($this->fixtures->getReference('marketa'), 'main');
        $client = static::makeClient();
        $crawler = $client->request('GET', '/uzivatele/simandl/role');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $form = $crawler->selectButton('role_submit')->form();
        $form['role[roles]'][1]->tick();
        $form['role[roles]'][0]->untick();
        $crawler = $client->submit($form);
        $crawler = $client->followRedirect();
        $this->assertNotContains('Skladník' ,  $crawler->filter('.list')->eq(1)->html());
        $this->assertContains('Zaměstnanec' ,  $crawler->filter('.list')->eq(1)->html());
    }

    public function testDeactivate(){
        $this->loginAs($this->fixtures->getReference('marketa'), 'main');
        $client = static::makeClient();
        $crawler = $client->request('GET', '/uzivatele');
        $link = $crawler->filter('.list')->eq(1)->selectLink('Deaktivovat')->link();
        $crawler = $client->click($link);
        $crawler = $client->followRedirect();
        $this->assertContains('Aktivovat' , $crawler->filter('.list')->eq(1)->html());

        $client->request('GET', '/logout');
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form();
        $form['_username'] = 'simandl';
        $form['_password'] = 'heslo';
        $crawler = $client->submit($form);
        $crawler = $client->followRedirect();
        $this->assertContains('Účet je zakázaný.' , $client->getResponse()->getContent());
    }

    public function testActivate(){
        $this->loginAs($this->fixtures->getReference('marketa'), 'main');
        $client = static::makeClient();
        $crawler = $client->request('GET', '/uzivatele');
        $link = $crawler->filter('.list')->eq(2)->selectLink('Aktivovat')->link();
        $crawler = $client->click($link);
        $crawler = $client->followRedirect();
        $this->assertContains('Deaktivovat' , $crawler->filter('.list')->eq(2)->html());
        $client->request('GET', '/logout');
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('_submit')->form();
        $form['_username'] = 'tereza';
        $form['_password'] = 'heslo';
        $crawler = $client->submit($form);
        $crawler = $client->followRedirect();
        $this->assertNotContains('Účet je zakázaný.' , $client->getResponse()->getContent());
    }
}
