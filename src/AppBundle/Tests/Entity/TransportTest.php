<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Entity;


use AppBundle\Entity\Business;
use AppBundle\Entity\InternalTransport;


class TransportTest extends \PHPUnit_Framework_TestCase
{
    /** @var InternalTransport */
    private $transport;

    public function setUp(){
        $this->transport = new InternalTransport();
    }

    public function testDate(){
        $date = new \DateTime();
        $this->transport->setDate($date);
        $this->assertEquals($date, $this->transport->getDate());
    }

    // todo

    public function testMaterial(){
        $material1 = new Business();
        $material2 = new Business();
        $this->transport->addMaterial($material1);
        $this->transport->addMaterial($material2);
        $this->assertEquals(2, count($this->transport->getMaterials()->toArray()));
    }

}
