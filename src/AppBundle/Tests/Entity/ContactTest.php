<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Entity;


use AppBundle\Entity\Contact;
use AppBundle\Entity\BusinessPartner;
use AppBundle\Entity\Address;

class ContactTest extends \PHPUnit_Framework_TestCase
{
    /** @var  Contact */
    private $contact;

    public function setUp(){
        $this->contact = new Contact();
    }

    public function testFirstname(){
        $name = 'Tereza';
        $this->contact->setFirstname($name);
        $this->assertEquals($name, $this->contact->getFirstname());
    }

    public function testLastname(){
        $name = 'Tereza';
        $this->contact->setLastname($name);
        $this->assertEquals($name, $this->contact->getLastname());
    }

    public function testPhoneNumber(){
        $number = '987654321';
        $this->contact->setPhoneNumber($number);
        $this->assertEquals($number, $this->contact->getPhoneNumber());
    }

    public function testEmail()
    {
        $email = 'Tereza@dr.cz';
        $this->contact->setEmail($email);
        $this->assertEquals($email, $this->contact->getEmail());
    }

    public function testPartner(){
        $partner = new BusinessPartner();
        $this->contact->setPartner($partner);
        $this->assertEquals($partner, $this->contact->getPartner());
    }
}
