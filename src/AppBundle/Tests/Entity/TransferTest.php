<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Entity;


use AppBundle\Entity\Transfer;
use AppBundle\Entity\Warehouse;
use AppBundle\Entity\BusinessPartner;

class TransferTest extends \PHPUnit_Framework_TestCase
{
    /** @var Transfer */
    private $tranfer;

    public function setUp(){
        $this->tranfer = new Transfer();
    }

    public function testFromWarehouse(){
        $warehouse = new Warehouse();
        $this->tranfer->setFrom($warehouse);
        $this->assertEquals($warehouse, $this->tranfer->getFrom());
    }

    public function testTo(){
        $partner = new BusinessPartner();
        $this->tranfer->setTo($partner);
        $this->assertEquals($partner, $this->tranfer->getTo());
    }
}
