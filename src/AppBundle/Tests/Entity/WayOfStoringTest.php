<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Entity;


use AppBundle\Entity\WayOfStoring;
use AppBundle\Entity\Form;

class WayOfStoringTest extends \PHPUnit_Framework_TestCase
{
    /** @var  WayOfStoring */
    private $storing;

    public function setUp(){
        $this->storing = new WayOfStoring();
    }


    public function testTitle(){
        $title = 'v oktabinu';
        $this->storing->setTitle($title);
        $this->assertEquals($title, $this->storing->getTitle());
    }

    public function testAddForm(){
        $form1 =  new Form();
        $this->storing->addForm($form1);
        $form2 =  new Form();
        $this->storing->addForm($form2);

        $this->assertEquals(2, count($this->storing->getForms()->toArray()));
        return [ $this->storing, $form1, $form2 ];
    }

    /**
     * @depends testAddForm
     */
    public function testRemoveForm($data)
    {
        list($storing, $form1, $form2) = $data;
        /** var WayOfStoring $storing */
        $storing->removeForm($form1);

        $this->assertEquals(1, count($storing->getForms()->toArray()));
    }


}
