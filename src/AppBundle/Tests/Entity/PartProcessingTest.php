<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Entity;


use AppBundle\Entity\MaterialPart;
use AppBundle\Entity\PartProcessing;

class PartProcessingTest extends \PHPUnit_Framework_TestCase
{
    /** @var PartProcessing */
    private $partProcessing;

    public function setUp(){
        $this->partProcessing = new PartProcessing();
    }

    public function testMaterial(){
        $material = new MaterialPart();
        $this->partProcessing->setPart($material);
        $this->assertEquals($material, $this->partProcessing->getPart());
    }

    // todo
}
