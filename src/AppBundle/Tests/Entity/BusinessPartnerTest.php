<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Entity;


use AppBundle\Entity\Address;
use AppBundle\Entity\BusinessPartner;
use AppBundle\Entity\Contact;

class BusinessPartnerTest extends \PHPUnit_Framework_TestCase
{
    /** @var  BusinessPartner */
    private $partner;

    public function setUp(){
        $this->partner = new BusinessPartner();
    }

    public function testName(){
        $name = 'Partner';
        $this->partner->setName($name);
        $this->assertEquals($name, $this->partner->getName());
    }

    public function testNote(){
        $note = 'Note';
        $this->partner->setNote($note);
        $this->assertEquals($note, $this->partner->getNote());
    }

    public function testAddress(){
        $address = new Address('Town1', 'PSC1', 'Street1', 21);
        $this->partner->setAddress($address);
        $this->assertEquals($address, $this->partner->getAddress());
    }

    public function testAddContact(){
        $contact1 = new Contact('Firstname', 'Lastname', '123456789', 'email@partner.cz', $this->partner);
        $contact2 = new Contact('Firstname1', 'Lastname1', '123456789', 'email@partner.com', $this->partner);
        $this->partner->addContact($contact1);
        $this->partner->addContact($contact2);
        $this->assertEquals(2, count($this->partner->getContacts()->toArray()));
        return [$this->partner, $contact1, $contact2];
    }

    /**
     * @depends testAddContact
     */
    public function testRemoveContact($data){
        list($partner, $contact1, $contact2) = $data;
        $partner->removeContact($contact1);
        $this->assertEquals(1, count($partner->getContacts()->toArray()));
    }
}
