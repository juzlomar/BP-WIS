<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Entity;


use AppBundle\Entity\Bringing;
use AppBundle\Entity\BusinessPartner;
use AppBundle\Entity\Warehouse;
use AppBundle\Entity\Address;

class BringingTest extends \PHPUnit_Framework_TestCase
{
    /** @var Bringing */
    private $bringing;

    public function setUp(){
        $this->bringing = new Bringing();
    }

    public function testFrom(){
        $partner = new BusinessPartner();
        $this->bringing->setFrom($partner);
        $this->assertEquals($partner, $this->bringing->getFrom());
    }

    public function testTo(){
        $warehouse = new Warehouse();
        $this->bringing->setTo($warehouse);
        $this->assertEquals($warehouse, $this->bringing->getTo());
    }
}
