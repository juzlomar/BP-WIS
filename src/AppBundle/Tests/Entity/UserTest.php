<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Entity;


use AppBundle\Entity\User;

class UserTest extends \PHPUnit_Framework_TestCase
{
    /** @var User */
    private $user;

    public function setUp(){
        $this->user = new User();
    }

    public function testName(){
        $name = 'Tereza';
        $this->user->setName($name);
        $this->assertEquals($name, $this->user->getName());
    }


    public function testPhoneNumber(){
        $number = '987654321';
        $this->user->setPhoneNumber($number);
        $this->assertEquals($number, $this->user->getPhoneNumber());
    }

    public function testEmail(){
        $email = 'Tereza@dr.cz';
        $this->user->setEmail($email);
        $this->assertEquals($email, $this->user->getEmail());
    }

    public function testUsername(){
        $login = 'Tereza';
        $this->user->setUsername($login);
        $this->assertEquals($login, $this->user->getUsername());
    }

    public function testPassword(){
        $password = 'Tereza';
        $this->user->setPassword($password);
        $this->assertEquals($password, $this->user->getPassword());
    }

    public function testRole(){
        $role = 'ROLE_ADMIN';
        $this->user->addRole($role);
        $this->assertContains($role, $this->user->getRoles());
    }

}
