<?php

/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Entity;


use AppBundle\Entity\Form;
use AppBundle\Entity\WayOfProcessing;
use AppBundle\Entity\WayOfStoring;

class FormTest extends \PHPUnit_Framework_TestCase
{
    /** @var  Form */
    private $form;

    public function setUp(){
        $this->form = new Form();
    }


    public function testTitle(){
        $title = 'drt';
        $this->form->setTitle($title);
        $this->assertEquals($title, $this->form->getTitle());
    }

    public function testTitleEng(){
        $title_eng = 'drt_eng';
        $this->form->setTitleEng($title_eng);
        $this->assertEquals($title_eng, $this->form->getTitleEng());
    }

    public function testAddWayOfProcessing(){
        $way1 = new WayOfProcessing();
        $this->form->addWayOfProcessing($way1);
        $way2 = new WayOfProcessing();
        $this->form->addWayOfProcessing($way2);

        $this->assertEquals(2, count($this->form->getWayOfProcessing()->toArray()));
        return [ $this->form, $way1, $way2 ];
    }

    /**
     * @depends testAddWayOfProcessing
     */
    public function testRemoveWayOfProcessing($data)
    {
        list($form, $way1, $way2) = $data;
        /** var Form $form */
        $form->removeWayOfProcessing($way1);

        $this->assertEquals(1, count($form->getWayOfProcessing()->toArray()));
    }

    public function testAddOutputOfProcessing(){
        $way1 = new WayOfProcessing();
        $this->form->addOutputOfProcessing($way1);
        $way2 = new WayOfProcessing();
        $this->form->addOutputOfProcessing($way2);

        $this->assertEquals(2, count($this->form->getOutputOfProcessing()->toArray()));

        return [ $this->form, $way1, $way2 ];
    }

    /**
     * @depends testAddOutputOfProcessing
     */
    public function testRemoveOutputOfProcessing($data)
    {
        list($form, $way1, $way2) = $data;
        /** var Form $form */
        $form->removeOutputOfProcessing($way1);

        $this->assertEquals(1, count($form->getOutputOfProcessing()->toArray()));
    }


    public function testAddWayOfStoring(){
        $way1 = new WayOfStoring();
        $this->form->addWayOfStoring($way1);
        $way2 = new WayOfStoring();
        $this->form->addWayOfStoring($way2);

        $this->assertEquals(2, count($this->form->getWayOfStoring()->toArray()));

        return [ $this->form, $way1, $way2 ];
    }

    /**
     * @depends testAddWayOfStoring
     */
    public function testRemoveWayOfStoring($data)
    {
        list($form, $way1, $way2) = $data;
        /** var Form $form */
        $form->removeWayOfStoring($way1);

        $this->assertEquals(1, count($form->getWayOfStoring()->toArray()));
        return $data;
    }
}
