<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Entity;


use AppBundle\Entity\Warehouse;
use AppBundle\Entity\Address;
use AppBundle\Entity\WayOfProcessing;

class WarehouseTest extends \PHPUnit_Framework_TestCase
{
    /** @var Warehouse */
    private $warehouse;

    public function setUp(){
        $this->warehouse = new Warehouse();
    }

    public function testId(){
        $id = 1;
        $this->warehouse->setId($id);
        $this->assertEquals($id, $this->warehouse->getId());
    }

    public function testName(){
        $name = 'Partner';
        $this->warehouse->setName($name);
        $this->assertEquals($name, $this->warehouse->getName());
    }

    public function testAddress(){
        $address = new Address();
        $this->warehouse->setAddress($address);
        $this->assertEquals($address, $this->warehouse->getAddress());
    }

    public function testAddWayOfProcessing(){
        $way1 = new WayOfProcessing();
        $this->warehouse->addWayOfProcessing($way1);
        $way2 = new WayOfProcessing();
        $this->warehouse->addWayOfProcessing($way2);

        $this->assertEquals(2, count($this->warehouse->getWaysOfProcessing()->toArray()));
        return [ $this->warehouse, $way1, $way2 ];
    }

    /**
     * @depends testAddWayOfProcessing
     */
    public function testRemoveWayOfProcessing($data)
    {
        list($warehouse, $way1, $way2) = $data;
        /** var Warehouse $warehouse */
        $warehouse->removeWayOfProcessing($way1);

        $this->assertEquals(1, count($warehouse->getWaysOfProcessing()->toArray()));
    }
}
