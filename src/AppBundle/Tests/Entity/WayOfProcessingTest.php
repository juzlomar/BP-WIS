<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Entity;

use AppBundle\Entity\Form;
use AppBundle\Entity\Warehouse;
use AppBundle\Entity\WayOfProcessing;

class WayOfProcessingTest extends \PHPUnit_Framework_TestCase
{
    /** @var  WayOfProcessing */
    private $way;

    public function setUp(){
        $this->way = new WayOfProcessing();
    }


    public function testTitle(){
        $title = 'regranulace';
        $this->way->setTitle($title);
        $this->assertEquals($title, $this->way->getTitle());
    }

    public function testOutputForm(){
        $form = new Form();
        $this->way->setOutputForm($form);
        $this->assertEquals($form, $this->way->getOutputForm());
    }

    public function testAddEntranceForm(){
        $form1 = new Form();
        $form2 = new Form();
        $this->way->addEntranceForm($form1);
        $this->way->addEntranceForm($form2);

        $this->assertEquals(2, count($this->way->getEntranceForms()->toArray()));
        return [$this->way, $form1, $form2];
    }

    /**
     * @depends testAddEntranceForm
     */
    public function testRemoveEntranceForm($data)
    {
        list($way, $form1, $form2) = $data;
        /** var WayOfProcessing $way */
        $way->removeEntranceForm($form1);
        $this->assertEquals(1, count($way->getEntranceForms()->toArray()));
    }

    public function testAddWarehouse(){
        $warehouse1 = new Warehouse();
        $warehouse2 = new Warehouse();
        $this->way->addWarehouse($warehouse1);
        $this->way->addWarehouse($warehouse2);
        $this->assertEquals(2, count($this->way->getWarehouses()->toArray()));
        return [$this->way, $warehouse1, $warehouse2];
    }

    /**
     * @depends testAddWarehouse
     */
    public function testRemoveWarehouse($data){
        list($way, $warehouse1, $warehouse2) = $data;
        $way->removeWarehouse($warehouse1);
        $this->assertEquals(1, count($way->getWarehouses()->toArray()));
    }
}
