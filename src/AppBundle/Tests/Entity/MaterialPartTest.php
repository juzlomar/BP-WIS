<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Entity;


use AppBundle\Entity\MaterialPart;

class MaterialPartTest extends \PHPUnit_Framework_TestCase
{
    /** @var MaterialPart */
    private $part;

    public function setUp(){
        $this->part = new MaterialPart();
    }

    public function testNumber(){
        $number = 1;
        $this->part->setNumber($number);
        $this->assertEquals($number, $this->part->getNumber());
    }

    // todo
}
