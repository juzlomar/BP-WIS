<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Entity;


use AppBundle\Entity\InternalTransport;
use AppBundle\Entity\Warehouse;
use AppBundle\Entity\Address;

class InternalTransportTest extends \PHPUnit_Framework_TestCase
{
    /** @var InternalTransport */
    private $transport;

    public function setUp(){
        $this->transport = new InternalTransport();
    }

    public function testFrom(){
        $warehouse = new Warehouse();
        $this->transport->setFrom($warehouse);
        $this->assertEquals($warehouse, $this->transport->getFrom());
    }

    public function testTo(){
        $warehouse = new Warehouse();
        $this->transport->setTo($warehouse);
        $this->assertEquals($warehouse, $this->transport->getTo());
    }
}
