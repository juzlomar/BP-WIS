<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Entity;


use AppBundle\Entity\MaterialPart;
use AppBundle\Entity\PartProcessing;
use AppBundle\Entity\Processing;
use AppBundle\Entity\User;

class ProcessingTest extends \PHPUnit_Framework_TestCase
{
    /** @var Processing */
    private $processing;

    public function setUp(){
        $this->processing = new Processing();
    }

    public function testUser(){
        $user = new User();
        $this->processing->setProcessor($user);
        $this->assertEquals($user, $this->processing->getProcessor());
    }

    public function testInput(){
        $part1 = new PartProcessing();
        $part2 = new PartProcessing();
        $this->processing->addInput($part1);
        $this->processing->addInput($part2);
        $this->assertEquals(2, count($this->processing->getInputs()->toArray()));
    }

    public function testOutput(){
        $part1 = new MaterialPart();
        $part2 = new MaterialPart();
        $this->processing->addOutput($part1);
        $this->processing->addOutput($part2);
        $this->assertEquals(2, count($this->processing->getOutputs()->toArray()));
    }

    //todo
}
