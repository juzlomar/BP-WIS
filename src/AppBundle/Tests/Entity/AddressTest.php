<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Entity;


use AppBundle\Entity\Address;

class AddressTest extends \PHPUnit_Framework_TestCase
{
    /** @var Address */
    private $address;

    public function setUp(){
        $this->address = new Address();
    }

    public function testTown(){
        $town = 'Praha';
        $this->address->setTown($town);
        $this->assertEquals($town, $this->address->getTown());
    }

    public function testLastname(){
        $psc = '98765';
        $this->address->setPsc($psc);
        $this->assertEquals($psc, $this->address->getPsc());
    }

    public function testStreet(){
        $ulice = 'Ecilu';
        $this->address->setStreet($ulice);
        $this->assertEquals($ulice, $this->address->getStreet());
    }

    public function testCp(){
        $cp = 21;
        $this->address->setCp($cp);
        $this->assertEquals($cp, $this->address->getCp());
    }

}
