<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Entity;


use AppBundle\Entity\Material;
use AppBundle\Entity\Warehouse;
use AppBundle\Entity\Address;
use AppBundle\Entity\Form;
use AppBundle\Exception\AttributAlreadySetException;

class MaterialTest extends \PHPUnit_Framework_TestCase
{
    /** @var Material */
    private $material;

    public function setUp(){
        $this->material = new Material();
    }

    public function testPlastics(){
        $plastics = 'PP     HP';
        $this->material->setPlastics($plastics);
        $this->assertEquals('PP+HP', $this->material->getPlastics());
        return $this->material;
    }



    public function testSpecification(){
        $specification = 'specification';
        $this->material->setSpecification($specification);
        $this->assertEquals($specification, $this->material->getSpecification());
        return $this->material;
    }

    public function testSpecificationEng(){
        $specification = 'specification_eng';
        $this->material->setSpecificationEng($specification);
        $this->assertEquals($specification, $this->material->getSpecificationEng());
        return $this->material;
    }

    public function testColor(){
        $color = 'mix';
        $this->material->setColor($color);
        $this->assertEquals($color, $this->material->getColor());
    }

    public function testColorEng(){
        $color = 'mix';
        $this->material->setColorEng($color);
        $this->assertEquals($color, $this->material->getColorEng());
    }
}
