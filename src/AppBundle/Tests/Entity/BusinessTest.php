<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Tests\Entity;


use AppBundle\Entity\Business;
use AppBundle\Entity\InternalTransport;
use AppBundle\Entity\Material;

class BusinessTest extends \PHPUnit_Framework_TestCase
{
    /** @var  Business */
    private $business;

    public function setUp(){
        $this->business = new Business();
    }

    /**
     * @before
     */
    public function testPrize(){
        $prize = 123;
        $this->business->setMaterialPrice($prize);
        $this->assertEquals($prize, $this->business->getMaterialPrice());
    }

    public function testTransport(){
        $transport = new InternalTransport();
        $this->business->setTransport($transport);
        $this->assertEquals($transport, $this->business->getTransport());
    }

    public function testMaterial(){
        $material1 = new Material();
        $material2 = new Material();
        $this->business->addMaterial($material1);
        $this->business->addMaterial($material2);
        $this->assertEquals(2, $this->business->getMaterials()->count());
    }
}
