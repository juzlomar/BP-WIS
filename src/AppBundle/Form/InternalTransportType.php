<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Form;


use AppBundle\Repository\WarehouseRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InternalTransportType extends TransportType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('from', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\Warehouse',
                    'choice_label' => 'name',
                    'label' => 'Z provozovny',
                    'query_builder' => function(WarehouseRepository $wr) {
                        return $wr->findActive();
                    },
                    'invalid_message' => 'Neexistující, nebo neaktivní provozovna.',
                ))
            ->add('to', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\Warehouse',
                    'choice_label' => 'name',
                    'label' => 'Na provozovnu',
                    'query_builder' => function(WarehouseRepository $wr) {
                        return $wr->findActive();
                    },
                    'invalid_message' => 'Neexistující, nebo neaktivní provozovna.',
                ))
            ->add('choose', ButtonType::class,
                array(
                    'label' => 'Vyber material',
                ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver
            ->setDefault('data_class', 'AppBundle\Entity\InternalTransport');
    }
}