<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TransportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('date', DateType::class,
                array(
                    'widget' => 'single_text',
                    'format' => 'dd.MM.yyyy',
                    'data' => new \DateTime(),
                    'label' => 'Datum',
                    'invalid_message' => 'Nesprávný formát data. Datum musí být ve tvaru dd.MM.yyyy.',
                ))
            ->add('price', NumberType::class,
                array(
                    'label' => 'Cena přepravy [Kč]',
                    'invalid_message' => 'Cena dopravy musí být desetinné číslo.',
                ))
            ->add('materials', CollectionType::class,
                array(
                    'entry_type' => BusinessType::class,
                    'allow_add' => true,
                    'by_reference' => false,
                    'prototype_name' => 'business_name',
                    'label' => 'Materiál',
                    'entry_options' => array('states' => $options['states'], 'error_bubbling' => false),
                ))
            ->add('save', SubmitType::class,
                array(
                    'label' => 'Ulož',
                    'validation_groups' => array('Choose', 'Editation', 'NotNew'),
                ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('data_class', 'AppBundle\Entity\Transport')
            ->setDefault('states', array())
            ->setAllowedTypes('states', array('array'));
    }

}