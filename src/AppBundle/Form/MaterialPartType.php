<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MaterialPartType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', IntegerType::class,
                array(
                    'label' => 'ID',
                    'invalid_message' => 'ID materiálu musí být celé číslo.',
                ))
            ->add('number', IntegerType::class,
                array(
                    'label' => 'Počet',
                    'invalid_message' => 'Počet musí být celé číslo.',
                ))
            ->add('weightNetto', NumberType::class,
                array(
                    'label' => 'Váha netto [kg]',
                    'invalid_message' => 'Váha musí být desetinné číslo.',
                    'attr' => array(
                        'class' => 'number',
                    )
                ))
            ->add('weightBrutto', NumberType::class,
                array(
                    'label' => 'Váha brutto [kg]',
                    'invalid_message' => 'Váha musí být desetinné číslo.',
                    'attr' => array(
                        'class' => 'number',
                    ),
                    'required' => false,
                ))
            ->add('currentForm', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\Form',
                    'choice_label' => 'title',
                    'label' => 'Současná podoba',
                    'invalid_message' => 'Neexistující podoba.',
                ))
            ->add('wayOfStoring', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\WayOfStoring',
                    'choice_label' => 'title',
                    'label' => 'Způsob uskladnění',
                    'invalid_message' => 'Neexistující způsob uskladnění.',
                ))
            ->add('state', ChoiceType::class,
                array(
                    'choices' => $options['states'],
                    'label' => 'stav',
                    'choices_as_values' => true,
                    'invalid_message' => 'Část nemůže být v tomto stavu.',
                ))
            ->add('note', TextType::class,
                array(
                    'label' => 'Poznámka',
                    'required' => false,
                ));


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('data_class', 'AppBundle\Entity\MaterialPart')
            ->setDefault('states', null)
            ->setRequired('states')
            ->setAllowedTypes('states', array('array'))
            ->setDefault('error_mapping', array('weightBruttoLegal' => 'weightBrutto'));
    }

}