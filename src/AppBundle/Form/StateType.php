<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class StateType extends AbstractType
{
    private $types;

    public function __construct($types)
    {
        $this->types = $types;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('form', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\Form',
                    'choice_label' => 'title',
                    'label' => 'Podoba',
                    'invalid_message' => 'Neexistující podoba.',
                    'constraints' => new NotBlank(array('message' => 'Zadejte podobu, pro kterou chcete najít stav materiálu.')),
                ))
            ->add('warehouse', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\Warehouse',
                    'choice_label' => 'name',
                    'required' => false,
                    'placeholder' => 'Všechny provozovny',
                    'label' => 'Provozovna',
                    'invalid_message' => 'Neexistující provozovna.',
                ))
            ->add('type', ChoiceType::class,
                array(
                    'choices' => array(
                        'Česky' => $this->types['cz'],
                        'Anglicky' => $this->types['eng'],
                    ),
                    'label' => 'Jazyk',
                    'choices_as_values' => true,
                    'constraints' => new NotBlank(array('message' => 'Zadejte jazyk výkazu.')),
                    'invalid_message' => 'Neexistující jazyk výkazu.',
                ))
            ->add('show', SubmitType::class,
                array(
                    'label' => 'Ukaž'
                ));
    }
}