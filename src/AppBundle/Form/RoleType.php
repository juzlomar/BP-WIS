<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RoleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('roles', ChoiceType::class,
                array(
                    'multiple' => true,
                    'expanded' => true,
                    'choices' => array(
                        'Skladník' => 'ROLE_WAREHOUSEMAN',
                        'Zaměstnanec' => 'ROLE_EMPLOYEE',
                        'Správce' => 'ROLE_ADMIN',
                    ),
                    'choices_as_values' => true,
                    'label' => 'Role',
                ))
            ->add('submit', SubmitType::class);
    }

}