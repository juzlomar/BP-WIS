<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,
                array(
                    'label' => 'Jméno',
                ))
            ->add('phoneNumber', TextType::class,
                array(
                    'label' => 'Telefonní číslo',
                ))
            ->add('roles', ChoiceType::class,
                array(
                    'multiple' => true,
                    'expanded' => true,
                    'choices' => array(
                        'ROLE_WAREHOUSEMAN' => 'Skladník',
                        'ROLE_EMPLOYEE' => 'Zaměstnanec',
                        'ROLE_ADMIN' => 'Správce',
                    ),
                    'label' => 'Role',
                ));
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

}