<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WarehouseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,
                array(
                    'label' => 'Název provozovny',
                ))
            ->add('address', AddressType::class,
                array(
                    'label' => 'Adresa prozovozovny',
                    'error_bubbling' => false,
                ))
            ->add('waysOfProcessing', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\WayOfProcessing',
                    'choice_label' => 'title',
                    'multiple' => true,
                    'required' => false,
                    'expanded' => true,
                    'label' => 'Možné způsoby zpracování',
                    'invalid_message' => 'Neexistující způsob zpracování.'
                ))
            ->add('save', SubmitType::class,
                array(
                    'label' => 'Ulož',
                ));
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event){
                $form = $event->getForm();
                $data = $event->getData();
                if($data->getId() === null){
                    $form->add('id', IntegerType::class,
                        array(
                            'label' => 'ID',
                            'invalid_message' => 'ID provozovny musí být celé číslo.'
                        ));
                }
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('data_class', 'AppBundle\Entity\Warehouse');
    }

}