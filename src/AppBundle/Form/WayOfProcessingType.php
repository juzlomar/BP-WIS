<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WayOfProcessingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('entranceForms', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\Form',
                    'choice_label' => 'title',
                    'label' => 'Vstupní podoby',
                    'by_reference' => false,
                    'multiple' => true,
                    'expanded' => true,
                    'invalid_message' => 'Neexitující podoba.',
                ))
            ->add('outputForm', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\Form',
                    'choice_label' => 'title',
                    'label' => 'Výstupní podoba',
                    'multiple' => false,
                    'invalid_message' => 'Neexistující podoba.',
                ))
            ->add('warehouses', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\Warehouse',
                    'choice_label' => 'name',
                    'label' => 'Provozovna, kde se tak dá materiál zpracovávat',
                    'by_reference' => false,
                    'multiple' => true,
                    'required' => false,
                    'expanded' => true,
                    'invalid_message' => 'Neexistující provozovna.'
                ))
            ->add('save', SubmitType::class,
                array(
                    'label' => 'Ulož',
                ));

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event){
                // this would be your entity, i.e. WayOfProcessing
                $form = $event->getForm();
                $data = $event->getData();
                if($data->getTitle() === null){
                    $form->add('title', TextType::class,
                        array(
                            'label' => 'Název zpracování'
                        ));
                }
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('data_class', 'AppBundle\Entity\WayOfProcessing');
    }


}