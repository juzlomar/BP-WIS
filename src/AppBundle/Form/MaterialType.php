<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class MaterialType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', IntegerType::class,
                array(
                    'label' => 'ID',
                    'invalid_message' => 'ID materiálu musí být celé číslo.',
                ))
            ->add('plastics', TextType::class,
                array(
                    'label' => 'Plasty'
                ))
            ->add('specification', TextType::class,
                array(
                    'label' => 'Specifikace'
                ))
            ->add('specification_eng', TextType::class,
                array(
                    'label' => 'Specifikace v angličtině'
                ))
            ->add('color', TextType::class,
                array(
                    'label' => 'Barva',
                ))
            ->add('color_eng', TextType::class,
                array(
                    'label' => 'Barva v angličtině'
                ))
            ->add('currentForm', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\Form',
                    'choice_label' => 'title',
                    'mapped' => false,
                    'label' => 'Současná podoba',
                    'invalid_message' => 'Neexistující podoba.'
                ))
            ->add('finalForm', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\Form',
                    'choice_label' => 'title',
                    'label' => 'Finální podoba',
                    'invalid_message' => 'Neexistující podoba.'
                ))
            ->add('warehouse', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\Warehouse',
                    'choice_label' => 'name',
                    'label' => 'Současná provozovna',
                    'invalid_message' => 'Neexistující provozovna.'
                ))
            ->add('parts', CollectionType::class,
                array(
                    'entry_type' => MaterialPartType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'prototype_name' => 'part_name',
                    'label' => 'Části materiálu',
                    'entry_options' =>
                        array(
                            'states' => $options['states'],
                            'error_bubbling' => false
                        ),
                ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('data_class','AppBundle\Entity\Material')
            ->setDefault('states', null)
            ->setRequired('states')
            ->setAllowedTypes('states', array('array'));
    }


}