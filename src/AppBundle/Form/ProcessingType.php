<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Form;


use AppBundle\Entity\Warehouse;
use AppBundle\Repository\WarehouseRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProcessingType extends  AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', DateType::class,
                array(
                    'widget' => 'single_text',
                    'format' => 'dd.MM.yyyy',
                    'data' => new \DateTime(),
                    'label' => 'Datum',
                    'invalid_message' => 'Nesprávný formát data. Datum musí být ve tvaru dd.MM.yyyy.',
                ))
            ->add('warehouse', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\Warehouse',
                    'choice_label' => 'name',
                    'label' => 'Provozovna',
                    'query_builder' => function(WarehouseRepository $wr) {
                        return $wr->findActive();
                    },
                    'invalid_message' => 'Neexistující, nebo neaktivní provozovna.',
                ))
            ->add('processor', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\User',
                    'choice_label' => 'name',
                    'label' => 'Zpracoval',
                    'invalid_message' => 'Neexistující, nebo neoprávněný zaměstnanec.',
                ))
            ->add('inputs', CollectionType::class,
                array(
                    'entry_type' => PartProcessingType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'prototype_name' => 'input_name',
                    'label' => 'Zpracovaný materiál',
                    'entry_options' => array('states' => $options['states'], 'error_bubbling' => false),
                ))
            ->add('outputs', CollectionType::class,
                array(
                    'entry_type' => MaterialPartType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'prototype_name' => 'output_name',
                    'label' => 'Novy materiál',
                    'entry_options' => array('states' => $options['states'], 'error_bubbling' => false),
                ))
            ->add('choose', ButtonType::class,
                array(
                    'label' => 'Vyber material',
                ))
            ->add('save', SubmitType::class,
                array(
                    'label' => 'Uloz',
                    'validation_groups' => array('Default')
                ));

        $formModifier = function (FormInterface $form, Warehouse $warehouse = null) {
            $wayOfProcessing = null === $warehouse ? array() : $warehouse->getWaysOfProcessing();

            $form->add('wayOfProcessing', EntityType::class, array(
                'class'       => 'AppBundle\Entity\WayOfProcessing',
                'choice_label' => 'title',
                'label' => 'Způsob zpracování',
                'choices'     => $wayOfProcessing,
                'invalid_message' => 'Na provozovně nemůže být materiál takto zpracován',
            ));
        };
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                // this would be your entity, i.e. Processing
                $data = $event->getData();

                $formModifier($event->getForm(), $data->getWayOfProcessing());
            }
        );

        $builder->get('warehouse')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                // It's important here to fetch $event->getForm()->getData(), as
                // $event->getData() will get you the client data (that is, the ID)
                $warehouse = $event->getForm()->getData();

                // since we've added the listener to the child, we'll have to pass on
                // the parent to the callback functions!
                $formModifier($event->getForm()->getParent(), $warehouse);
            }
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('data_class', 'AppBundle\Entity\Processing')
            ->setDefault('states', array())
            ->setAllowedTypes('states', array('array'));
    }


}