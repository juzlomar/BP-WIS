<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BusinessType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // todo material
        $builder
            ->add('materialPrice', NumberType::class,
                array(
                    'label' => 'Nákupní cena [Kč]',
                    'attr' => array('class' => 'number'),
                    'invalid_message' => 'Cena materiálu musí být desetinné číslo.',
                    ))
            ->add('materials', CollectionType::class,
                array(
                    'entry_type' => MaterialType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'prototype_name' => 'material_name',
                    'entry_options' => array('states' => $options['states'], 'error_bubbling' => false)
                ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('data_class', 'AppBundle\Entity\Business')
            ->setDefault('states', null)
            ->setRequired('states')
            ->setAllowedTypes('states', array('array'));
    }

}