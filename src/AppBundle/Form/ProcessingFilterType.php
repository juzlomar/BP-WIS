<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProcessingFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('start_date', DateType::class,
                array(
                    'label' => 'Od',
                    'widget' => 'single_text',
                    'format' => 'dd.MM.yyyy',
                    'data' => new \DateTime(),
                    'invalid_message' => 'Nesprávný formát data. Datum musí být ve formátu dd.MM.yyyy.',
                    'constraints' => new NotBlank(array('message' => 'Zadejte datum, od kterého chcete zpracování zobrazit.')),
                ))
            ->add('end_date', DateType::class,
                array(
                    'label' => 'Do',
                    'widget' => 'single_text',
                    'format' => 'dd.MM.yyyy',
                    'data' => new \DateTime(),
                    'invalid_message' => 'Nesprávný formát data. Datum musí být ve formátu dd.MM.yyyy.',
                    'constraints' => new NotBlank(array('message' => 'Zadejte datum, od kterého chcete zpracování zobrazit.')),
                ))
            ->add('warehouse', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\Warehouse',
                    'choice_label' => 'name',
                    'label' => 'Prozovona',
                    'required' => false,
                    'placeholder' => 'Všechny provozovny',
                    'invalid_message' => 'Neexistující provozovna',
                ))
            ->add('wayOfProcessing', EntityType::class, array(
                'class'       => 'AppBundle\Entity\WayOfProcessing',
                'choice_label' => 'title',
                'label' => 'Způsob zpracování',
                'required' => false,
                'placeholder' => 'Všechny způsoby',
                'invalid_message' => 'Neexistující způsob zpracování',
            ))
            ->add('show', SubmitType::class,
                array(
                    'label' => 'Ukaž',
                ));
    }

}