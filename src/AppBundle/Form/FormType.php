<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title_eng', TextType::class,
                array(
                    'label' => 'Název v angličtině',
                ))
            ->add('wayOfProcessing', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\WayOfProcessing',
                    'choice_label' => 'title',
                    'multiple' => true,
                    'required' => false,
                    'expanded' => true,
                    'label' => 'Lze zpracovat',
                    'invalid_message' => 'Neexistující způsob zpracování.',
                ))
            ->add('wayOfStoring', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\WayOfStoring',
                    'choice_label' => 'title',
                    'multiple' => true,
                    'required' => false,
                    'expanded' => true,
                    'label' => 'Lze uskladnit',
                    'invalid_message' => 'Neexistující způsob uskladnění.'
                ))
            ->add('save', SubmitType::class,
                array(
                    'label' => 'Ulož',
                ));

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event){
                $form = $event->getForm();
                $data = $event->getData();
                if($data->getTitle() === null){
                    $form->add('title', TextType::class,
                        array(
                            'label' => 'Název podoby'
                        ));
                }
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('data_class', 'AppBundle\Entity\Form');
    }

}