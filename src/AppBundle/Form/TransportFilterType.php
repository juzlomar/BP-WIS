<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class TransportFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('start_date', DateType::class,
                array(
                    'label' => 'Od',
                    'widget' => 'single_text',
                    'format' => 'dd.MM.yyyy',
                    'data' => new \DateTime(),
                    'invalid_message' => 'Nesprávný formát data. Datum musí být ve formátu dd.MM.yyyy.',
                    'constraints' => new NotBlank(array('message' => 'Zadejte datum, od kterého chcete přepravu zobrazit.')),
                ))
            ->add('end_date', DateType::class,
                array(
                    'label' => 'Do',
                    'widget' => 'single_text',
                    'format' => 'dd.MM.yyyy',
                    'data' => new \DateTime(),
                    'invalid_message' => 'Nesprávný formát data. Datum musí být ve formátu dd.MM.yyyy.',
                    'constraints' => new NotBlank(array('message' => 'Zadejte datum, do kterého chcete přepravu zobrazit.')),
                ))
            ->add('show', SubmitType::class,
                array(
                    'label' => 'Ukaž',
                ));
    }

}