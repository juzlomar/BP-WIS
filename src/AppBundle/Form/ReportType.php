<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class ReportType extends AbstractType
{
    private $types;

    /**
     * ReportType constructor.
     * @param $types
     */
    public function __construct($types)
    {
        $this->types = $types;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('start_date', DateType::class,
                array(
                    'label' => 'Od',
                    'widget' => 'single_text',
                    'format' => 'dd.MM.yyyy',
                    'data' => new \DateTime(),
                    'invalid_message' => 'Nesprávný formát data. Datum musí být ve formátu dd.MM.yyyy.',
                    'constraints' => new NotBlank(array('message' => 'Zadejte datum, od kterého chcete výkaz zobrazit.')),
                ))
            ->add('end_date', DateType::class,
                array(
                    'label' => 'Do',
                    'widget' => 'single_text',
                    'format' => 'dd.MM.yyyy',
                    'data' => new \DateTime(),
                    'invalid_message' => 'Nesprávný formát data. Datum musí být ve formátu dd.MM.yyyy.',
                    'constraints' => new NotBlank(array('message' => 'Zadejte datum, do kterého chcete výkaz zobrazit.')),
                ))
            ->add('type', ChoiceType::class,
                array(
                    'choices' => array(
                        'Navezený materiál' => $this->types['bringing'],
                        'Odvezený materiál' => $this->types['transfer'],
                        'Zpracovaný materiál' => $this->types['processing'],
                    ),
                    'label' => 'Typ výkazu',
                    'choices_as_values' => true,
                    'constraints' => new NotBlank(array('message' => 'Zadejte jazyk výkazu.')),
                    'invalid_message' => 'Neexistující typ výkazu.',
                ))
            ->add('warehouse', EntityType::class,
                array(
                    'label' => 'Provozovna',
                    'class' => 'AppBundle\Entity\Warehouse',
                    'choice_label' => 'name',
                    'constraints' => new NotBlank(array('message' => 'Zadejte provozovnu, pro kterou chcete výkaz zobrazit.')),
                    'invalid_message' => 'Neexistující provozovna'
                ))
            ->add('show', SubmitType::class,
                array(
                    'label' => 'Ukaž'
                ));
    }


}