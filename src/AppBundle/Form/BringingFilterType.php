<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;


class BringingFilterType extends TransportFilterType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('from', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\BusinessPartner',
                    'choice_label' => 'name',
                    'label' => 'Odesílatel',
                    'required' => false,
                    'placeholder' => 'Všichni partneři',
                    'invalid_message' => 'Neexistující partner.',
                ))
            ->add('to', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\Warehouse',
                    'choice_label' => 'name',
                    'label' => 'Na provozovnu',
                    'required' => false,
                    'placeholder' => 'Všechny provozovny',
                    'invalid_message' => 'Neexistující, nebo neaktivní provozovna.',

                ));
        parent::buildForm($builder, $options);

    }


}