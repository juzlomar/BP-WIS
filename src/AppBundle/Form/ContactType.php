<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class,
                array(
                    'label' => 'Jméno',
                ))
            ->add('lastname', TextType::class,
                array(
                    'label' => 'Příjmení',
                ))
            ->add('phoneNumber', TextType::class,
                array(
                    'label' => 'Telefonní číslo',
                ))
            ->add('email', EmailType::class,
                array(
                    'label' => 'Email',
                    'required' => false,
                ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
       $resolver
           ->setDefault('data_class', 'AppBundle\Entity\Contact');
    }


}