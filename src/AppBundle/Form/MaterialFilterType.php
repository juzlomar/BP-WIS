<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MaterialFilterType extends AbstractType
{

    public function buildform(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('warehouse', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\Warehouse',
                    'choice_label' => 'name',
                    'multiple' => true,
                    'expanded' => true,
                    'required' => true,
                    'label' => 'Současná provozovna',
                ))
            ->add('finalForm', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\Form',
                    'choice_label' => 'title',
                    'multiple' => true,
                    'expanded' => true,
                    'label' => 'Finální podoba',
                ))
            ->add('plastics', ChoiceType::class,
                array(
                    'choices' => $options['plastics'],
                    'choices_as_values' => true,
                    'multiple' => true,
                    'data' => $options['plastics'],
                    'label' => 'Plasty',
                ))
            ->add('specification', ChoiceType::class,
                array(
                    'choices' => $options['specifications'],
                    'choices_as_values' => true,
                    'multiple' => true,
                    'data' => $options['specifications'],
                    'label' => 'Specifikace',
                ))
            ->add('color', ChoiceType::class,
                array(
                    'choices' => $options['colors'],
                    'choices_as_values' => true,
                    'multiple' => true,
                    'data' => $options['colors'],
                    'label' => 'Barva',
                ))
            ->add('currentForm', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\Form',
                    'choice_label' => 'title',
                    'multiple' => true,
                    'expanded' => true,
                    'label' => 'Současná podoba',
                ))
            ->add('wayOfStoring', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\WayOfStoring',
                    'choice_label' => 'title',
                    'multiple' => true,
                    'expanded' => true,
                    'label' => 'Způsob uskladnění',
                ))
            ->add('state', ChoiceType::class,
                array(
                    'choices' => $options['states'],
                    'choices_as_values' => true,
                    'multiple' => true,
                    'expanded' => true,
                    'data' => $options['states'],
                    'label' => 'Stav',
                ))
            ->add('from', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\BusinessPartner',
                    'choice_label' => 'name',
                    'multiple' => true,
                    'required' => false,
                    'label' => 'Návoz od partnera',
                ))
            ->add('to', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\BusinessPartner',
                    'choice_label' => 'name',
                    'multiple' => true,
                    'required' => false,
                    'label' => 'Odvoz k partnerovi',
                ))
            ->add('reset', ResetType::class)
            ->add('show', SubmitType::class,
                array(
                    'label' => 'Ukaž',
                ));



    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('plastics', null)
            ->setRequired('plastics')
            ->setAllowedTypes('plastics', array('array'))
            ->setDefault('specifications', null)
            ->setRequired('specifications')
            ->setAllowedTypes('specifications', array('array'))
            ->setDefault('colors', null)
            ->setRequired('colors')
            ->setAllowedTypes('colors', array('array'))
            ->setDefault('states', null)
            ->setRequired('states')
            ->setAllowedTypes('states', array('array'))
        ;
    }
}