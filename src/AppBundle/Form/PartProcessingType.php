<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PartProcessingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('price', NumberType::class,
                array(
                    'label' => 'Cena zpracování [Kč]',
                    'invalid_message' => 'Cena zpracování musí být desetinné číslo.'
                ))
            ->add('part', MaterialPartType::class,
                array(
                    'states' => $options['states'],
                    'error_bubbling' => false,
                ))
            ->add('note', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Poznamka ke zpracování'
                ));
        $builder->setErrorBubbling(false);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('data_class', 'AppBundle\Entity\PartProcessing')
            ->setDefault('states', array())
            ->setAllowedTypes('states', array('array'));
    }


}