<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class BusinessPartnerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,
                array(
                    'label' => 'Název',
                ))
            ->add('address', AddressType::class,
                array(
                    'label' => 'Adresa',
                    'error_bubbling' => false,
                ))
            ->add('note', TextareaType::class,
                array(
                    'label' => 'Poznámka',
                    'required' => false,
                ))
            ->add('contacts', CollectionType::class,
                array(
                    'entry_type' => ContactType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'prototype_name' => 'contact_name',
                    'label' => 'Kontakty',
                    'entry_options' => array('error_bubbling' => false)
                ))
            ->add('save', SubmitType::class,
                array(
                    'label' => 'Ulož',
                ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('data_class', 'AppBundle\Entity\BusinessPartner');
    }

}