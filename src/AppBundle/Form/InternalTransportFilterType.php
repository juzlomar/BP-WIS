<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;

class InternalTransportFilterType extends TransportFilterType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('from', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\Warehouse',
                    'choice_label' => 'name',
                    'label' => 'Z provozovny',
                    'required' => false,
                    'placeholder' => 'Všechny provozovny',
                    'invalid_message' => 'Neexistující provozona.',
                ))
            ->add('to', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\Warehouse',
                    'choice_label' => 'name',
                    'label' => 'Na provozovnu',
                    'required' => false,
                    'placeholder' => 'Všechny provozovny',
                    'invalid_message' => 'Neexistující provozona.',
                ));
        parent::buildForm($builder, $options);
    }

}