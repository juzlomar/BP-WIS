<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class WayOfProcessing
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WayOfProcessingRepository")
 * @ORM\Table(name="WayOfProcessing")
 * @UniqueEntity(fields="title", message="Způsob zpracování s tímto názvem už existuje.")
 * @package AppBundle\Entity
 */
class WayOfProcessing
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=30)
     * @Assert\NotBlank(message="Způsob zpracování musí mít název.")
     * @var string
     */
    private $title;
    /**
     * @ORM\ManyToMany(
     *     targetEntity="Form",
     *     mappedBy="wayOfProcessing"
     * )
     * @var ArrayCollection<Form>
     */
    private $entranceForms;
    /**
     * @ORM\ManyToOne(
     *     targetEntity="Form",
     *     inversedBy="outputOfProcessing"
     * )
     * @ORM\JoinColumn(
     *     name="form_title",
     *     referencedColumnName="title"
     * )
     * @var Form
     */
    private $outputForm;
    /**
     * @ORM\ManyToMany(
     *     targetEntity="Warehouse",
     *     mappedBy="waysOfProcessing"
     * )
     * @var ArrayCollection<Warehouse>
     */
    private $warehouses;

    /**
     * WayOfProcessing constructor.
     */
    public function __construct()
    {
        $this->entranceForms = new ArrayCollection();
        $this->warehouses = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getEntranceForms()
    {
        return $this->entranceForms;
    }

    public function addEntranceForm(Form $form){
        $this->entranceForms[] = $form;
        if (! $form->getWayOfProcessing()->contains($this)) {
            $form->addWayOfProcessing($this);
        }
        return $this;
    }

    public function removeEntranceForm(Form $form){
        $this->entranceForms->removeElement($form);
        if ($form->getWayOfProcessing()->contains($this)){
            $form->removeWayOfProcessing($this);
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return Form
     */
    public function getOutputForm()
    {
        return $this->outputForm;
    }

    /**
     * @param Form $outputForm
     */
    public function setOutputForm($outputForm)
    {
        $this->outputForm = $outputForm;
    }

    /**
     * @return ArrayCollection
     */
    public function getWarehouses()
    {
        return $this->warehouses;
    }

    /**
     * @param Warehouse $warehouse
     */
    public function addWarehouse(Warehouse $warehouse)
    {
        $this->warehouses[] = $warehouse;
        if (! $warehouse->getWaysOfProcessing()->contains($this)) {
            $warehouse->addWayOfProcessing($this);
        }
        return $this;
    }

    public function removeWarehouse(Warehouse $warehouse){
        $this->warehouses->removeElement($warehouse);
        if ($warehouse->getWaysOfProcessing()->contains($this)){
            $warehouse->removeWayOfProcessing($this);
        }
        return $this;
    }

    /**
     * @Assert\IsTrue(message="Výstupní podoba nesmí být stejná jako vstupní.")
     * @return bool
     */
    public function isOutputFormLegal(){
        if ($this->entranceForms->contains($this->outputForm))
            return false;
        return true;
    }


}