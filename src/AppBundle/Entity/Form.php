<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class Form
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FormRepository")
 * @ORM\Table(name="Form")
 * @UniqueEntity(fields="title", message="Podoba s tímto názvem už existuje.")
 * @package AppBundle\Entity
 */
class Form
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(message="Podoba musí mít název.")
     * @var string
     */
    private $title;
    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(message="Podoba musí mít anglický název.")
     * @var string
     */
    private $title_eng;
    /**
     * @ORM\ManyToMany(
     *     targetEntity="WayOfProcessing",
     *     inversedBy="entranceForms"
     * )
     * @ORM\JoinTable(
     *     name="forms_wayOfProcessing",
     *     joinColumns={@ORM\JoinColumn(name="form_title", referencedColumnName="title")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="wayOfProcessing_title", referencedColumnName="title")}
     * )
     * @var ArrayCollection<WayOfProcessing>
     */
    private $wayOfProcessing;
    /**
     * @ORM\OneToMany(
     *     targetEntity="WayOfProcessing",
     *     mappedBy="outputForm"
     * )
     * @var ArrayCollection<WayOfProcessing>
     */
    private $outputOfProcessing;
    /**
     * @ORM\ManyToMany(
     *     targetEntity="WayOfStoring",
     *     inversedBy="forms"
     * )
     * @ORM\JoinTable(
     *     name="forms_wayOfStoring",
     *     joinColumns={@ORM\JoinColumn(name="form_title", referencedColumnName="title")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="wayOfStoring_title", referencedColumnName="title")}
     * )
     * @var ArrayCollection<WayOfStoring>
     */
    private $wayOfStoring;

    /**
     * @ORM\ManyToMany(targetEntity="Form")
     * @ORM\JoinTable(
     *     name="final_forms",
     *     joinColumns={@ORM\JoinColumn(name="current_form_title", referencedColumnName="title")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="final_form_title", referencedColumnName="title")}
     * )
     * @var ArrayCollection<Form>
     */
    private $finalForms;

    public $visited = false;

    /**
     * @return ArrayCollection
     */
    public function getFinalForms()
    {
        return $this->finalForms;
    }

    /**
     * @param ArrayCollection $finalForms
     */
    public function addFinalForm(Form $finalForm)
    {
        $this->finalForms[] = $finalForm;
        return $this;
    }

    public function removeFinalForm(Form $finalForm){
        $this->finalForms->removeElement($finalForm);
        return $this;
    }

    /**
     * Form constructor.
     */
    public function __construct()
    {
        $this->wayOfStoring = new ArrayCollection();
        $this->wayOfProcessing = new ArrayCollection();
        $this->outputOfProcessing = new ArrayCollection();
        $this->finalForms = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitleEng()
    {
        return $this->title_eng;
    }

    /**
     * @param string $title_eng
     */
    public function setTitleEng($title_eng)
    {
        $this->title_eng = $title_eng;
    }

    /**
     * @return ArrayCollection
     */
    public function getWayOfProcessing()
    {
        return $this->wayOfProcessing;
    }

    public function addWayOfProcessing(WayOfProcessing $wayOfProcessing){
        $this->wayOfProcessing[] = $wayOfProcessing;
        if (! $wayOfProcessing->getEntranceForms()->contains($this)) {
            $wayOfProcessing->addEntranceForm($this);
        }
        return $this;
    }

    public function removeWayOfProcessing(WayOfProcessing $wayOfProcessing){
        $this->wayOfProcessing->removeElement($wayOfProcessing);
        if ($wayOfProcessing->getEntranceForms()->contains($this)){
            $wayOfProcessing->removeEntranceForm($this);
        }
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getOutputOfProcessing()
    {
        return $this->outputOfProcessing;
    }

    public function addOutputOfProcessing(WayOfProcessing $wayOfProcessing){
        $this->outputOfProcessing[] = $wayOfProcessing;
        return $this;
    }

    public function removeOutputOfProcessing(WayOfProcessing $wayOfProcessing){
        $this->outputOfProcessing->removeElement($wayOfProcessing);
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getWayOfStoring()
    {
        return $this->wayOfStoring;
    }

    public function addWayOfStoring(WayOfStoring $wayOfStoring){
        $this->wayOfStoring[] = $wayOfStoring;
        if (! $wayOfStoring->getForms()->contains($this)) {
            $wayOfStoring->addForm($this);
        }
        return $this;
    }

    public function removeWayOfStoring(WayOfStoring $wayOfStoring){
        $this->wayOfStoring->removeElement($wayOfStoring);
        if ($wayOfStoring->getForms()->contains($this)){
            $wayOfStoring->removeForm($this);
        }
        return $this;
    }
}