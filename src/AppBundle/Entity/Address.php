<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Address
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AddressRepository")
 * @ORM\Table(name="Address")
 * @package AppBundle\Entity
 */
class Address
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

     /**
      * @ORM\Column(type="string", length=30)
      * @Assert\NotBlank(message="Adresa musí mít město.")
      * @var string */
    private $town;
    /**
     * @ORM\Column(type="string", length=5)
     * @Assert\NotBlank(message="Adresa musí mít PSČ.")
     * @Assert\Regex(
     *     pattern="/^\d{5}$/",
     *     message="PSČ musí být pětimístné celé číslo."
     * )
     * @var string */
    private $psc;
    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(message="Adresa musí mít ulici.")
     * @var string */
    private $street;
    /**
     * @ORM\Column(type="smallint")
     * @Assert\NotBlank(message="Adresa musí mít číslo popisné.")
     * @Assert\Type(
     *     type="integer",
     *     message="ČP musí být celé číslo."
     * )
     * @Assert\GreaterThan(
     *     value=0,
     *     message="ČP musí být kladné."
     * )
     * @var number */
    private $cp;

    /**
     * @return string
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * @param string $town
     */
    public function setTown($town)
    {
        $this->town = $town;
    }

    /**
     * @return string
     */
    public function getPsc()
    {
        return $this->psc;
    }

    /**
     * @param string $psc
     */
    public function setPsc($psc)
    {
        $this->psc = $psc;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return number
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * @param number $cp
     */
    public function setCp($cp)
    {
        $this->cp = $cp;
    }


}