<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Material
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MaterialRepository")
 * @ORM\Table(name="Material")
 * @package AppBundle\Entity
 */
class Material
{
    /**
     * Identification number of the material.
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="bigint")
     * @Assert\NotBlank(
     *     groups={"Choose"},
     *     message="Materiál musí mít ID."
     * )
     * @Assert\Type(
     *     type="integer",
     *     message="ID materiálu musí být celé číslo.",
     *     groups={"Choose"}
     * )
     * @var number
     */
    private $id;

    /**
     * Plastics which is the material from.
     * @ORM\Column(type="string")
     * @Assert\NotBlank(
     *     groups={"New"},
     *     message="Materiál musí mít plasty."
     * )
     * @Assert\Type(
     *     type="string",
     *     groups={"New"},
     *     message="Plasty musí být řetězec."
     * )
     * @var string
     */
    private $plastics;

    /**
     * Specification of the original product which is the material from.
     * @ORM\Column(type="string")
     * @Assert\NotBlank(
     *     groups={"New"},
     *     message="Materiál musí mít spedifikaci."
     * )
     * @Assert\Type(
     *     type="string",
     *     groups={"New"},
     *     message="Specifikace musí být řetězec."
     * )
     * @var string
     */
    private $specification;

    /**
     * Specification of the original product which is the material from in English.
     * @ORM\Column(type="string")
     * @Assert\NotBlank(
     *     groups={"New"},
     *     message="Materiál musí mít anglický překlad specifikace."
     * )
     * @Assert\Type(
     *     type="string",
     *     groups={"New"},
     *     message="Specifikace musí být řetězec."
     * )
     * @var string
     */
    private $specification_eng;

    /**
     * Color of the material.
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Materiál musí mít barvu.")
     * @Assert\Type(
     *     type="string",
     *     groups={"New"},
     *     message="Barva musí být řetězec."
     * )
     * @var string
     */
    private $color;

    /**
     * English name of the material color.
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Materiál musí mít anglický překlad barvy.")
     * @Assert\Type(
     *     type="string",
     *     message="Barva musí být řetězec."
     * )
     * @var string
     */
    private $color_eng;

    /**
     * Final form of the material;
     * @ORM\ManyToOne(targetEntity="Form")
     * @ORM\JoinColumn(
     *     name="form_title",
     *     referencedColumnName="title"
     * )
     * @Assert\NotBlank(message="Materiál musí mít finální podobu.")
     * @var Form
     */
    private $finalForm;

    /**
     * Warehouse where the material now is
     * @ORM\ManyToOne(targetEntity="Warehouse")
     * @var Warehouse
     */
    private $warehouse;

    /**
     * Sales made with the material
     * @ORM\ManyToMany(
     *     targetEntity="Business",
     *      mappedBy="materials",
     *     fetch="EXTRA_LAZY"
     * )
     * @var ArrayCollection<Businnes>
     */
    private $businesses;

    /**
     * Parts of the material
     * @ORM\OneToMany(
     *     targetEntity="MaterialPart",
     *     mappedBy="material",
     *     cascade={"all"}
     * )
     * @Assert\Valid
     * @Assert\Count(
     *     min=1,
     *     groups={"Default", "Choose"},
     *     minMessage="Materiál musí obsahovat alespoň jednu část"
     * )
     * @var ArrayCollection<MaterialPart>
     */
    private $parts;


    /**
     * Material constructor.
     */
    public function __construct()
    {
        $this->businesses = new ArrayCollection();
        $this->parts = new ArrayCollection();
    }

    /**
     * @param number $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * @return number
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPlastics()
    {
        return $this->plastics;
    }

    /**
     * @param string $plastics
     */
    public function setPlastics($plastics)
    {
      //  if ($this->plastics != null) throw new AttributAlreadySetException();
        $this->plastics = preg_replace('!\s+!', '+', $plastics);
    }

    /**
     * @return string
     */
    public function getSpecification()
    {
        return $this->specification;
    }


    /**
     * @param string $specification
     */
    public function setSpecification($specification)
    {
     //   if ($this->specification != null) throw new AttributAlreadySetException();
        $this->specification = $specification;
    }


    /**
     * @return string
     */
    public function getSpecificationEng()
    {
        return $this->specification_eng;
    }

    /**
     * @param string $specification_eng
     */
    public function setSpecificationEng($specification_eng)
    {
      //  if ($this->specification_eng != null) throw new AttributAlreadySetException();
        $this->specification_eng = $specification_eng;
    }


    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * @param string $color_eng
     */
    public function setColorEng($color_eng)
    {
        $this->color_eng = $color_eng;
    }


    /**
     * @return string
     */
    public function getColorEng()
    {
        return $this->color_eng;
    }

    /**
     * @return Form
     */
    public function getFinalForm()
    {
        return $this->finalForm;
    }

    /**
     * @param Form $finalForm
     */
    public function setFinalForm($finalForm)
    {
        $this->finalForm = $finalForm;
    }

    /**
     * @return Warehouse
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }

    /**
     * @param Warehouse $warehouse
     */
    public function setWarehouse($warehouse)
    {
        $this->warehouse = $warehouse;
    }

    /**
     * @return ArrayCollection
     */
    public function getBusinesses()
    {
        return $this->businesses;
    }

    /**
     * @param Business $business
     * @return $this
     */
    public function addBusiness(Business $business){
        $this->businesses[] = $business;
        if (! $business->getMaterials()->contains($this)) {
            $business->addMaterial($this);
        }
        return $this;
    }

    public function removeBusiness(Business $business){
        $this->businesses->removeElement($business);
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getParts()
    {
        return $this->parts;
    }

    public function hasPart($id){
        if ($id === null) return null;
        return $this->parts->filter(function($part) use ($id){
            return $part->getId() == $id;
        })->first();
    }

    /**
     * @param MaterialPart $part
     * @return $this
     */
    public function addPart(MaterialPart $part){
        $this->parts[] = $part;
        $part->setMaterial($this);
        return $this;
    }

    /**
     * @param MaterialPart $part
     * @return $this
     */
    public function removePart(MaterialPart $part){
        $this->parts->removeElement($part);
        return $this;
    }

    public function __toString(){
        return 'Material';
    }

    function __clone()
    {

        $this->id = null;
        $this->parts = new ArrayCollection();
    }


}