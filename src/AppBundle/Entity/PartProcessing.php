<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class PartProcessing
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PartProcessingRepository")
 * @ORM\Table(name="PartProcessing")
 * @package AppBundle\Entity
 */
class PartProcessing
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="bigint")
     * @var number
     */
    private $id;
    /**
     * @ORM\OneToOne(targetEntity="MaterialPart", mappedBy="inputOfProcessing")
     * @Assert\NotBlank(message="Zpracování musí obsahovat alespoň jednu část.")
     * @var MaterialPart
     */
    private $part;
    /**
     * @ORM\ManyToOne(targetEntity="Processing", inversedBy="inputs")
     * @var Processing
     */
    private $processing;
    /**
     * @ORM\Column(type="float", precision=2)
     * @Assert\NotBlank(message="Zpracování části materiálu musí mít cenu.")
     * @Assert\Type(
     *     type="float",
     *     message="Cena zpracování musí být desetinné číslo.",
     * )
     * @Assert\GreaterThanOrEqual(
     *     value=0,
     *     message="Cena zpracování musí být větší nebo rovna nule.",
     * )
     * @var float
     */
    private $price;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    private $note;

    /**
     * @return MaterialPart
     */
    public function getPart()
    {
        return $this->part;
    }

    /**
     * @param MaterialPart $part
     */
    public function setPart($part)
    {
        $this->part = $part;
    }

    /**
     * @return Processing
     */
    public function getProcessing()
    {
        return $this->processing;
    }

    /**
     * @param Processing $processing
     */
    public function setProcessing($processing)
    {
        $this->processing = $processing;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * @return number
     */
    public function getId()
    {
        return $this->id;
    }

}