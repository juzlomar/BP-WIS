<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class InternalTransport
 * @ORM\Entity
 * @package AppBundle\Entity
 */
class InternalTransport extends Transport
{
    /**
     * @ORM\ManyToOne(targetEntity="Warehouse")
     * @ORM\JoinColumn(name="internal_from")
     * @Assert\NotBlank(
     *     groups={"Choose"},
     *     message="Mezistředisková přeprava musí mít provozovnu, ze které je materiál odvážen."
     * )
     * @var Warehouse
     */
    private $from;
    /**
     * @ORM\ManyToOne(targetEntity="Warehouse")
     * @ORM\JoinColumn(name="internal_to")
     * @Assert\NotBlank(
     *     groups={"Choose"},
     *     message="Mezistředisková přeprava musí mít provozovnu, na kterou je materiál přivážen."
     * )
     * @var Warehouse
     */
    private $to;

    /**
     * @return Warehouse
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param Warehouse $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return Warehouse
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param Warehouse $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }
}