<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class BusinessPartner
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BusinessPartnerRepository")
 * @ORM\Table(name="BusinessPartner")
 * @UniqueEntity(fields="name", message="Partner s tímto názvem už existuje.")
 * @package AppBundle\Entity
 */
class BusinessPartner
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @var
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=50, unique=true)
     * @Assert\NotBlank(message="Partner musí mít název.")
     * @var  string */
    private $name;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @var  string */
    private $note;
    /**
     * @ORM\OneToMany(
     *     targetEntity="Contact",
     *     mappedBy="partner",
     *     cascade={"all"}
     * )
     * @Assert\Valid
     * @var  ArrayCollection<Contact> */
    private $contacts;
    /**
     * @ORM\OneToOne(
     *     targetEntity="Address",
     *     cascade={"all"}
     * )
     * @Assert\NotBlank(message="Partner musí mít adresu.")
     * @Assert\Valid
     * @var  Address */
    private $address;

    /**
     * BusinessPartner constructor.
     */
    public function __construct()
    {
        $this->contacts = new ArrayCollection();
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * @return ArrayCollection
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * @param $contact
     * @return $this
     */
    public function addContact(Contact $contact)
    {
        $this->contacts[] = $contact;
        return $this;
    }

    public function removeContact(Contact $contact){
        $this->contacts->removeElement($contact);
        return $this;
    }
    /**
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param Address $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }


}