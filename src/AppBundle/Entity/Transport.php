<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TransportRepository")
 * @ORM\Table(name="Transport")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"navoz" = "Bringing", "odvoz" = "Transfer", "mezistrediskova" = "InternalTransport"})
 * @package AppBundle\Entity
 */
abstract class Transport
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="bigint")
     */
    protected $id;
    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank(
     *     groups={"Default", "Choose"},
     *     message="Přeprava musí mít datum převozu."
     * )
     * @Assert\Date(
     *     groups={"Default", "Choose"},
     *     message="Datum přepravy musí bát datum."
     * )
     * @var  DateTime */
    protected $date;
    /**
     * @ORM\Column(type="float", precision=2)
     * @Assert\NotBlank(
     *     groups={"Default", "Choose"},
     *     message="Přeprava musí mít cenu dopravy."
     * )
     * @Assert\Type(
     *     type="float",
     *     groups={"Default", "Choose"},
     *     message="Cena dopravy musí bát desetinné číslo."
     * )
     * @Assert\GreaterThan(
     *     value=0,
     *     groups={"Default", "Choose"},
     *     message="Cena dopravy musí být větší než nula."
     * )
     * @var  float */
    protected $price;

    /**
     * @ORM\Column(type="float", precision=2, nullable=true)
     * @var float
     */
    protected $pricePerKg;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Business",
     *     mappedBy="transport",
     *     cascade={"persist"}
     * )
     * @Assert\Valid
     * @Assert\Count(
     *     min=1,
     *     groups={"Default", "Choose"},
     *     minMessage="Přeprava musí obsahovat alespoň jeden materiál"
     * )
     * @var  ArrayCollection<Business>
     */
    protected $materials;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getPricePerKg()
    {
        return $this->pricePerKg;
    }

    public function countPrizePerKg()
    {
        $weight = 0;
        foreach($this->materials as $business){
            foreach($business->getMaterials() as $material ){
                foreach($material->getParts() as $part){
                    $weight += $part->getWeightNetto();
                }
            }
        }

        $this->pricePerKg =  $this->price / $weight;
    }


    /**
     * Transport constructor.
     */
    public function __construct()
    {
        $this->materials = new ArrayCollection();
    }


    /**
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return ArrayCollection
     */
    public function getMaterials()
    {
        return $this->materials;
    }

    public function addMaterial(Business $business){
        $this->materials[] = $business;
        $business->setTransport($this);
        return $this;
    }

    /**
     * @param Business $business
     */
    public function removeMaterial(Business $business)
    {
        $this->materials->removeElement($business);
    }

    abstract public function getFrom();
    abstract public function setFrom($from);
    abstract public function getTo();
    abstract public function setTo($to);
}