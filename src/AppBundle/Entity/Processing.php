<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Processing
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProcessingRepository")
 * @ORM\Table(name="Processing")
 * @package AppBundle\Entity
 */
class Processing
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="bigint")
     * @var number
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="WayOfProcessing")
     * @ORM\JoinColumn(name="way_title", referencedColumnName="title")
     * @Assert\NotBlank(message="Zpracování musí probíhat nějakým způsobem.", groups={"Default", "Choose"})
     * @var WayOfProcessing
     */
    private $wayOfProcessing;
    /**
     * @ORM\OneToMany(targetEntity="PartProcessing", mappedBy="processing", cascade={"all"})
     * @Assert\Valid
     * @Assert\Count(
     *     min=1,
     *     minMessage="Zpracování musí obsahovat alespoň jednu část."
     * )
     * @var ArrayCollection<PartProcessing>
     */
    private $inputs;
    /**
     * @ORM\OneToMany(targetEntity="MaterialPart", mappedBy="outputOfProcessing")
     * @Assert\Valid
     * @Assert\Count(
     *     min=1,
     *     minMessage="Zpracovaním musí vzniknout alespoň jedna část."
     * )
     * @var ArrayCollection<MaterialPart>
     */
    private $outputs;
    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank(message="Zpracování musí mít datum.")
     * @Assert\Date(message="Datum zpracování musí být validní datum.")
     * @var DateTime
     */
    private $date;
    /**
     * @ORM\ManyToOne(targetEntity="User", fetch="EAGER")
     * @Assert\NotBlank(message="Zpracování musí mít zpracovatele.")
     * @var User
     */
    private $processor;

    /**
     * @ORM\ManyToOne(targetEntity="Warehouse")
     * @Assert\NotBlank(message="Zpracování musí probíhat na nějaké provozovně.", groups={"Default", "Choose"})
     * @var Warehouse
     */
    private $warehouse;

    /**
     * @return number
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return Warehouse
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }

    /**
     * @param Warehouse $warehouse
     */
    public function setWarehouse($warehouse)
    {
        $this->warehouse = $warehouse;
    }

    /**
     * Processing constructor.
     * @param WayOfProcessing $wayOfProcessing
     * @param DateTime $date
     * @param User $processor
     */
    public function __construct()
    {
        $this->inputs = new ArrayCollection();
        $this->outputs = new ArrayCollection();
    }


    /**
     * @return WayOfProcessing
     */
    public function getWayOfProcessing()
    {
        return $this->wayOfProcessing;
    }

    /**
     * @param WayOfProcessing $wayOfProcessing
     */
    public function setWayOfProcessing($wayOfProcessing)
    {
        $this->wayOfProcessing = $wayOfProcessing;
    }


    /**
     * @return ArrayCollection
     */
    public function getInputs()
    {
        return $this->inputs;
    }

    public function addInput(PartProcessing $partProcessing){
        $this->inputs[] = $partProcessing;
        return $this;
    }

    public function removeInput(PartProcessing $partProcessing){
        $this->inputs->removeElement($partProcessing);
    }

    /**
     * @return ArrayCollection
     */
    public function getOutputs()
    {
        return $this->outputs;
    }

    public function addOutput(MaterialPart $part){
        $this->outputs[] = $part;
        return $this;
    }

    public function removeOutput(MaterialPart $part){
        $this->outputs->removeElement($part);
    }

    /**
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }


    /**
     * @return User
     */
    public function getProcessor()
    {
        return $this->processor;
    }

    /**
     * @param User $processor
     */
    public function setProcessor($processor)
    {
        $this->processor = $processor;
    }


}