<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Class Bringing
 * @ORM\Entity
 * @package AppBundle\Entity
 */
class Transfer extends Transport
{
    /**
     * @ORM\ManyToOne(targetEntity="Warehouse")
     * @ORM\JoinColumn(name="from_warehouse")
     * @Assert\NotBlank(
     *     groups={"Choose"},
     *     message="Návoz musí mít partnera, od kterého je materiál přivezen."
     * )
     * @var Warehouse
     */
    private $from;
    /**
     * @ORM\ManyToOne(targetEntity="BusinessPartner")
     * @ORM\JoinColumn(name="to_partner")
     * @Assert\NotBlank(
     *     groups={"Choose"},
     *     message="Odvoz musí mít partnera, ke kterému je materiál odvezen."
     * )
     * @var BusinessPartner
     */
    private $to;

    /**
     * @return Warehouse
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param Warehouse $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return BusinessPartner
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param BusinessPartner $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }


}