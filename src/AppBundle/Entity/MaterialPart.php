<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class MaterialPart
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MaterialPartRepository")
 * @ORM\Table(name="MaterialPart")
 * @package AppBundle\Entity
 */
class MaterialPart
{
    /**
     * MaterialPart identification
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="bigint")
     * @Assert\NotBlank(
     *     groups={"Choose"},
     *     message="Část musí mít ID."
     * )
     * @Assert\Type(
     *     type="integer",
     *     message="ID materiálu musí být celé číslo.",
     *     groups={"Choose"}
     * )
     * @var number
     */
    private $id;

    /**
     * Number of the parts that are logged together
     * @ORM\Column(name="nmbr", type="integer")
     * @Assert\NotBlank(message="Zadejte počet kusů uskladněných pohromadě.")
     * @Assert\Type(
     *     type="integer",
     *     message="Počet musí být celé číslo."
     * )
     * @Assert\GreaterThan(
     *     value=0,
     *     message="Počet musí být větší než 0."
     * )
     * @var number
     */
    private $number;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Material",
     *     inversedBy="parts",
     *     cascade={"persist", "merge"}
     * )
     * @var Material
     */
    private $material;

    /**
     * MaterialPart weight without packaging
     * @ORM\Column(type="float", precision=2)
     * @Assert\NotBlank(message="Část musí mít váhu netto.")
     * @Assert\Type(
     *     type="float",
     *     message="Váha musí být desetinné číslo."
     * )
     * @var float
     */
    private $weightNetto;

    /**
     * MaterialPart weight with packaging
     * @ORM\Column(type="float", precision=2)
     * @Assert\Type(
     *     type="float",
     *     message="Váha musí být desetinné číslo."
     * )
     * @var float
     */
    private $weightBrutto;

    /**
     * State of the part
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(
     *     groups={"Default", "Choose"},
     *     message="Část musí být v nějakém stavu."
     * )
     * @var string
     */
    private $state;

    /**
     * Note to part
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    private $note;

    /**
     * Current form
     * @ORM\ManyToOne(targetEntity="Form")
     * @ORM\JoinColumn(
     *     name="form_title",
     *     referencedColumnName="title"
     * )
     * @var Form
     */
    private $currentForm;

    /**
     * Way how the part is currently stored
     * @ORM\ManyToOne(targetEntity="WayOfStoring")
     * @ORM\JoinColumn(
     *     name="way_title",
     *     referencedColumnName="title"
     * )
     * @Assert\NotBlank(message="Část musí být nějak uskladěná.")
     * @var WayOfStoring
     */
    private $wayOfStoring;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Processing",
     *     inversedBy="outputs"
     * )
     * @var Processing
     */
    private $outputOfProcessing;

    /**
     * @ORM\OneToOne(
     *     targetEntity="PartProcessing",
     *     inversedBy="part"
     * )
     * @var PartProcessing
     */
    private $inputOfProcessing;

    /**
     * @param number $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * @return number
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return number
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param number $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return Material
     */
    public function getMaterial()
    {
        return $this->material;
    }

    /**
     * @param Material $material
     */
    public function setMaterial($material)
    {
        $this->material = $material;
    }

    /**
     * @return float
     */
    public function getWeightNetto()
    {
        return $this->weightNetto;
    }

    /**
     * @param float $weightNetto
     */
    public function setWeightNetto($weightNetto)
    {
        $this->weightNetto = $weightNetto;
    }


    /**
     * @return float
     */
    public function getWeightBrutto()
    {
        return $this->weightBrutto;
    }

    /**
     * @param float $weightBrutto
     */
    public function setWeightBrutto($weightBrutto)
    {
        $this->weightBrutto = $weightBrutto;
    }

    /**
     * @Assert\NotBlank(message="Váha brutto musí byt větší nebo rovna váze netto.")
     */
    public function isWeightBruttoLegal(){
        if ($this->getWeightBrutto() === null) return true;
        if ($this->getWeightNetto() > $this->getWeightBrutto()) return false;
        return true;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * @return Form
     */
    public function getCurrentForm()
    {
        return $this->currentForm;
    }

    /**
     * @param Form $currentForm
     */
    public function setCurrentForm($currentForm)
    {
        $this->currentForm = $currentForm;
    }

    /**
     * @return WayOfStoring
     */
    public function getWayOfStoring()
    {
        return $this->wayOfStoring;
    }

    /**
     * @param WayOfStoring $wayOfStoring
     */
    public function setWayOfStoring($wayOfStoring)
    {
        $this->wayOfStoring = $wayOfStoring;
    }

    /**
     * @return Processing
     */
    public function getOutputOfProcessing()
    {
        return $this->outputOfProcessing;
    }

    /**
     * @param Processing $outputOfProcessing
     */
    public function setOutputOfProcessing($outputOfProcessing)
    {
        $this->outputOfProcessing = $outputOfProcessing;
    }

    /**
     * @return PartProcessing
     */
    public function getInputOfProcessing()
    {
        return $this->inputOfProcessing;
    }

    /**
     * @param PartProcessing $inputOfProcessing
     */
    public function setInputOfProcessing($inputOfProcessing)
    {
        $this->inputOfProcessing = $inputOfProcessing;
    }

}