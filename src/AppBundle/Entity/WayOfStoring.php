<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class WayOfStoring
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WayOfStoringRepository")
 * @ORM\Table(name="WayOfStoring")
 * @UniqueEntity(fields="title", message="Způsob uskladnění s tímto názvem už existuje.")
 * @package AppBundle\Entity
 */
class WayOfStoring
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=30)
     * @Assert\NotBlank(message="Způsob uskladnění musí mít název.")
     * @var string
     */
    private $title;
    /**
     * @ORM\ManyToMany(
     *     targetEntity="Form",
     *     mappedBy="wayOfStoring"
     * )
     * @var ArrayCollection<Form>
     */
    private $forms;

    /**
     * WayOfStoring constructor.
     * @param string $title
     */
    public function __construct()
    {
        $this->forms = new ArrayCollection();
    }

    /**php app/console doctrine:schema:validateTransfer
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return ArrayCollection
     */
    public function getForms()
    {
        return $this->forms;
    }

   public function addForm(Form $form){
       $this->forms[] = $form;
       if (! $form->getWayOfStoring()->contains($this)) {
           $form->addWayOfStoring($this);
       }
       return $this;
   }

    public function removeForm(Form $form){
        $this->forms->removeElement($form);
        if ($form->getWayOfStoring()->contains($this)){
            $form->removeWayOfStoring($this);
        }
        return $this;
    }

}