<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Class Bringing
 * @ORM\Entity
 * @package AppBundle\Entity
 */
class Bringing extends Transport
{
    /**
     * @ORM\ManyToOne(targetEntity="BusinessPartner")
     * @ORM\JoinColumn(name="from_partner")
     * @Assert\NotBlank(message="Návoz musí mít partnera, od kterého je materiál přivezen.")
     * @var BusinessPartner
     */
    private $from;
    /**
     * @ORM\ManyToOne(targetEntity="Warehouse")
     * @ORM\JoinColumn(name="to_warehouse")
     * @Assert\NotBlank(message="Návoz musí mít provozovnu, na kterou je materiál přivezen.")
     * @var Warehouse
     */
    private $to;

    /**
     * @return BusinessPartner
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param BusinessPartner $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return Warehouse
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param Warehouse $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }

}