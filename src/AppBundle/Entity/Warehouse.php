<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class Warehouse
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WarehouseRepository")
 * @ORM\Table(name="Warehouse")
 * @UniqueEntity(fields="id", message="Provozovna s tímto ID už existuje.")
 * @UniqueEntity(fields="name", message="Provozovna s tímto názvem už existuje.")
 * @package AppBundle\Entity
 */
class Warehouse
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="Provozovna musí mít identifikátor.")
     * @Assert\Type(
     *     type="integer",
     *     message="ID provozovny musí být celé číslo."
     * )
     * @Assert\GreaterThan(
     *     value=0,
     *     message="ID provozovny musí být větší než nula."
     * )
     * @var number
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=50, unique=true)
     * @Assert\NotBlank(message="Provozovna musí mít název.")
     * @var string
     */
    private $name;
    /**
     * @ORM\OneToOne(
     *     targetEntity="Address",
     *     cascade={"all"}
     * )
     * @Assert\NotBlank(message="Provozovna musí být na nějaké adrese.")
     * @Assert\Valid
     * @var Address
     */
    private $address;
    /**
     * @ORM\ManyToMany(targetEntity="WayOfProcessing", inversedBy="warehouses")
     * @ORM\JoinTable(
     *     name="warehouses_wayOfProcessing",
     *     inverseJoinColumns={@ORM\JoinColumn(name="wayOfProcessing_title", referencedColumnName="title")}
     * )
     * @var  ArrayCollection<WayOfProcessing> */
    private $waysOfProcessing;

    /**
     * @ORM\Column(type="boolean")
     * @var
     */
    private $isActive = true;

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * Warehouse constructor.
     */
    public function __construct()
    {
        $this->materials = new ArrayCollection();
        $this->waysOfProcessing = new ArrayCollection();
    }

    /**
     * @return number
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param number $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param Address $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return ArrayCollection
     */
    public function getMaterials()
    {
        return $this->materials;
    }

    public function addMaterial(Material $material){
        $this->materials[] = $material;
        return $this;
    }

    public function removeMaterial(Material $material){
        $this->materials->removeElement($material);
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getWaysOfProcessing()
    {
        return $this->waysOfProcessing;
    }

    public function addWayOfProcessing(WayOfProcessing $wayOfProcessing){
        $this->waysOfProcessing[] = $wayOfProcessing;
        return $this;
    }

    public function removeWayOfProcessing(WayOfProcessing $wayOfProcessing){
        $this->waysOfProcessing->removeElement($wayOfProcessing);
        return $this;
    }

}