<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Contact
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContactRepository")
 * @ORM\Table(name="Contact")
 * @package AppBundle\Entity
 */


class Contact
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     * @Assert\NotBlank(message="Kontakt musí mít křestní jméno kontaktní osoby.")
     * @var string
     */
    private $firstname;
    /**
     * @ORM\Column(type="string", length=30)
     * @Assert\NotBlank(message="Kontakt musí mít příjmení jméno kontaktní osoby.")
     * @var  string
     */
    private $lastname;
    /**
     * @ORM\Column(type="string", length=15)
     * @Assert\NotBlank(message="Kontakt musí mít telefonní číslo kontaktní osoby.")
     * @Assert\Regex(
     *     pattern="/^\d{9}$/",
     *     message= "Telefonní číslo musí být devítimístné číslo."
     * )
     * @var  string */
    private $phoneNumber;
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Assert\Email()
     * @var  string */
    private $email;
    /**
     * @ORM\ManyToOne(targetEntity="BusinessPartner", inversedBy="contacts")
     * @var  BusinessPartner */
    private $partner;


    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return BusinessPartner
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * @param BusinessPartner $partner
     */
    public function setPartner($partner)
    {
        $this->partner = $partner;
    }



}