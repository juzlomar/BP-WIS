<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class Business
 * @ORM\Table(name="Business")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BusinessRepository")
 * @package AppBundle\Entity
 */
class Business
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="bigint")
     */
    private $id;
    /**
     * @ORM\ManyToMany(
     *     targetEntity="Material",
     *     inversedBy="businesses"
     * )
     * @Assert\Valid
     * @Assert\Count(
     *     min=1,
     *     groups={"Default", "Choose"},
     *     minMessage="Přeprava musí obsahovat alespoň jeden materiál"
     * )
     * @var  ArrayCollection<Material> */
    private $materials;
    /**
     * @ORM\ManyToOne(
     *     targetEntity="Transport",
     *     inversedBy="materials"
     * )
     * @var  Transport */
    private $transport;
    /**
     * @ORM\Column(type="float", precision=2)
     * @Assert\NotBlank(
     *     groups={"Default", "Choose"},
     *     message="Materiál musí mít nákupní cenu."
     * )
     * @Assert\Type(
     *     type="float",
     *     groups={"Default", "Choose"},
     *     message="Cena materiálu musí být desetinné číslo"
     * )
     * @var  double */
    private $materialPrice;

    /**
     * Business constructor.
     */
    public function __construct()
    {
        $this->materials = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getMaterials()
    {
        return $this->materials;
    }

    public function addMaterial(Material $material){
        $this->materials[] = $material;
        if (! $material->getBusinesses()->contains($this)) {
            $material->addBusiness($this);
        }
        return $this;
    }

    public function removeMaterial(Material $material){
        $this->materials->removeElement($material);
        return $this;
    }
    /**
     * @return Transport
     */
    public function getTransport()
    {
        return $this->transport;
    }

    /**
     * @param Transport $transport
     */
    public function setTransport($transport)
    {
        $this->transport = $transport;
    }

    /**
     * @return float
     */
    public function getMaterialPrice()
    {
        return $this->materialPrice;
    }

    /**
     * @param float $materialPrice
     */
    public function setMaterialPrice($materialPrice)
    {
        $this->materialPrice = $materialPrice;
    }

}