
/*
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

const FORM_NAME = 'business_partner';
const CONTACT_NAME = 'contacts';
const CONTACT_PROTOTYPE_NAME = /contact_name/g;
const SEPARATOR = '_';
const LINK_NAME = 'new';

jQuery(document).ready(function(){

    var $collectionHolder;

    // setup an "add a tag" link
    var contactLink = FORM_NAME + SEPARATOR + CONTACT_NAME + SEPARATOR + 'new';
    var addContactLink = $('<a href="#" id=' + contactLink + ' class="add_link btn">Přidat kontakt</a>');
    var addContactLinkLi = $('<li></li>').append(addContactLink)
    var $newLinkDiv = $('<ul class="col-md-2 col-sm-4 list-inline"></ul>').append(addContactLink);
    // Get the ul that holds the collection of tags
    $collectionHolder = $('div#business_partner_contacts');

    // add the "add a tag" anchor and li to the tags ul
    $collectionHolder.append($newLinkDiv);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find('.contact').length);

    addContactLink.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();


        // add a new tag form (see next code block)
        addContactForm($collectionHolder, $newLinkDiv);
    });

    $collectionHolder.find('.contact').each(function() {
        addContactFormDeleteLink($(this));
    });

    function addContactForm($collectionHolder, $newLinkDiv) {
        // Get the data-prototype explained earlier
        var prototype = $collectionHolder.data('prototype');
        // get the new index
        var index = $collectionHolder.data('index');

        // Replace '__name__' in the prototype's HTML to
        // instead be a number based on how many items we have
        var newForm = prototype.replace(CONTACT_PROTOTYPE_NAME, index);


        // increase the index with one for the next item
        $collectionHolder.data('index', index + 1);

        // Display the form in the page in an li, before the "Add a tag" link li
        $newLinkDiv.before(newForm);
        var $newFormDiv = $('#' + FORM_NAME + SEPARATOR + CONTACT_NAME + SEPARATOR + index);
        addContactFormDeleteLink($newFormDiv);
    }

    function addContactFormDeleteLink($contactFormDiv) {
        var $removeFormA = $('<a href="#" class="delete_link btn">Odstranit kontakt</a>');
        var $removeFormALi = $('<li></li>').append($removeFormA)
        var $removeFormADiv = $('<ul class="col-md-2 col-sm-4 list-inline pull-right"></ul>').append($removeFormALi);
        $contactFormDiv.append($removeFormADiv);

        $removeFormA.on('click', function(e) {
            // prevent the link from creating a "#" on the URL
            e.preventDefault();

            // deactivate the li for the tag form
            $contactFormDiv.remove();
        });
    }

});
