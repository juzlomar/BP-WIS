
/*
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

var $inputsMaterials;
var $outputMaterials;
var $materials;
const PART_REGEX = /choose_material\[materials\]\[\d+\]\[parts\]\[\d+\]/g;
const PART_NAME = 'processing[inputs][partId][part]';
const INPUT_PROTOTYPE_NAME = /input_name/g;
const FORM_NAME = 'processing';
const OUTPUT_NAME = 'outputs';
const OUTPUT_PROTOTYPE_NAME = /output_name/g;
const SEPARATOR = '_';

jQuery(document).ready(function(){
    $inputsMaterials = $('#processing_inputs');
    $outputMaterials = $('#processing_outputs');
    $materials = $('#materials_to_process');

    $('#processing_choose').bind('click', function (e){
        e.preventDefault();
        var $form = $(this).closest('form');
        getMaterials($form);
    });

    var $warehouse = $('#processing_warehouse');
    var $wayOfProcessing = $('#processing_wayOfProcessing option:selected');
    if($wayOfProcessing.length == 0) {
        getProcessing($warehouse);
    }
    $warehouse.change(function(){
        getProcessing($warehouse);
    });

    $inputsMaterials.find('.material').each(function(){
        var $material = $(this);
        $material.find('.part').each(function(){
            var $part = $(this);
            var $removeButton = $('<a href="#" class="remove_link btn">Odeber ze zpracování</a>');
            var $li = $('<li></li>').append($removeButton);
            var $div = $('<ul class="col-md-2 col-sm-4 list-inline remove_button"></ul>').append($li);
            $part.append($div);
            $removeButton.on('click', function(e){
                e.preventDefault();
                $part.remove();
                if ( ! $material.find('.part').length) $material.remove();
            });
        });
    });

    function getProcessing($warehouse){
        var $form = $($warehouse).closest('form');
        // Simulate form data, but only include the selected sport value.
        var data = {};
        data[$warehouse.attr('name')] = $warehouse.val();
        // Submit data via AJAX to the form's action path.
        $.ajax({
            url : $form.attr('action'),
            type: $form.attr('method'),
            data : data,
            success: function(html) {
                // Replace current position field ...
                $('#processing_wayOfProcessing').replaceWith(
                    // ... with the returned one from the AJAX response.
                    $(html).find('#processing_wayOfProcessing')
                );

                //  getCurrentForm($('#processing_wayOfProcessing'), warehouse);
                // Position field now displays the appropriate positions.
            }
        });

    }

    function getMaterials($form){
        if ($inputsMaterials.find('.material').length){
            if ( !confirm('Vybranim noveho materialu vymazete stavajici material ze zpracovani\n' +
                    'Prejete si pokracovat?') ){
                return;
            }
        }
        $.ajax({
            type: $form.attr('method'),
            url: '/zpracovani/material',
            data:  $form.serialize(),
            success: function(html){
                $materials.empty();
                if ($(html).find('#materials_to_process').find('.material').length > 0){
                    $materials.replaceWith(
                        $(html).find('#materials_to_process')
                    );
                    $materials = $('#materials_to_process');
                    addButtons($materials);
                }else if ($(html).find('#materials_to_process').find('.material').length == 0) {
                    alert('Žádný materiál na provozovně k tomuto způsobu zpracování není k dispozici');
                }
                addErrors(html);
                $inputsMaterials.empty();
            },
            error: function(){
                alert('error');
            }
        });
    }

    function addErrors(html){
        var $divWarehouse = $(html).find('#processing_warehouse').parent();
        if($('#processing_warehouse').parent().find('ul').length > 0){
            $('#processing_warehouse').parent().find('ul').replaceWith(
                $divWarehouse.find('ul')
            );
        }else{
            $('#processing_warehouse').parent().find('label').after(
                $divWarehouse.find('ul')
            );
        }
        var $divWay = $(html).find('#processing_wayOfProcessing').parent();
        if($('#processing_wayOfProcessing').parent().find('ul').length > 0){
            $('#processing_wayOfProcessing').parent().find('ul').replaceWith(
                $divWay.find('ul')
            );
        }else{
            $('#processing_wayOfProcessing').parent().find('label').after(
                $divWay.find('ul')
            );
        }
    }

    addOutputLink();

    $outputMaterials.find('.part').each(function() {
        addPartFormDeleteLink($(this));
    });

});

