
/*
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

jQuery(document).ready(function(){
    const FORM_NAME = $('form').attr('name');
    const MATERIAL_REGEX = /choose_material\[materials\]\[\d+\]/g;
    const MATERIAL_FORM = FORM_NAME +'[materials][materialId][materials][materialId]';
    const PART_REGEX = /choose_material\[materials\]\[\d+\]\[parts\]\[\d+\]/g;
    const PART_NAME = FORM_NAME +'[materials][materialId][materials][materialId][parts][partId]';
    const BUSINESS_PROTOTYPE_NAME = /business_name/g;
    var $businessHolder = $('#' + FORM_NAME + '_materials');

    var formMaterialId = '#' + FORM_NAME + '_materials';
    var $formMaterials = $(formMaterialId);
    var $materials = $('#materials_to_transfer');

    $('#'+ FORM_NAME +'_choose').bind('click', function (e){
        e.preventDefault();
        var $form = $(this).closest('form');
        getMaterials($form);
    });

    $formMaterials.find('.material').each(function(){
        var $material = $(this);
        $material.find('.part').each(function(){
            var $part = $(this);
            var $removeButton = $('<a href="#" class="remove button">Odeber z přepravy</a>');
            var $div = $('<div></div>').append($removeButton);
            $part.append($div);
            $removeButton.on('click', function(e){
                e.preventDefault();
                $part.remove();
                if ( ! $material.find('.part').length ) $material.remove();
            });
        });
    });

    function getMaterials($form){
        if ($formMaterials.find('.material').length){
            if ( !confirm('Vybráním nového materiálu vymažete stávající materiál z přepravy\n' +
                    'Přejete si pokračovat?') ){
                return;
            }
        }

        $.ajax({
            type: $form.attr('method'),
            url: '/preprava/material/' + FORM_NAME,
            data:  $form.serialize(),
            success: function(html){
                $materials.empty();
                if ($(html).find('#materials_to_transfer').find('.material').length > 0){
                    $materials.replaceWith(
                        $(html).find('#materials_to_transfer')
                    );
                    $materials = $('#materials_to_transfer');
                    addButtons($materials);
                }else if ($(html).find('#materials_to_transfer').find('ul').length == 0) {
                    alert('Na provozovně není žádný materiál');
                }
                addErrors(html);
                $formMaterials.empty();
            },
            error: function(){
                alert('error');
            }
        });
    }

    function addErrors(html){
        var warehouseId = '#' + FORM_NAME + '_from';
        var $divWarehouse = $(html).find(warehouseId).parent();
        if($(warehouseId).parent().find('ul').length > 0){
            $(warehouseId).parent().find('ul').replaceWith(
                $divWarehouse.find('ul')
            );
        }else{
            $(warehouseId).parent().find('label').after(
                $divWarehouse.find('ul')
            );
        }
    }

    function addButtons($materials) {
        $materials.find('.material').each(function () {
            var $material = $(this);
            $material.find('.part').each(function () {
                var $part = $(this);
                var $addButton = $('<a href="#" class="add button">Přidej do přepravy</a>');
                var $removeButton = $('<a href="#" class="remove button hidden">Odeber z přepravy</a>');
                var $addLi = $('<li></li>').append($addButton);
                var $removeLi = $('<li></li>').append($removeButton);
                var $div = $('<ul class="col-md-2 col-sm-4 list-inline remove_button buttons"></ul>').append($addLi).append($removeLi);
                $part.append($div);
                $addButton.on('click', function (e) {
                    e.preventDefault();
                    $(this).addClass('hidden');
                    addToTransfer($part, $material);
                    $removeButton.removeClass('hidden');
                });
                $removeButton.on('click', function (e) {
                    e.preventDefault();
                    $(this).addClass('hidden');
                    removeFromTransport($part, $material);
                    $addButton.removeClass('hidden');
                });

            })
        })
    }

    function addToTransfer($part, $material){
        $part.addClass(FORM_NAME);
        var materialId = $material.find('.id').val();
        var $divNewMaterial = $('#'+ FORM_NAME + '_material_' + materialId);
        if (! $divNewMaterial.length ) {
            $divNewMaterial = copyMaterial($material, materialId);
        }
        addPartToTransfer();

        function copyMaterial() {
            var prototype = $businessHolder.data('prototype');
            var $newForm = prototype.replace(BUSINESS_PROTOTYPE_NAME, materialId);
            var newMaterial = $material.html();
            newMaterial = newMaterial.replace(MATERIAL_REGEX, MATERIAL_FORM.replace(/materialId/g, materialId));
            var $divNewMaterial = $("<div id='material_" + materialId + "' class='material'></div>").append(newMaterial);
            $divNewMaterial.prepend($newForm);
            $formMaterials.append($divNewMaterial);
            $divNewMaterial.find('.part').remove();
            return $divNewMaterial;
        }

        function addPartToTransfer(){
            var $buttons = $('<div></div>').append($part.find('.buttons'));
            var text = $part.html();
            var partId = $part.find('.id').val();
            text = text.replace(PART_REGEX, PART_NAME.replace(/materialId/g, materialId).replace(/partId/g, partId));
            $part.html(text);
            $part.append($buttons.find('.buttons'));
            $divNewMaterial.find('.parts').append($part);
            if (!$material.find('.part').length) {
                $material.hide();
            }
        }
    }

    function removeFromTransport($part, $material) {
        $part.removeClass(FORM_NAME);
        $material.find('.parts').append($part);
        $material.find('.business').addClass('hidden');
        $material.show();
        var materialId = $material.find('.id').val();
        var divNewMaterial = $formMaterials.find('#material_' + materialId);
        if (!divNewMaterial.find('.part').length) {
            divNewMaterial.remove();
        }
    }

});


