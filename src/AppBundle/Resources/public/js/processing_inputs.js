
/*
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

function addButtons($materials) {
        $materials.find('.material').each(function () {
            var $material = $(this);
            $material.find('.part').each(function () {
                var $part = $(this);
                var $addButton = $('<a href="#" class="add button">Přidej do zpracování</a>');
                var $removeButton = $('<a href="#" class="remove button hidden">Odeber ze zpracování</a>');
                var $addButtonLi = $('<li></li>').append($addButton);
                var $removeButtonLi = $('<li></li>').append($removeButton);
                var $div = $('<div class="col-md-2 col-sm-4 list-inline remove_button buttons"></div>').append($addButtonLi).append($removeButtonLi);
                $part.append($div);
                $addButton.on('click', function (e) {
                    e.preventDefault();
                    if (canBeAdded($material)) {
                        $(this).addClass('hidden');
                        addToProcessing($part, $material);
                        $removeButton.removeClass('hidden');
                    } else {
                        alert('Material nelze s dalsimi materialy zpracovat');
                    }
                })
                $removeButton.on('click', function (e) {
                    e.preventDefault();
                    $(this).addClass('hidden');
                    removeFromProcessing($part, $material);
                    $addButton.removeClass('hidden');
                })

            })
        })
    }

function getMaterialData($material) {
        var data = {
            'id': $material.find('.id').val(),
            'plastics': $material.find('.plastics').val(),
            'specification': $material.find('.specification').val(),
            'finalForm': $material.find('finalForm').val(),
        };
        return data;
    }

function canBeAdded($material) {
        if (!$inputsMaterials.find('.material').length) {
            return true;
        }
        var $inputMaterial = $inputsMaterials.find('.material').first();
        var inputData = getMaterialData($inputMaterial);
        var materialData = getMaterialData($material);
        if (inputData.id == materialData.id) return true;
        if (inputData.plastics != materialData.plastics
            || inputData.specification != materialData.specification
            || inputData.finalForm != materialData.finalForm)
            return false;

        return true;
    }

function addToProcessing($part, $material) {
        $part.addClass('processing');
        var materialId = $material.find('.id').val();
        var $divNewMaterial = $inputsMaterials.find('#material_' + materialId);
        if (!$divNewMaterial.length) {
            $divNewMaterial = copyMaterial($material, materialId);
        }
        addPartToProcessing();

        function addPartToProcessing() {
            var $buttons = $('<div></div>').append($part.find('.buttons'));
            var text = $part.html();
            var partId = $part.find('.id').val();
            text = text.replace(PART_REGEX, PART_NAME.replace(/partId/g, partId));
            $part.html(text);
            var prototype = $inputsMaterials.data('prototype');
            var $newForm = prototype.replace(INPUT_PROTOTYPE_NAME, partId);
            $part.prepend($newForm);
            $part.append($buttons.find('.buttons'));
            $divNewMaterial.find('.parts').append($part);
            if (!$material.find('.part').length) {
                $material.hide();
            }

        }

        function copyMaterial($material, materialId) {
            var newMaterial = $material.html();
            var $divNewMaterial = $("<div id='material_" + materialId + "' class='material'></div>").append(newMaterial);
            $divNewMaterial.find('input').each(function(){
                $(this).attr('name', '');
            })
            $inputsMaterials.append($divNewMaterial);
            $divNewMaterial.find('.part').remove();
            return $divNewMaterial;
        }
    }

function removeFromProcessing($part, $material) {
        $part.removeClass('processing');
        $material.find('.parts').append($part);
        $part.find('.part_processing').addClass('hidden');
        $material.show();
        var materialId = $material.find('.id').val();
        var divNewMaterial = $inputsMaterials.find('#material_' + materialId);
        if (!divNewMaterial.find('.part').length) {
            divNewMaterial.remove();
        }
    }
