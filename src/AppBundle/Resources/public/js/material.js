
/*
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

jQuery(document).ready(function(){
    selectAll();

    $('button[type="submit"]').on('click', function(e){
        if ( $('#material_filter_warehouse :checkbox:checked').length == 0){
            if ( !confirm('Není vybraná žádná provozovna\n' +
                    'Prejete si pokracovat?') ){
                e.preventDefault();
                return;
            }
        }
    });

    function selectAll(){
        $('option').each(function(){
            $(this).prop('selected', true);
        });
        $('input[type="checkbox"]').each(function(){
            var html = $("<div />").append($(this).clone()).html();
            if ( html.indexOf('warehouse') == -1 ) {
                $(this).prop('checked', true);
            }
        })
    }

    $('button[type="reset"]').bind('click', function (e){
        e.preventDefault();
        $(this).closest('form').get(0).reset();
        selectAll();
    });

    $( "form" ).on( "submit", function(e){
        e.preventDefault();
        getMaterials($(this));
    });
});

function getMaterials(form){
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data:  form.serialize(),
        success: function(html){
            $('#materials_table').replaceWith(
                // ... with the returned one from the AJAX response.
                $(html).find('#materials_table')
            );

        },
        error: function(){
            alert('error');
        }
    });
}

