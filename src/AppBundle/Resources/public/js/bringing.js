/*
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */


const FORM_NAME = 'form_bringing';
const BUSINESS_NAME = 'materials';
const BUSINESS_PROTOTYPE_NAME = /business_name/g;
const MATERIAL_NAME = 'materials';
const MATERIAL_PROTOTYPE_NAME = /material_name/g;
const PART_NAME = 'parts';
const PART_PROTOTYPE_NAME = /part_name/g;
const SEPARATOR = '_';
const LINK_NAME = 'new';

var $businessHolder;
var $materialHolder;
var $partHolder;

var zkouska = '<h2>Funguje</h2>';

jQuery(document).ready(function(){

    var idBusinessLink = FORM_NAME + SEPARATOR + BUSINESS_NAME + SEPARATOR + 'new';
    var $addBusinessLink = $('<a href="#" id=' + idBusinessLink + ' class="add_link btn">Přidat materiál do návozu</a>');
    var $addBusinessLinkLi = $('<li></li>').append($addBusinessLink)
    var $newLinkDiv = $('<ul class="col-md-2 col-sm-4 button list-inline"></ul>').append($addBusinessLinkLi);

    $businessHolder = $('#' + FORM_NAME + SEPARATOR + BUSINESS_NAME);
    $businessHolder.append($newLinkDiv);

    var index = $businessHolder.find('.material').length;
    for(var i = 0; i < index; i++){
       // var materialName = BUSINESS_NAME + SEPARATOR + BUSINESS_NAME + SEPARATOR + index + SEPARATOR + MATERIAL_NAME + SEPARATOR + index;
        addPartLink(i, $partHolder);
    }
    $businessHolder.data('index', index);


    $businessHolder.find('.business').each(function() {
        addBusinessFormDeleteLink($(this));
    });


    $addBusinessLink.on('click', function(e){
        e.preventDefault();
        addMaterialForm($newLinkDiv);
    })

});

function addMaterialForm($newLinkDiv, $materialHolder){
    var prototype = $businessHolder.data('prototype');
    var index = $businessHolder.data('index');
    var $newForm = prototype.replace(BUSINESS_PROTOTYPE_NAME, index);
    $businessHolder.data('index', index + 1);
    $newLinkDiv.before($newForm);
    var $newFormDiv = $('#' + FORM_NAME + SEPARATOR + BUSINESS_NAME + SEPARATOR + index);
    addMaterial(index, $materialHolder);
    addBusinessFormDeleteLink($newFormDiv);
}

function addMaterial(index, $materialHolder){
    $materialHolder = $('#' + FORM_NAME + SEPARATOR + BUSINESS_NAME + SEPARATOR + index + SEPARATOR + MATERIAL_NAME);
    var prototype = $materialHolder.data('prototype');
    var $newForm = prototype.replace(MATERIAL_PROTOTYPE_NAME, index);
    $materialHolder.append($newForm);
    addPartLink(index, $partHolder);
}

function addPartLink(index, $partHolder){
    var partName = FORM_NAME + SEPARATOR + BUSINESS_NAME + SEPARATOR + index + SEPARATOR + MATERIAL_NAME + SEPARATOR + index + SEPARATOR + PART_NAME;
    $partHolder = $('#' + partName);
    var newLinkName = partName + SEPARATOR + LINK_NAME;
    var $addPartLink = $('<a href="#" class="add_link btn" id='+newLinkName+'>Přidat část</a>');
    var $addPartLinkLi = $('<li ></li>').append($addPartLink)
    var $newPartLinkDiv = $('<ul class="col-md-2 col-sm-4 list-inline"></ul>').append($addPartLinkLi);
    $partHolder.append($newPartLinkDiv);
    var partIndex = $partHolder.find('.part').length;
    $partHolder.data('index', partIndex);

    for (var i = 0; i < partIndex; i++){
        addPartFormDeleteLink(index, i);
    }

    $addPartLink.on('click', function(e){;
        e.preventDefault();
        addPartForm($newPartLinkDiv, $partHolder, index);
    })
}

function addPartForm($newPartLinkDiv, $partHolder, materialIndex){
    var prototype = $partHolder.data('prototype');
    var index = $partHolder.data('index');
    var $newForm = prototype.replace(PART_PROTOTYPE_NAME, index).replace(MATERIAL_PROTOTYPE_NAME, materialIndex).replace(BUSINESS_PROTOTYPE_NAME, materialIndex);
    $partHolder.data('index', index + 1);
    $newPartLinkDiv.before($newForm);
    addPartFormDeleteLink(materialIndex, index);
}

function addBusinessFormDeleteLink($businessForm) {
    var $removeFormA = $('<a class="remove_link btn" href="#">Odstranit materiál z návozu</a>');
    var $removeFormALi = $('<li></li>').append($removeFormA);
    var $removeFormADiv = $('<ul class="col-md-2 col-sm-4 list-inline remove_button"></ul>').append($removeFormALi);
    $businessForm.find('.parts').before($removeFormADiv);
    $removeFormA.on('click', function(e) {
        e.preventDefault();
        $businessForm.remove();
    });
}

function addPartFormDeleteLink(index, partIndex) {
    var $newFormDiv = $('#'+FORM_NAME + SEPARATOR + BUSINESS_NAME + SEPARATOR + index + SEPARATOR + MATERIAL_NAME + SEPARATOR + index + SEPARATOR + PART_NAME + SEPARATOR+partIndex);
    var $removeFormA = $('<a class="remove_link btn" href="#">Odstranit část</a>');
    var $removeFormALi = $('<li></li>').append($removeFormA)
    var $removeFormADiv = $('<ul class="col-md-2 col-sm-4 list-inline pull-right"></ul>').append($removeFormALi);
    $newFormDiv.append($removeFormADiv);
    $removeFormA.on('click', function(e) {
        e.preventDefault();
        $newFormDiv.remove();
    });
}

