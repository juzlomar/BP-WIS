
/*
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

function addOutputLink(){
    // setup an "add a tag" link
    var outputLink = FORM_NAME + SEPARATOR + OUTPUT_NAME + SEPARATOR + 'new';
    var $addOutputLink = $('<a href="#" id=' + outputLink + ' class="add_link btn">Přidat cast</a>');
    var $addOutputLinkLi = $('<li></li>').append($addOutputLink);
    var $newLinkDiv = $('<ul class="col-md-2 col-sm-4 list-inline button"></ul>').append($addOutputLinkLi);
    // Get the ul that holds the collection of tags
    // add the "add a tag" anchor and li to the tags ul
    $outputMaterials.append($newLinkDiv);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $outputMaterials.data('index', $outputMaterials.find(':input').length);

    $addOutputLink.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();


        // add a new tag form (see next code block)
        addPartForm($outputMaterials, $newLinkDiv);
    });
}


function addPartForm($collectionHolder, $newLinkDiv) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');
    // get the new index
    var index = $collectionHolder.data('index');

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    var newForm = prototype.replace(OUTPUT_PROTOTYPE_NAME, index);


    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add a tag" link li
    $newLinkDiv.before(newForm);
    var $newFormDiv = $('#' + FORM_NAME + SEPARATOR + OUTPUT_NAME + SEPARATOR + index);
    addPartFormDeleteLink($newFormDiv);
}

function addPartFormDeleteLink($partFormDiv) {
    var $removeFormA = $('<a href="#" class="remove_link btn">Odstranit část</a>');
    var $removeFormLi = $('<li></li>').append($removeFormA);
    var $removeFormDiv = $('<ul class="col-md-2 col-sm-4 list-inline pull-right"></ul>').append($removeFormLi);
    $partFormDiv.append($removeFormDiv);

    $removeFormA.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // deactivate the li for the tag form
        $partFormDiv.remove();
    });
}



