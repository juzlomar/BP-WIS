<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Bringing;
use AppBundle\Form\BringingType;
use AppBundle\Form\BringingFilterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class BringingController extends TransportController
{
    /**
     * Create new Bringing.
     * @Route("/navoz/novy", name="bringing_new")
     * @Template("AppBundle:Transport:new_bringing.html.twig")
     * @Security("has_role('ROLE_WAREHOUSEMAN')")
     */
    public function newAction(Request $request){
        $bringing = new Bringing();
        $states = array(
            $this->getParameter('states')['processing'] => $this->getParameter('states')['processing'],
            $this->getParameter('states')['error'] => $this->getParameter('states')['error'],
            $this->getParameter('states')['expedition'] => $this->getParameter('states')['expedition'],
        );
        $form = $this->createForm(BringingType::class, $bringing, array('states' => $states));
        $form->handleRequest($request);
        if ($form->isSubmitted()){
            $bringingManager = $this->get('app.service.manager.transport');
            $bringingManager->validate($form);
            if ($form->isValid()){
                $bringingManager->save($bringing, $form);
                $this->addFlash('notice', 'Návoz uložen');
              //  return array('form_bringing' => $form->createView());
                return $this->redirectToRoute('transport_detail', array('id' => $bringing->getId()));
            }
        }
        return array('form' => $form->createView());
    }

    /**
     * Show bringings according forms data.
     * @Route("/navoz", name="bringing_show")
     * @Template("AppBundle:Transport:show.html.twig")
     */
    public function showAction(Request $request){
        $transports = array();
        $form = $this->createForm(BringingFilterType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted()){
            if ($form->isValid()){
                $transferManager = $this->get('app.service.manager.transport');
                $transports = $transferManager->find($form);
            }
        }
        return array('transports' => $transports, 'form' => $form->createView());
    }

}