<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

/**
 * Created by PhpStorm.
 * User: musilane
 * Date: 15.02.16
 * Time: 17:35
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\RoleType;

class UserController extends Controller
{
    /**
     * Show all users.
     * @Route("/uzivatele", name="user_show")
     * @Template()
     * @return array
     */
    public function showAction(){
        $userManager = $this->get('app.service.manager.user');
        $users = $userManager->findAll();
        return array (
            'users' => $users,
        );
    }

    /**
     * Update roles of the user.
     * @Route("/uzivatele/{username}/role", name="user_role")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function roleAction($username, Request $request){
        $manager = $this->get('fos_user.user_manager');
        $user =  $manager->findUserByUsername($username);
        if (! $user ){
            throw $this->createNotFoundException('Uživatel neexistuje');
        }
        $form = $this->createForm(RoleType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $manager->updateUser($user);
            $this->addFlash('notice', 'Role upraveny');
            return $this->redirectToRoute('user_show');
        }
        return array('form' => $form->createView(), 'user' => $user);
    }

    /**
     * Deactivate user.
     * @Route("/uzivatele/{username}/deaktivovat", name="user_deactivate")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deactivateAction($username){
        $manager = $this->get('fos_user.user_manager');
        $user =  $manager->findUserByUsername($username);
        if (! $user ){
            throw $this->createNotFoundException('Uživatel neexistuje');
        }
        $user->setEnabled(false);
        $manager->updateUser($user);
        $this->addFlash('notice', 'Uživatel deaktivován');
        return $this->redirectToRoute('user_show');
    }

    /**
     * Activate user.
     * @Route("/uzivatele/{username}/aktivovat", name="user_activate")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function activateAction($username){
        $manager = $this->get('fos_user.user_manager');
        $user =  $manager->findUserByUsername($username);
        if (! $user ){
            throw $this->createNotFoundException('Uživatel neexistuje');
        }
        $user->setEnabled(true);
        $manager->updateUser($user);
        $this->addFlash('notice', 'Uživatel aktivován');
        return $this->redirectToRoute('user_show');
    }
    
}