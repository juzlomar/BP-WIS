<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Warehouse;
use AppBundle\Form\WarehouseType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class WarehouseController extends Controller
{
    /**
     * Shows all warehouses.
     * @Route("/provozovna", name="warehouse_show")
     * @Template()
     * @Security("has_role('ROLE_EMPLOYEE')")
     */
    public function showAction(){
        $manager = $this->get('app.service.manager.warehouse');
        $warehouses = $manager->findWarehouses();
        return array( 'warehouses' => $warehouses );
    }

    /**
     * Creates new warehouse.
     * @Route("/provozovna/novy", name="warehouse_new")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function newAction(Request $request){
        $warehouse = new Warehouse();
        $form = $this->createForm(WarehouseType::class, $warehouse);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $manager = $this->get('app.service.manager.warehouse');
            $manager->updateWarehouse($warehouse);
            $this->addFlash('notice', 'Provozovna uložena');
            return $this->redirectToRoute('warehouse_show');
        }
        return array('form' => $form->createView());
    }

    /**
     * Update warehouse.
     * @Route("/provozovna/upravit/{id}", name="warehouse_update")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function updateAction(Request $request, $id){
        $manager = $this->get('app.service.manager.warehouse');
        $warehouse = $manager->findWarehouseById($id);
        if ($warehouse === null){
            throw $this->createNotFoundException('Provozovna neexistuje');
        }
        $form = $this->createForm(WarehouseType::class, $warehouse);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $manager->updateWarehouse($warehouse);
            $this->addFlash('notice', 'Provozovna uložena');
            return $this->redirectToRoute('warehouse_show');
        }
        return array('form' => $form->createView());
    }

    /**
     * Deactivate warehouse.
     * @Route("/provozovna/smazat/{id}", name="warehouse_remove")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deactivateAction($id){
        $manager = $this->get('app.service.manager.warehouse');
        if ( $manager->deactivate($id) === null){
            throw $this->createNotFoundException('Provozovna neexistuje');
        }
        $this->addFlash('notice', 'Provozovna deaktivována');
        return $this->redirectToRoute('warehouse_show');
    }

    /**
     * Activate warehouse.
     * @Route("/provozovna/aktivovat/{id}", name="warehouse_activate")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function activateAction($id){
        $manager = $this->get('app.service.manager.warehouse');
        if ( $manager->activate($id) === null){
            throw $this->createNotFoundException('Provozovna neexistuje');
        }
        $this->addFlash('notice', 'Provozovna aktivována');
        return $this->redirectToRoute('warehouse_show');
    }

}