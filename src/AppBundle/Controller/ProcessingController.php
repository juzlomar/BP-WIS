<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Processing;
use AppBundle\Entity\WayOfProcessing;
use AppBundle\Form\ChooseMaterialType;
use AppBundle\Form\ProcessingType;
use AppBundle\Form\ProcessingFilterType;
use AppBundle\Form\WayOfProcessingType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


class ProcessingController extends Controller
{
    /**
     * Create new processing.
     * @Route("/zpracovani/novy", name="processing_new")
     * @Template()
     * @Security("has_role('ROLE_WAREHOUSEMAN')")
     * @param Request $request
     * @return array
     */
    public function newAction(Request $request){
        $processingManager = $this->get('app.service.manager.processing');
        $processing = new Processing();
        $states = array(
            $this->getParameter('states')['processing'] => $this->getParameter('states')['processing'],
            $this->getParameter('states')['expedition'] => $this->getParameter('states')['expedition'],
            $this->getParameter('states')['error'] => $this->getParameter('states')['error'],
        );
        $form = $this->createForm(ProcessingType::class, $processing, array('states' => $states));
        $form->handleRequest($request);
        if($form->isSubmitted()){
            $processingManager->validate($form);
            if ($form->isValid()){
                $processingManager->save($processing);
                $this->addFlash('notice', 'Zpracování uloženo');
                return $this->redirectToRoute('processing_detail', array('id' => $processing->getId()));
            }
        }
        return array('form_processing' => $form->createView());
    }

    /**
     * Show all material which can be processed.
     * @Route("/zpracovani/material", name="processing_material")
     * @Method({"POST"})
     * @Template()
     * @Security("has_role('ROLE_WAREHOUSEMAN')")
     * @return array
     */
    public function materialAction(Request $request)
    {
        $states = array(
            $this->getParameter('states')['processing'] => $this->getParameter('states')['processing'],
            $this->getParameter('states')['expedition'] => $this->getParameter('states')['expedition'],
            $this->getParameter('states')['error'] => $this->getParameter('states')['error'],
        );
        $processing = new Processing();
        $form = $this->createForm(ProcessingType::class, $processing);
        $result = array('materials' => array());
        $materialForm = $this->createForm(ChooseMaterialType::class, $result, array('states' => $states));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $processing->getWarehouse() && $processing->getWayOfProcessing()){
            $processingManager = $this->get('app.service.manager.processing');
            $materials = $processingManager->findMaterials($form->get('warehouse')->getData(), $form->get('wayOfProcessing')->getData());
            $materialForm->get('materials')->setData($materials);
        }
        return array('form' => $materialForm->createView(), 'form_processing' => $form->createView());
    }

    /**
     * Show processings according data from form.
     * @Route("/zpracovani", name="processing_show")
     * @Template()
     * @param Request $request
     */
    public function showAction(Request $request){
        $form = $this->createForm(ProcessingFilterType::class);
        $form->handleRequest($request);
        $processings = array();
        if ($form->isSubmitted() ){
            if ($form->isValid()){
                $processingManager = $this->get('app.service.manager.processing');
                $processings = $processingManager->find($form);
            }
        }
        return array('form' => $form->createView(), 'processings' => $processings);
    }

    /**
     * Show detail of the processing.
     * @Route("/zpracovani/detail/{id}", name="processing_detail")
     * @Template()
     */
    public function detailAction($id){
        $processingManager = $this->get('app.service.manager.processing');
        $processing = $processingManager->findById($id);
        if ($processing === null){
            throw $this->createNotFoundException('Zpracování neexistuje');
        }
        return array('processing' => $processing);
    }

    /**
     * Show all ways how can be material processed.
     * @Route("/zpracovani/zpusob", name="processing_way_show")
     * @Template("AppBundle:Processing:way_show.html.twig")
     * @Security("has_role('ROLE_EMPLOYEE')")
     */
    public function showWaysAction(){
        $manager = $this->get('app.service.manager.processing');
        $waysOfProcessing = $manager->findWays();
        return array( 'waysOfProcessing' => $waysOfProcessing );
    }

    /**
     * Create ne way of processing.
     * @Route("/zpracovani/zpusob/novy", name="processing_way_new")
     * @Template("AppBundle:Processing:way_new.html.twig")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function newWayAction(Request $request){
        $processing = new WayOfProcessing();
        $form = $this->createForm(WayOfProcessingType::class, $processing);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $manager = $this->get('app.service.manager.processing');
            $manager->validateWay($form);
            if ( $form->isValid() ){
                $manager->updateWay($processing);
                $this->addFlash('notice', 'Způsob zpracování uložen');
                return $this->redirectToRoute('processing_way_show');
            }
        }
        return array('form' => $form->createView());
    }

    /**
     * Update way of processing.
     * @Route("/zpracovani/zpusob/upravit/{title}", name="processing_way_update")
     * @Template("AppBundle:Processing:way_update.html.twig")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function updateWayAction(Request $request, $title){
        $manager = $this->get('app.service.manager.processing');
        $processing = $manager->findWayByTitle($title);
        if ($processing == null){
            throw $this->createNotFoundException('Způsob zpracování neexistuje');
        }
        $form = $this->createForm(WayOfProcessingType::class, $processing);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $manager->validateWay($form);
            if ( $form->isValid() ){
                $manager->updateWay($processing);
                $this->addFlash('notice', 'Způsob zpracování uložen');
                return $this->redirectToRoute('processing_way_show');
            }
        }
        return array('form' => $form->createView());
    }
}