<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use AppBundle\Controller\TransportController;
use AppBundle\Entity\Transfer;
use AppBundle\Form\TransferType;
use AppBundle\Form\TransferFilterType;
use AppBundle\Form\ChooseMaterialType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;


class TransferController extends TransportController
{
    /**
     * Create new transfer.
     * @Route("odvoz/novy", name="transfer_new")
     * @Template("AppBundle:Transport:new_transfer.html.twig")
     * @Security("has_role('ROLE_WAREHOUSEMAN')")
     */

    public function newAction(Request $request){
        $transfer = new Transfer();
        $states = array(
            $this->getParameter('states')['expedition'] => $this->getParameter('states')['expedition'],
        );
        $form = $this->createForm(TransferType::class, $transfer, array('states' => $states));
        $form->handleRequest($request);
        if ($form->isSubmitted()){
            $transferManager = $this->get('app.service.manager.transport');
            $transferManager->validate($form);
            if ($form->isValid()){
                $transferManager->save($transfer);
                $this->addFlash('notice', 'Odvoz uložen');
                return $this->redirectToRoute('transport_detail', array('id' => $transfer->getId()));
            }
        }

        return array('form' => $form->createView());
    }


    /**
     * Show transfers according data from form.
     * @Route("/odvoz", name="transfer_show")
     * @Template("AppBundle:Transport:show.html.twig")
     */
    public function showAction(Request $request){
        $transports = array();
        $form = $this->createForm(TransferFilterType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted()){
            if ($form->isValid()){
                $transferManager = $this->get('app.service.manager.transport');
                $transports = $transferManager->find($form);
            }
        }

        return array('transports' => $transports, 'form' => $form->createView());
    }

    /**
     * Show all materials which can be transfered.
     * @Route("/preprava/material/transfer", name="transfer_material")
     * @Method({"POST"})
     * @Template("AppBundle:Transport:material.html.twig")
     * @Security("has_role('ROLE_WAREHOUSEMAN')")
     * @return array
     */
    public function materialAction(Request $request)
    {
        $states = array(
            $this->getParameter('states')['expedition'] => $this->getParameter('states')['expedition'],
        );
        $transport = new Transfer();
        $form = $this->createForm(TransferType::class, $transport);
        $result = array('materials' => array());
        $materialForm = $this->createForm(ChooseMaterialType::class, $result, array('states' => $states));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $transport->getFrom()){
            $manager = $this->get('app.service.manager.transport');
            $materials = $manager->findMaterialsToTransfer($transport->getFrom());
            $materialForm->get('materials')->setData($materials);
        }
        return array('form_materials' => $materialForm->createView(), 'form' => $form->createView());
    }

}