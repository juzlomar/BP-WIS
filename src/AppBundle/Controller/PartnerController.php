<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Controller;


use AppBundle\Entity\BusinessPartner;
use AppBundle\Form\BusinessPartnerType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;



class PartnerController extends Controller
{

    /**
     * Show all business partners.
     * @Route("/partner", name="partner_show")
     * @Template()
     * @Security("has_role('ROLE_EMPLOYEE')")
     */
    public function showAction(){
        $manager = $this->get('app.service.manager.partner');
        $partners = $manager->find();
        return array('partners' => $partners);
    }

    /**
     * Create new business partner.
     * @Route("/partner/novy", name="partner_new")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function newAction(Request $request){
        $partner = new BusinessPartner();
        $form = $this->createForm(BusinessPartnerType::class, $partner);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $manager = $this->get('app.service.manager.partner');
            $manager->update($partner);
            $this->addFlash('notice', 'Partner uložen');
            return $this->redirectToRoute('partner_show');
        }
        return array('form' => $form->createView());
    }

    /**
     * Updata business partner.
     * @Route("/partner/upravit/{id}", name="partner_update")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function updateAction(Request $request, $id){
        $manager = $this->get('app.service.manager.partner');
        $partner = $manager->findById($id);
        if ($partner === null){
            throw $this->createNotFoundException('Partner neexistuje');
        }
        $errors = array();
        $form = $this->createForm(BusinessPartnerType::class, $partner);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $manager->update($partner);
            $this->addFlash('notice', 'Partner uložen');
            return $this->redirectToRoute('partner_show');
        }
        return array('form' => $form->createView(), 'message' => $errors);
    }
}