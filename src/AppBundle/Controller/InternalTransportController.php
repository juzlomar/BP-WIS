<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Controller;


use AppBundle\Entity\InternalTransport;
use AppBundle\Form\InternalTransportType;
use AppBundle\Form\InternalTransportFilterType;
use AppBundle\Form\ChooseMaterialType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class InternalTransportController extends TransportController
{
    /**
     * Create new internal transport.
     * @Route("/prevoz/novy", name="internal_new")
     * @Template("AppBundle:Transport:new_internal.html.twig")
     * @Security("has_role('ROLE_WAREHOUSEMAN')")
     * @param Request $request
     * @return array
     */
    public function newAction(Request $request){
        $transport = new InternalTransport();
        $states = array(
            $this->getParameter('states')['processing'] => $this->getParameter('states')['processing'],
            $this->getParameter('states')['error'] => $this->getParameter('states')['error'],
            $this->getParameter('states')['expedition'] => $this->getParameter('states')['expedition'],
        );
        $form = $this->createForm(InternalTransportType::class, $transport, array('states' => $states));
        $form->handleRequest($request);
        if ($form->isSubmitted()){
            $transferManager = $this->get('app.service.manager.transport');
            $transferManager->validate($form);
            if ($form->isValid()){
                $transferManager->save($transport);
                $this->addFlash('notice', 'Mezistředisková přeprava uložena');
                return $this->redirectToRoute('transport_detail', array('id' => $transport->getId()));
            }
        }

        return array('form' => $form->createView());
    }

    /**
     * Show internal transport according form data.
     * @Route("/prevoz", name="internal_show")
     * @Template("AppBundle:Transport:show.html.twig")
     */
    public function showAction(Request $request){
        $transports = array();
        $form = $this->createForm(InternalTransportFilterType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted()){
            if ($form->isValid()){
                $transferManager = $this->get('app.service.manager.transport');
                $transports = $transferManager->find($form);
            }
        }
        return array('transports' => $transports, 'form' => $form->createView());
    }

    /**
     * Get materials which can be transported.
     * @Route("/preprava/material/internal_transport", name="internal_material")
     * @Method({"POST"})
     * @Template("AppBundle:Transport:material.html.twig")
     * @Security("has_role('ROLE_WAREHOUSEMAN')")
     * @return array
     */
    public function materialAction(Request $request)
    {
        $states = array(
            $this->getParameter('states')['processing'] => $this->getParameter('states')['processing'],
            $this->getParameter('states')['expedition'] => $this->getParameter('states')['expedition'],
            $this->getParameter('states')['error'] => $this->getParameter('states')['error'],
        );
        $transport = new InternalTransport();
        $form = $this->createForm(InternalTransportType::class, $transport, array('states' => $states));
        $result = array('materials' => array());
        $materialForm = $this->createForm(ChooseMaterialType::class, $result, array('states' => $states));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $transport->getFrom()){
            $manager = $this->get('app.service.manager.transport');
            $materials = $manager->findMaterialsToInternalTransport($transport->getFrom());
            $materialForm->get('materials')->setData($materials);
        }
        return array('form' => $form->createView(), 'form_materials' => $materialForm->createView());
    }
}