<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class TransportController extends Controller
{
    /**
     * Show detail of the transport.
     * @Route("/preprava/detail/{id}", name="transport_detail")
     * @Template()
     */
    public function detailAction($id){
        $transferManager = $this->get('app.service.manager.transport');
        $transport = $transferManager->findById($id);
        if ($transport === null){
            throw $this->createNotFoundException('Přeprava neexistuje');
        }
        return array('transport' => $transport);
    }
}