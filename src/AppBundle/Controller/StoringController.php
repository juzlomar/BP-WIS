<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Controller;


use AppBundle\Entity\WayOfStoring;
use AppBundle\Form\WayOfStoringType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;


class StoringController extends Controller
{
    /**
     * Show all ways of storing.
     * @Route("/uskladneni", name="storing_show")
     * @Template()
     * @Security("has_role('ROLE_EMPLOYEE')")
     */
    public function showAction(){
        $manager = $this->get('app.service.manager.storing');
        $waysOfStoring = $manager->findWays();
        return array( 'waysOfStoring' => $waysOfStoring );
    }

    /**
     * Create new way of Storing
     * @Route("/uskladneni/novy", name="storing_new")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function newAction(Request $request){
        $storing = new WayOfStoring();
        $form = $this->createForm(WayOfStoringType::class, $storing);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $manager = $this->get('app.service.manager.storing');
            $manager->updateWay($storing);
            $this->addFlash('notice', 'Uskladnění uloženo');
            return $this->redirectToRoute('storing_show');
        }
        return array('form' => $form->createView());
    }

    /**
     * Update way of Storing.
     * @Route("/uskladneni/upravit/{title}", name="storing_update")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function updateAction(Request $request, $title){
        $manager = $this->get('app.service.manager.storing');
        $storing = $manager->findWayByTitle($title);
        if ($storing == null){
            throw $this->createNotFoundException('Uskladnění neexistuje');
        }
        $form = $this->createForm(WayOfStoringType::class, $storing);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $manager->updateWay($storing);
            $this->addFlash('notice', 'Uskladnění uloženo');
            return $this->redirectToRoute('storing_show');
        }
        return array('form' => $form->createView());
    }

}