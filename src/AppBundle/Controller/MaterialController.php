<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use AppBundle\Form\MaterialFilterType;
use AppBundle\Form\MaterialType;
use AppBundle\Form\ReportType;
use AppBundle\Form\StateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class MaterialController extends Controller
{
    /**
     * Show materials according form data.
     * @Route("/material", name="material_show")
     * @Template()
     * @Security("has_role('ROLE_EMPLOYEE')")
     * @param Request $request
     */
    public function showAction(Request $request){
        $materialManager = $this->get('app.service.manager.material');
        $attributes = $materialManager->findAllAtributes();
        $states = array(
            $this->getParameter('states')['processing'] => $this->getParameter('states')['processing'],
            $this->getParameter('states')['expedition'] => $this->getParameter('states')['expedition'],
            $this->getParameter('states')['error'] => $this->getParameter('states')['error'],
            $this->getParameter('states')['processed'] => $this->getParameter('states')['processed'],
            $this->getParameter('states')['transferred'] => $this->getParameter('states')['transferred'],
        );
        $form = $this->createForm(MaterialFilterType::class, null,
            array('plastics' => $attributes['plastics'],
                'specifications' => $attributes['specifications'],
                'colors' => $attributes['colors'],
                'states' => $states));
        $form->handleRequest($request);
        if ($form->isSubmitted()){
            if ($form->isValid()){
                $materials = $materialManager->findByFilter($form->getData());
                return array('materials' => $materials, 'filter_form' => $form->createView());
            }
        }
        $materials = array();
        return array('materials' => $materials, 'filter_form' => $form->createView());
    }

    /**
     * Update material.
     * @Route("/material/upravit/{id}", name="material_update")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateAction(Request $request, $id){
        $materialManager = $this->get('app.service.manager.material');
        $material = $materialManager->findCurrentById($id);
        if ($material === null) throw $this->createNotFoundException('Materiál neexistuje');
        $states = array(
            $this->getParameter('states')['processing'] => $this->getParameter('states')['processing'],
            $this->getParameter('states')['expedition'] => $this->getParameter('states')['expedition'],
            $this->getParameter('states')['error'] => $this->getParameter('states')['error'],
        );
        $form = $this->createForm(MaterialType::class, $material, array('states' => $states) );
        $form->add('save', SubmitType::class,
            array(
                'label' => 'Ulož',
                'validation_groups' => array('Default'),
            ));
        if ($request->isMethod('POST')){
            $postData = $request->request->get($form->getName());
            $materialManager->validate($form, $postData);
            $this->get('validator')->validate($material);
            if ($form->isValid()){
                $this->addFlash('notice', 'Ulozeno');
                $materialManager->save($material);
                return $this->redirectToRoute('material_detail', array('id' => $id), 301);
            }
        }
        return array('form' => $form->createView());
    }

    /**
     * Show detail of the material
     * @Route("/material/detail/{id}", name="material_detail")
     * @Security("has_role('ROLE_EMPLOYEE')")
     * @Template()
     */
    public function detailAction($id){

        $materialManager = $this->get('app.service.manager.material');
        $result = $materialManager->findAllById($id);
        if ($result['material'] === null) throw $this->createNotFoundException('Materiál neexistuje');
        return array('material' => $result['material'], 'processing' => $result['processing'], 'weight' => $result['weight']);
    }

    /**
     * Show report according form data.
     * @Route("/material/vykaz", name="material_report")
     * @Template("AppBundle:Material/Report:base.html.twig")
     * @Security("has_role('ROLE_EMPLOYEE')")
     * @param Request $request
     */
    public function reportAction(Request $request){
        $data = array();
        $form = $this->createForm(ReportType::class, $data);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid() ){
            $materialManager = $this->get('app.service.manager.material');
            $materials = $materialManager->findMaterialToReport($form->getData());
            switch($form->get('type')->getData()){
                case $this->getParameter('reports')['processing']:
                    return $this->render('AppBundle:Material/Report:processing.html.twig', array(
                        'form' => $form->createView(),
                        'materials' => $materials,
                    ));
                case $this->getParameter('reports')['bringing']:
                    return $this->render('AppBundle:Material/Report:bringing.html.twig', array(
                        'form' => $form->createView(),
                        'materials' => $materials,
                    ));
                case $this->getParameter('reports')['transfer']:
                    return $this->render('AppBundle:Material/Report:transfer.html.twig', array(
                        'form' => $form->createView(),
                        'materials' => $materials,
                    ));
            }
        }
        return $this->render('AppBundle:Material/Report:base.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Show current sum of weight materials.
     * @Route("/stav", name="state")
     * @Template()
     * @Security("has_role('ROLE_EMPLOYEE')")
     * @return array
     */
    public function stateAction(Request $request){
        $manager = $this->get('app.service.manager.material');
        $form = $this->createForm(StateType::class);
        $form->handleRequest($request);
        $plastics = array();
        if ($form->isSubmitted() && $form->isValid()){
            $plastics = $manager->getState($form->getData());
            if ($form->get('type')->getData() == $this->getParameter('languages')['eng']){
                return $this->render('AppBundle:Material:state_eng.html.twig', array(
                    'form' => $form->createView(),
                    'plastics' => $plastics,
                ));
            }
        }
        return array('form' => $form->createView(), 'plastics' => $plastics);
    }
}