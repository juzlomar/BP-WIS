<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Form;
use AppBundle\Form\FormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class FormController extends Controller
{
    /**
     * Show all forms.
     * @Route("/podoba", name="form_show")
     * @Template("AppBundle:MaterialForm:show.html.twig")
     * @Security("has_role('ROLE_EMPLOYEE')")
     */
    public function showAction(){
        $manager = $this->get('app.service.manager.form');
        $forms = $manager->find();
        return array( 'forms' => $forms );
    }

    /**
     * Update form.
     * @Route("/podoba/upravit/{title}", name="form_update")
     * @Template("AppBundle:MaterialForm:update.html.twig")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function updateAction(Request $request, $title){
        $manager = $this->get('app.service.manager.form');
        $form = $manager->findByTitle($title);
        if ($form == null){
            throw $this->createNotFoundException('Podoba neexistuje');
        }
        $formType = $this->createForm(FormType::class, $form);
        $formType->handleRequest($request);
        if ($formType->isSubmitted() && $formType->isValid()){
            $manager->validate($formType);
            if ( $formType->isValid() ){
                $manager->update($form);
                $this->addFlash('notice', 'Podoba uložena');
                return $this->redirectToRoute('form_show');
            }
        }
        return array('form' => $formType->createView());
    }

    /**
     * Create new form.
     * @Route("/podoba/novy", name="form_new")
     * @Template("AppBundle:MaterialForm:new.html.twig")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     */
    public function newAction(Request $request){
        $form = new Form();
        $formType = $this->createForm(FormType::class, $form);
        $formType->handleRequest($request);
        if ($formType->isSubmitted() && $formType->isValid()){
            $manager = $this->get('app.service.manager.form');
            $manager->validate($formType);
            if ( $formType->isValid() ){
                $manager->update($form);
                $this->addFlash('notice', 'Podoba uložena');
                return $this->redirectToRoute('form_show');
            }
        }
        return array('form' => $formType->createView());
    }

}