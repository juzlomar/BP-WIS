<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Service\Functionality;


use AppBundle\Entity\Bringing;
use AppBundle\Entity\InternalTransport;
use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialPart;
use AppBundle\Entity\Transfer;
use AppBundle\Entity\Transport;
use AppBundle\Entity\Warehouse;
use AppBundle\Repository\MaterialRepository;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;

class TransportFunctionality
{
    /** @var  MaterialRepository */
    private $materialRepository;

    /**
     * TransportFunctionality constructor.
     * @param MaterialRepository $materialRepository
     */
    public function __construct(MaterialRepository $materialRepository)
    {
        $this->materialRepository = $materialRepository;
    }

    /**
     * Check whether given material in a form is in the given warehouse.
     * @param Warehouse|null $warehouse
     * @param FormInterface $materialForm
     * @return mixed|null
     */
    public function checkMaterials(Warehouse $warehouse = null, FormInterface $materialForm){
        try{
            $newMaterial = $this->materialRepository->findCurrentById($materialForm->getData()->getId());
        }catch(\Doctrine\ORM\NoResultException $ex){
            $materialForm->addError(new FormError('Materiál s tímto ID neexistuje'));
            return null;
        }

        if ($newMaterial->getWarehouse() !== $warehouse) {
            $materialForm->addError(new FormError('Materiál není na provozovně '.$warehouse->getName()));
        }
        return $newMaterial;
    }

    /**
     * Check whether part in a form is part of the given material and has right state and form.
     * @param FormInterface $partForm
     * @param Material $material
     * @param $transportType
     * @param $state
     * @return mixed|null
     */
    public function checkPart(FormInterface $partForm, Material $material, $transportType, $state){
        $newPart = null;
        foreach($material->getParts() as $part){
            if ($partForm->getData()->getId() == $part->getId()){
                $newPart = $part;
                break;
            }
        }
        if ($newPart === null){
            $partForm->addError(new FormError('Materiál s ID '.$material->getId().' nemá část s tímto ID'));
            return null;
        }
        if ($newPart->getState() != $state){
            $partForm->addError(new FormError('Část není ve stavu k převozu'));
        }
        if ($transportType == 'transfer' && $newPart->getCurrentForm() !== $material->getFinalForm()){
            $partForm->addError(new FormError('Část není ve finální podobě'));
        }
        $newPart->setNote($partForm->getData()->getNote());
        if ($transportType == 'internal_transport'){
            $newPart->setState($partForm->getData()->getState());
        }
        return $newPart;
    }

    /**
     * Finds all transports which took place in  given interval.
     * @param Collection $transports
     * @param \DateTime $from
     * @param \DateTime $to
     * @return Collection
     */
    public function checkDates(Collection $transports, \DateTime $from, \DateTime $to){
        foreach($transports as $key => $transport){
            if ($transport->getDate() < $from){
                $transports->remove($key);
            }
            if ($transport->getDate() > $to){
                $transports->remove($key);
            }
        }
        return $transports;
    }

    /**
     * Finds all transports, which were from given partner or warehouse and headed to given partner or warehouse.
     * Najde všechny přepravy, které byly od daného partnera, nebo z dané provozovny a zároveň mířili k danému partnerovi, nebo provozovně.
     * @param Collection $transports
     * @param $from
     * @param $to
     * @return Collection
     */
    public function findByFromTo(Collection $transports, $from, $to){
        foreach($transports as $key => $transport){
            if ($from && $transport->getFrom() !== $from){
                $transports->remove($key);
            }
            if ($to && $transport->getTo() !== $to){
                $transports->remove($key);
            }
        }
        return $transports;
    }
}