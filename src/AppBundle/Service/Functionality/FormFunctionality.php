<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Service\Functionality;


use AppBundle\Entity\Form;
use AppBundle\Entity\WayOfStoring;
use Doctrine\Common\Collections\Collection;

class FormFunctionality
{
    /**
     * Decide whether given form can be stored given way of storing.
     * Rozhodne zda daná podoba může být uskladněna daným způsobem.
     * @param Form $currentForm
     * @param WayOfStoring $wayOfStoring
     * @return bool
     */
    public function canBeStored(Form $currentForm, WayOfStoring $wayOfStoring){
        foreach($currentForm->getWayOfStoring() as $storing){
            if ($storing === $wayOfStoring){
                return true;
            }
        }
        return false;
    }

    /**
     * Decide whether given form can be processed to given final form.
     * Rozhodne zda první podoba může být zpracována na druhou.
     * @param Form $currentForm
     * @param Form $finalForm
     * @return bool
     */
    public function canBeFinal(Form $currentForm, Form $finalForm){
        foreach($currentForm->getFinalForms() as $form){
            if ($form === $finalForm)
                return true;
        }
        return false;
    }

    /**
     * Add given form all forms which the form can be processed to.
     * Nastaví dané podobě všechny možné finální podoby, na které může být zpracována.
     * @param Form $form
     * @return Form|null
     */
    public function setFinalForms(Form $form){
        if (! $form->getFinalForms()->contains($form))
            $form->addFinalForm($form);
        foreach($form->getWayOfProcessing() as $processing){
            $outputForm = $processing->getOutputForm();
            if ($outputForm->getFinalForms()->contains($form)){
                return null;
            }
            foreach($outputForm->getFinalForms() as $finalForm){
                if ( ! $form->getFinalForms()->contains($finalForm) ){
                    $form->addFinalForm($finalForm);
                }
            }
            if ( $this->setFinalForms($outputForm) === null) return null;
        }
        return $form;
    }

}