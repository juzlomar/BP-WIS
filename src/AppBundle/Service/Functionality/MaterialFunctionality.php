<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Service\Functionality;


use AppBundle\Entity\Bringing;
use AppBundle\Entity\Business;
use AppBundle\Entity\BusinessPartner;
use AppBundle\Entity\Form;
use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialPart;
use AppBundle\Entity\PartProcessing;
use AppBundle\Entity\Processing;
use AppBundle\Entity\Transfer;
use AppBundle\Entity\Transport;
use AppBundle\Entity\Warehouse;
use AppBundle\Entity\WayOfProcessing;
use AppBundle\Entity\WayOfStoring;
use AppBundle\Repository\MaterialPartRepository;
use AppBundle\Repository\MaterialRepository;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Form\FormInterface;

class MaterialFunctionality
{
    /** @var  MaterialRepository */
    private $materialRepository;
    /** @var  MaterialPartRepository */
    private $partRepository;
    /** @var  array */
    private $states;

    /**
     * MaterialFunctionality constructor.
     * @param MaterialRepository $materialRepository
     * @param MaterialPartRepository $partRepository
     * @param array $states
     */
    public function __construct(MaterialRepository $materialRepository, MaterialPartRepository $partRepository, array $states)
    {
        $this->materialRepository = $materialRepository;
        $this->partRepository = $partRepository;
        $this->states = $states;
    }

    public function findMaterialsToTransfer(Collection $materials){
        foreach($materials as $key => $material){
            foreach($material->getParts() as $part){
                if ($part->getCurrentForm() !== $material->getFinalForm()){
                    $material->removePart($part);
                }
            }
            if($material->getParts()->isEmpty()){
                $materials->remove($key);
            }
        }
        return $materials;
    }

    /**
     * Přepraví daný materiál.
     * @param Material $material
     * @param Transport $transport
     */
    public function transportMaterial(Material $material, Transport $transport){
        if ( $material->getId() === null ){
            $this->materialRepository->persist($material);
            if ( ! ($transport instanceof  Bringing)){
                foreach($material->getBusinesses() as $business){
                    if ( $business->getTransport() !== $transport )
                        $business->addMaterial($material);
                }
            }
        }
        if ($transport instanceof Transfer) {
            foreach ($material->getParts() as $part) {
                $part->setState($this->states['transferred']);
            }
        }else{
            $material->setWarehouse($transport->getTo());
        }
        foreach($material->getParts() as $part){
            if ( ! $part->getWeightBrutto() ){
                $part->setWeightBrutto($part->getWeightNetto());
            }
        }
    }

    /**
     * Decide whether two materials can be merged.
     * Rozhodne zda dva materiály mohou být sloučeny.
     * @param Material $material1
     * @param Material $material2
     * @return bool
     */
    public function canBeMerged(Material $material1, Material $material2){
        if ($material1->getPlastics() != $material2->getPlastics()
            || $material1->getSpecification() != $material2->getSpecification()
            || $material1->getFinalForm() !== $material2->getFinalForm()){
            return false;
        }
        return true;
    }

    /**
     * Merge given materials into one.
     * Sloučí pole materiálů do jednoho.
     * @param $materials
     * @return mixed
     */
    public function merge($materials){
        $keys = array_keys($materials);
        $newMaterial = clone $materials[$keys[0]];
        $newMaterial->getBusinesses()->clear();
        foreach($materials as $material){
            if ($newMaterial->getColor() != $material->getColor()){
                $newMaterial->setColor('mix');
                $newMaterial->setColorEng('mix');
            }
        }
        $this->materialRepository->persist($newMaterial);
        return $newMaterial;
    }

    /**
     * Copy given material and move given parts to new material.
     * @param Material $material
     * @param array $parts
     * @return Material
     */
    public function divide(Material $material, array $parts){
        $newMaterial = clone $material;
        foreach($parts as $part){
            $newMaterial->addPart($part);
            $part->setMaterial($newMaterial);
            $material->removePart($part);
        }
        return $newMaterial;
    }

    /**
     * Create new part which is output of given Processing and add the part to material.
     * @param Material $material
     * @param MaterialPart $part
     * @param Processing $processing
     */
    public function createPart(Material $material, MaterialPart $part, Processing $processing){
        $part->setMaterial($material);
        $material->addPart($part);
        $part->setOutputOfProcessing($processing);
        if ( ! $part->getWeightBrutto() ){
            $part->setWeightBrutto($part->getWeightNetto());
        }
        $this->partRepository->persist($part);
    }

    public function processPart(PartProcessing $input, Processing $processing){
        $input->setProcessing($processing);
        $input->getPart()->setInputOfProcessing($input);
        $input->getPart()->setState($this->states['processed']);
    }

    /**
     * Finds materials which come from given partner and were transferred to given Partner to or were not transferred yet.
     * @param Collection $materials
     * @param Collection $from
     * @param Collection $to
     * @return array
     */
    public function findMaterialByPartners(Collection $materials, Collection $from , Collection $to){
        $results = array();
        foreach($materials as $material){
            $transport = $this->findTransports($material);
            if( $from->contains($transport['bringing_partner']) ){
                if ($material->getParts()->first()->getState() == $this->states['transferred']){
                    if (  $to->contains($transport['transfer_partner']) )
                        array_push($results, array('material'=> $material, 'transport' => $transport));
                }else{
                    array_push($results, array('material'=> $material, 'transport' => $transport));
                }
            }
        }
        return $results;
    }

    /**
     * Find bringing, transfer and last transport of the material.
     * @param Material $material
     * @return array
     */
    public function findTransports(Material $material){
        $transport = array();
        $transport['transfer_partner'] = null;
        $transport['transfer_prize'] = null;
        $transport['bringing_warehouse'] = null;
        $transport['bringing_partner'] = null;
        $transport['bringing_prize'] = null;
        $transport['last_transport_prize'] = null;
        $transport['last_transport_date'] = null;
        if($material->getBusinesses()->first()){
            $transport['last_transport_prize'] = $material->getBusinesses()->first()->getMaterialPrice();
            $transport['last_transport_date'] = $material->getBusinesses()->first()->getTransport()->getDate();
        }
        foreach($material->getBusinesses() as $business){
            if ($business->getTransport() instanceof Bringing){
                $transport['bringing_warehouse'] = $business->getTransport()->getTo();
                $transport['bringing_partner'] = $business->getTransport()->getFrom();
                $transport['bringing_prize'] = $business->getMaterialPrice();
            }else if($business->getTransport() instanceof Transfer){
                $transport['transfer_partner'] = $business->getTransport()->getTo();
                $transport['transfer_prize'] = $business->getMaterialPrice();
            }
            if ($business->getTransport()->getDate() > $transport['last_transport_date']){
                $transport['last_transport_prize'] = $business->getMaterialPrice();
                $transport['last_transport_date'] = $business->getTransport()->getDate();
            }
        }
        return $transport;
    }

    /**
     * Find parts whose currentForm is in forms.
     * @param Collection $materials
     * @param Collection $forms
     * @return Collection
     */
    public function findByForms(Collection $materials, Collection $forms){
        foreach($materials as $key => $material){
            foreach($material->getParts() as $part){
                if (! $forms->contains($part->getCurrentForm())){
                    $material->removePart($part);
                }
            }
            if($material->getParts()->isEmpty()){
                $materials->remove($key);
            }
        }
        return $materials;
    }

    /**
     * Finds materials whose finalForm is in forms.
     * @param Collection $materials
     * @param Collection $forms
     * @return Collection
     */
    public function findByFinalForms(Collection $materials, Collection $forms){
        foreach($materials as $key => $material){
            if (!$forms->contains($material->getFinalForm())){
                unset($materials[$key]);
            }
        }
        return $materials;
    }

    /**
     * Finds materials that were transported to given warehouse.
     * @param Collection $materials
     * @param Warehouse $warehouse
     * @return Collection
     */
    public function findByWarehouseTo(Collection $materials, Warehouse $warehouse){
        foreach($materials as $key => $material){
            foreach($material->getBusinesses() as $business){
                if ($business->getTransport()->getTo() !== $warehouse){
                    $material->removeBusiness($business);
                }
            }
            if ($material->getBusinesses()->isEmpty()){
                $materials->remove($key);
            }
        }
        return $materials;
    }

    /**
     * Finds materials that were transported from given warehouse.
     * @param Collection $materials
     * @param Warehouse $warehouse
     * @return Collection
     */
    public function findByWarehouseFrom(Collection $materials, Warehouse $warehouse){
        foreach($materials as $key => $material){
            foreach($material->getBusinesses() as $business){
                if ($business->getTransport()->getFrom() !== $warehouse){
                    $material->removeBusiness($business);
                }
            }
            if ($material->getBusinesses()->isEmpty()){
                $materials->remove($key);
            }
        }
        return $materials;
    }

    /**
     * Finds parts of the material which existed in given time.
     * @param Material $material
     * @param \DateTime $date
     * @return Material
     */
    public function findPartsByDate(Material $material, \DateTime $date){
        foreach($material->getParts() as $part){
            if ($part->getOutputOfProcessing()){
                if ($part->getOutputOfProcessing()->getDate() > $date){
                    $material->removePart($part);
                }
            }
            if ($part->getInputOfProcessing()){
                if ($part->getInputOfProcessing()->getProcessing()->getDate() < $date){
                    $material->removePart($part);
                }
            }
        }
        return $material;
    }

    /**
     * Sorts material according their plastics.
     * @param Collection $materials
     * @return array Array whose keys are different plastics and values are materials which are from that plastics.
     */
    public function sortByPlastics(Collection $materials){
        $plastics = array();
        foreach($materials as $material){
            if (array_key_exists($material->getPlastics(), $plastics) === false){
                $plastics[$material->getPlastics()] = array();
            }
            array_push($plastics[$material->getPlastics()], $material);
        }
        return $plastics;
    }

    /**
     * Sorts material according their english specification.
     * @param $materials
     * @return array Array whose keys are different english specification and values are materials which are from that english specification.
     */
    public function sortBySpecificationEng($materials){
        $specification = array();
        foreach($materials as $material){
            if (array_key_exists($material->getSpecificationEng(), $specification) === false){
                $specification[$material->getSpecificationEng()] = array();
            }
            array_push($specification[$material->getSpecificationEng()], $material);
        }
        return $specification;
    }

    /**
     * Sorts material according their english color.
     * @param $materials
     * @return array Array whose keys are different english color and values are materials which have english color.
     */
    public function sortByColorEng($materials){
        $color = array();
        foreach($materials as $material){
            if (!array_key_exists($material->getColorEng(), $color)){
                $color[$material->getColorEng()] = $this->countWeights($material);
            }else{
                $color[$material->getColorEng()] += $this->countWeights($material);
            }
        }
        return $color;
    }

    /**
     * Sorts material according their specification.
     * @param $materials
     * @return array Array whose keys are different specification and values are materials which are from that specification.
     */
    public function sortBySpecification($materials){
        $specification = array();
        foreach($materials as $material){
            if (array_key_exists($material->getSpecification(), $specification) === false){
                $specification[$material->getSpecification()] = array();
            }
            array_push($specification[$material->getSpecification()], $material);
        }
        return $specification;
    }

    /**
     * Sorts material according their color..
     * @param $materials
     * @return array Array whose keys are different color and values are materials which have the color.
     */
    public function sortByColor($materials){
        $color = array();
        foreach($materials as $material){
            if (!array_key_exists($material->getColor(), $color)){
                $color[$material->getColor()] = $this->countWeights($material);
            }else{
                $color[$material->getColor()] += $this->countWeights($material);
            }
        }
        return $color;
    }

    /**
     * Count sum of weight materials.
     * @param Material $material
     * @return int
     */
    public function countWeights(Material $material){
        $weight = 0;
        foreach($material->getParts() as $part){
            $weight += $part->getWeightNetto();
        }
        return $weight;
    }

}