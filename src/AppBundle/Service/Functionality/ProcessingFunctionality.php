<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Service\Functionality;


use AppBundle\Entity\Form;
use AppBundle\Entity\MaterialPart;
use AppBundle\Entity\Processing;
use AppBundle\Entity\Warehouse;
use AppBundle\Repository\MaterialPartRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;

class ProcessingFunctionality
{

    /** @var FormFunctionality */
    private $formFunctionality;
    /** @var  MaterialFunctionality */
    private $materialFunctinality;
    /** @var  MaterialPartRepository */
    private $partRepository;

    /**
     * ProcessingFunctionality constructor.
     * @param FormFunctionality $formService
     * @param MaterialFunctionality $materialFunctionality
     * @param MaterialPartRepository $partRepository
     */
    public function __construct(FormFunctionality $formFunctionality, MaterialFunctionality $materialFunctionality, MaterialPartRepository $partRepository)
    {
        $this->formFunctionality = $formFunctionality;
        $this->materialFunctinality = $materialFunctionality;
        $this->partRepository = $partRepository;
    }


    /**
     * Check whether material of inputs of processing can be processed together.
     * @param Collection $partProcessings
     * @param FormInterface $form
     */
    public function checkMaterials(Collection $partProcessings, FormInterface $form){
        if( $partProcessings->isEmpty() ) return;
        $material1 = null;
        foreach($partProcessings as $partProcessing){
            if ($material1){
              if ( $partProcessing->getPart()->getMaterial()) {
                  if (!$this->materialFunctinality->canBeMerged($material1, $partProcessing->getPart()->getMaterial())) {
                      $form->addError(new FormError('Materiály s ID ' . $material1->getId() . ' a ' . $partProcessing->getPart()->getMaterial()->getId() . ' spolu nemohou být zpracovány'));
                  }
              }
            }else{
                $material1 = $partProcessing->getPart()->getMaterial();
            }
        }
    }

    /**
     * Check whether part form given form is in the warehouse in some from given forms and is in the state to process.
     * @param FormInterface $form_part
     * @param Warehouse $warehouse
     * @param Collection $forms
     * @param $state
     * @return mixed|null
     */
    public function findPartToProcess(FormInterface $form_part, Warehouse $warehouse, Collection $forms, $state){
        try{
            $savedPart = $this->partRepository->findById($form_part->getData()->getId());
        }catch(\Doctrine\ORM\NoResultException $ex){
            $form_part->addError( new FormError('Část s tímto neexistuje'));
            return null;
        }
        if ( ! $forms->contains($savedPart->getCurrentForm())){
            $form_part->get('currentForm')->addError( new FormError('Část není ve vhodné formě na tento způsob zpracování'));
        }
        if ( $savedPart->getMaterial()->getWarehouse() !== $warehouse){
            $form_part->addError( new FormError('Část není na provozovně '.$warehouse->getName()));
        }
        if ( $savedPart->getState() != $state){
            $form_part->get('state')->addError( new FormError('Část není ve stavu ke zpracování'));
        }
        return $savedPart;
    }

    /**
     * Set current form of the part from form part_form and check whether this current form can be stored in a way of storing which is in part_form.
     * @param FormInterface $form_part
     * @param Form $outputForm
     */
    public function checkOutput(FormInterface $form_part, Form $outputForm){
        $output = $form_part->getData();
        $output->setCurrentForm($outputForm);
        if ($output->getWayOfStoring()) {
            if (!$this->formFunctionality->canBeStored($output->getCurrentForm(), $output->getWayOfStoring())) {
                $form_part->get('wayOfStoring')->addError( new FormError('Podoba ' . $outputForm->getTitle() . ' se nedá uskladnit způsobem ' . $output->getWayOfStoring()->getTitle() . '.'));
            }
        }
    }

    /**
     * Check whether processing took place in a given time interval.
     * @param $processings
     * @param \DateTime $from
     * @param \DateTime $to
     * @return array
     */
    public function checkDates($processings, \DateTime $from, \DateTime $to){
        foreach($processings as $key => $processing){
            if ($processing->getDate() < $from){
                unset($processings[$key]);
            }
            if ($processing->getDate() > $to){
                unset($processings[$key]);
            }
        }
        return $processings;
    }

    /**
     * Get all materials from inputs of given Processing.
     * @param Processing $processing
     * @return array
     */
    public function getMaterials(Processing $processing){
        $materials = array();
        foreach($processing->getInputs() as $input){
            $materials[$input->getPart()->getMaterial()->getId()] = $input->getPart()->getMaterial();
        }
        return $materials;
    }

    /**
     * Find processings which took place in the given warehouse and with the given way of processing.
     * @param $processings
     * @param $warehouse
     * @param $way
     * @return mixed
     */
    public function findByWarehouseWayOfProcessing($processings, $warehouse, $way){
        foreach($processings as $key => $processing){
            if ($warehouse && $processing->getWarehouse() !== $warehouse){
                unset($processings[$key]);
            }
            if ($way && $processing->getWayOfProcessing() !== $way){
                unset($processings[$key]);
            }
        }
        return $processings;
    }
}