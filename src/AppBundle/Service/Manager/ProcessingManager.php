<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Service\Manager;


use AppBundle\Entity\Processing;
use AppBundle\Entity\Warehouse;
use AppBundle\Entity\WayOfProcessing;
use AppBundle\Repository\FormRepository;
use AppBundle\Repository\MaterialRepository;
use AppBundle\Repository\ProcessingRepository;
use AppBundle\Repository\WayOfProcessingRepository;
use AppBundle\Service\Functionality\FormFunctionality;
use AppBundle\Service\Functionality\MaterialPartService;
use AppBundle\Service\Functionality\MaterialFunctionality;
use AppBundle\Service\Functionality\ProcessingFunctionality;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;

class ProcessingManager
{
    /** @var  FormFunctionality */
    private $formFunctionality;
    /** @var  MaterialFunctionality */
    private $materialFunctionality;
    /** @var ProcessingFunctionality */
    private $processingFunctinality;
    /** @var  FormRepository */
    private $formRepository;
    /** @var  MaterialRepository */
    private $materialRepository;
    /** @var ProcessingRepository */
    private $processingRepository;
    /** @var  WayOfProcessingRepository */
    private $wayOfProcessingRepository;
    /** @var  array */
    private $states;

    /**
     * ProcessingManager constructor.
     * @param FormFunctionality $formFunctionality
     * @param MaterialFunctionality $materialFunctionality
     * @param ProcessingFunctionality $processingFunctionality
     * @param FormRepository $formRepository
     * @param ProcessingRepository $processingRepository
     * @param MaterialRepository $materialRepository
     * @param WayOfProcessingRepository $wayOfProcessingRepository
     * @param array $states
     */
    public function __construct(FormFunctionality $formFunctionality, MaterialFunctionality $materialFunctionality, ProcessingFunctionality $processingFunctionality, FormRepository $formRepository, MaterialRepository $materialRepository, ProcessingRepository $processingRepository, WayOfProcessingRepository $wayOfProcessingRepository, array $states)
    {
        $this->formFunctionality = $formFunctionality;
        $this->materialFunctionality = $materialFunctionality;
        $this->processingFunctinality = $processingFunctionality;
        $this->formRepository = $formRepository;
        $this->processingRepository = $processingRepository;
        $this->materialRepository = $materialRepository;
        $this->wayOfProcessingRepository = $wayOfProcessingRepository;
        $this->states = $states;
    }


    /**
     * @param FormRepository $formRepository
     */
    public function setFormRepository($formRepository)
    {
        $this->formRepository = $formRepository;
    }

    /**
     * @param FormFunctionality $formFunctionality
     */
    public function setFormFunctionality($formFunctionality)
    {
        $this->formFunctionality = $formFunctionality;
    }

    /**
     * @param mixed $processingFunctinality
     */
    public function setProcessingFunctinality($processingFunctinality)
    {
        $this->processingFunctinality = $processingFunctinality;
    }

    /**
     * @param MaterialRepository $materialRepository
     */
    public function setMaterialRepository($materialRepository)
    {
        $this->materialRepository = $materialRepository;
    }


    /**
     * @param ProcessingRepository $processingRepository
     */
    public function setProcessingRepository($processingRepository)
    {
        $this->processingRepository = $processingRepository;
    }

    /**
     * @param MaterialFunctionality $materialFunctionality
     */
    public function setMaterialFunctionality($materialFunctionality)
    {
        $this->materialFunctionality = $materialFunctionality;
    }

    /**
     * @param WayOfProcessingRepository $wayOfProcessingRepository
     */
    public function setWayOfProcessingRepository($wayOfProcessingRepository)
    {
        $this->wayOfProcessingRepository = $wayOfProcessingRepository;
    }

    /**
     * Find material which can be processed in the given warehouse, given way of processing.
     * @param Warehouse $warehouse
     * @param WayOfProcessing $wayOfProcessing
     * @return \Doctrine\Common\Collections\ArrayCollection|\Doctrine\Common\Collections\Collection|null
     */
    public function findMaterials(Warehouse $warehouse, WayOfProcessing $wayOfProcessing){
        if ( ! $warehouse->getWaysOfProcessing()->contains($wayOfProcessing) ){
            return null;
        }
        $materials = $this->materialRepository->findByWarehousesStates($warehouse, $this->states['processing']);
        $forms = $wayOfProcessing->getEntranceForms();
        $finalForms = $wayOfProcessing->getOutputForm()->getFinalForms();
        $materials = $this->materialFunctionality->findByForms($materials, $forms);
        $materials = $this->materialFunctionality->findByFinalForms($materials, $finalForms);
        return $materials;
    }

    /**
     * Validate data from ProcessingType form.
     * @param FormInterface $form
     * @return array|void
     */
    public function validate(FormInterface $form){
        $processing = $form->getData();
        if (! $processing->getWarehouse() || ! $processing->getWayOfProcessing()) return array();
        foreach($form->get('inputs') as $input){
            $part = $this->processingFunctinality->findPartToProcess($input->get('part'), $processing->getWarehouse(), $processing->getWayOfProcessing()->getEntranceForms(), $this->states['processing']);
            if ($part === null) {
                continue;
            }
            $input->getData()->setPart($part);

        }
        $this->processingFunctinality->checkMaterials($processing->getInputs(), $form);
        foreach($form->get('outputs') as $output){
            $this->processingFunctinality->checkOutput($output, $processing->getWayOfProcessing()->getOutputForm());
        }
    }

    /**
     * Save new processing.
     * @param Processing $processing
     */
    public function save(Processing $processing)
    {
        $material = null;
        foreach($processing->getInputs() as $input){
            $material = $input->getPart()->getMaterial();
            $inputs[$input->getPart()->getMaterial()->getId()] = $input->getPart()->getMaterial();
        }
        if (count($inputs) > 1){
            $material = $this->materialFunctionality->merge($inputs);
        }

        foreach($processing->getOutputs() as $output){
            $this->materialFunctionality->createPart($material, $output, $processing);
        }
        foreach($processing->getInputs() as $input){
            $this->materialFunctionality->processPart($input, $processing);
        }
        $this->processingRepository->save($processing);
    }

    /**
     * Find processings according data from the form.
     * @param FormInterface $form
     * @return array
     */
    public function find(FormInterface $form){
        $warehouse = $form->get('warehouse')->getData();
        $wayOfProcessing = $form->get('wayOfProcessing')->getData();
        $processings = $this->processingRepository->findAll();
        $processings = $this->processingFunctinality->findByWarehouseWayOfProcessing($processings, $warehouse, $wayOfProcessing);
        return $this->processingFunctinality->checkDates($processings, $form->get('start_date')->getData(), $form->get('end_date')->getData());
    }

    public function findById($id){
        try{
            $processing = $this->processingRepository->findById($id);
        }catch(\Doctrine\ORM\NoResultException $ex){
            return null;
        }
        return $processing;
    }

    /**
     * Find all saved ways of processing.
     * @return array|\Doctrine\Common\Collections\ArrayCollection
     */
    public function findWays(){
        return $this->wayOfProcessingRepository->findAll();
    }

    public function findWayByTitle($title){
        return $this->wayOfProcessingRepository->findByTitle($title);
    }

    /**
     * Validate new way of processing.
     * @param FormInterface $formType
     */
    public function validateWay(FormInterface $formType){
        $forms = $this->formRepository->findAll();
        foreach($forms as $form){
            $form->getFinalForms()->clear();
        }
        foreach($forms as $form){
            if ( $form->getFinalForms()->count() === 0 ){
                if ($this->formFunctionality->setFinalForms($form) === null){
                    $formType->addError(new FormError('Nesprávné vstupní a výstupní podoby.'));
                    return;
                }
            }
        }
    }


    public function updateWay(WayOfProcessing $wayOfProcessing){
        $this->wayOfProcessingRepository->save($wayOfProcessing);;
    }
}