<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Service\Manager;


use AppBundle\Entity\Warehouse;
use AppBundle\Repository\WarehouseRepository;

class WarehouseManager
{
    /** @var  WarehouseRepository */
    private $warehouseRepository;

    /**
     * WarehouseManager constructor.
     * @param WarehouseRepository $warehouseRepository
     */
    public function __construct(WarehouseRepository $warehouseRepository)
    {
        $this->warehouseRepository = $warehouseRepository;
    }


    public function updateWarehouse(Warehouse $warehouse){
        $this->warehouseRepository->save($warehouse);
        return;
    }

    public function activate($id){
        $warehouse = $this->warehouseRepository->findById($id);
        if (! $warehouse){
            return null;
        }
        $warehouse->setIsActive(true);
        $this->warehouseRepository->save($warehouse);
        return $warehouse;
    }

    public function findWarehouseById($id){
        return $this->warehouseRepository->findById($id);
    }

    public function findWarehouses(){
        return $this->warehouseRepository->findAll();
    }

    public function deactivate($id){
        $warehouse = $this->warehouseRepository->findById($id);
        if ($warehouse === null){
            return null;
        }
        $warehouse->setIsActive(false);
        $this->warehouseRepository->save($warehouse);
        return $warehouse;
    }
}