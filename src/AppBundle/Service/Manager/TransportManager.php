<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Service\Manager;

use AppBundle\Entity\Bringing;
use AppBundle\Entity\Transfer;
use AppBundle\Entity\Transport;
use AppBundle\Repository\MaterialRepository;
use AppBundle\Repository\TransportRepository;
use AppBundle\Service\Functionality\FormFunctionality;
use AppBundle\Service\Functionality\MaterialFunctionality;
use AppBundle\Service\Functionality\TransportFunctionality;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class TransportManager
{
    /** @var  FormFunctionality */
    private $formFunctionality;
    /** @var  MaterialFunctionality */
    private $materialFunctionality;
    /** @var  TransportFunctionality */
    private $transportFunctionality;
    /** @var  MaterialRepository */
    private $materialRepository;
    /** @var  TransportRepository */
    private $transportRepository;
    /** @var  array */
    private $states;

    /**
     * TransportManager constructor.
     * @param FormFunctionality $formFunctionality
     * @param MaterialFunctionality $materialFunctionality
     * @param TransportFunctionality $transportFunctionality
     * @param MaterialRepository $materialRepository
     * @param TransportRepository $transportRepository
     * @param array $states
     */
    public function __construct(FormFunctionality $formFunctionality, MaterialFunctionality $materialFunctionality, TransportFunctionality $transportFunctionality, MaterialRepository $materialRepository, TransportRepository $transportRepository, array $states)
    {
        $this->formFunctionality = $formFunctionality;
        $this->materialFunctionality = $materialFunctionality;
        $this->transportFunctionality = $transportFunctionality;
        $this->materialRepository = $materialRepository;
        $this->transportRepository = $transportRepository;
        $this->states = $states;
    }

    /**
     * Find materials which can be transported from given warehouse.
     * @param $action
     * @param $warehouse
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    private function findMaterials($action, $warehouse){
        $materials = $this->materialRepository->findByWarehousesStates($warehouse, $this->states['expedition']);
        if ($action == 'transfer'){
            $materials = $this->materialFunctionality->findMaterialsToTransfer($materials);
        }
        return $materials;
    }

    public function findMaterialsToInternalTransport($warehouse){
        return $this->findMaterials('internal_transport', $warehouse);
    }

    public function findMaterialsToTransfer($warehouse){
        return $this->findMaterials('transfer', $warehouse);
    }

    /**
     * Validate transport.
     * @param FormInterface $form
     */
    public function validate(FormInterface $form){
        $transport = $form->getData();
        if ($transport instanceof Bringing){
            $this->validateBringing($form);
            return;
        }
        $type = 'internal_transport';
        if ($transport instanceof Transfer) {
            $type = 'transfer';
        }
        $this->validateTransfer($form, $type);
    }

    /**
     * Validate internal transport or transfer.
     * @param FormInterface $form
     * @param $type
     */
    private function validateTransfer(FormInterface $form, $type){
        $transport = $form->getData();
        $newMaterial = null;
        foreach($form->get('materials') as $businessForm){
            foreach($businessForm->get('materials') as $materialForm){
                $newMaterial = $this->transportFunctionality->checkMaterials($transport->getFrom(), $materialForm);
                if ($newMaterial === null) continue;
                $parts = array();
                foreach($materialForm->get('parts') as $partForm){
                    $newPart = $this->transportFunctionality->checkPart($partForm, $newMaterial, $type, $this->states['expedition']);
                    if ($newPart === null) continue;
                    array_push($parts, $newPart);
                }
                if (count($parts) != $newMaterial->getParts()->count()){
                    $newMaterial = $this->materialFunctionality->divide($newMaterial, $parts);
                }

                $business = $businessForm->getData();
                $business->removeMaterial($materialForm->getData());
                $business->addMaterial($newMaterial);
            }
        }
    }

    /**
     * Validate bringing.
     * @param FormInterface $form
     * @return FormInterface
     */
    private function validateBringing(FormInterface $form){
        foreach($form->get('materials') as $index => $business){
            foreach($business->get('materials') as $materialIndex => $material) {
                $finalForm = $material->get('finalForm')->getData();
                $currentForm = $material->get('currentForm')->getData();
                if ($currentForm && $finalForm) {
                    if (!$this->formFunctionality->canBeFinal($currentForm, $finalForm)) {
                        $material->get('currentForm')->addError(new FormError('Podoba ' . $currentForm->getTitle() . ' se nedá zpracovat na ' . $finalForm->getTitle() . '.'));
                    }
                }
                foreach($material->get('parts') as $part){
                    $wayOfStoring = $part->get('wayOfStoring')->getData();
                    if ($currentForm && $wayOfStoring) {
                        if (!$this->formFunctionality->canBeStored($currentForm, $wayOfStoring)) {
                            $part->get('wayOfStoring')->addError(new FormError('Podoba ' . $currentForm->getTitle() . ' se nedá uskladnit způsobem ' . $wayOfStoring->getTitle() . '.'));
                        }
                    }
                    $part->getData()->setCurrentForm($currentForm);
                }
            }
        }
        return $form;
    }

    /**
     * Save transport.
     * @param Transport $transport
     */
    public function save(Transport $transport){
        foreach($transport->getMaterials() as $business){
            foreach($business->getMaterials() as $material){
                $this->materialFunctionality->transportMaterial($material, $transport);
            }
        }
        $transport->countPrizePerKg();
        $this->transportRepository->save($transport);
    }

    /**
     * Find transport according data form form.
     * @param FormInterface $form
     * @return array|\Doctrine\Common\Collections\ArrayCollection|\Doctrine\Common\Collections\Collection
     */
    public function find(FormInterface $form){
        $from = $form->get('from')->getData();
        $to = $form->get('to')->getData();
        switch($form->getName()){
            case 'bringing_filter':
                $transports = $this->transportRepository->findBringing();
                break;
            case 'transfer_filter':
                $transports = $this->transportRepository->findTransfer();
                break;
            case 'internal_transport_filter':
                $transports = $this->transportRepository->findInternalTransport();
                break;
            default:
                $transports = $this->transportRepository->findAll();
        }
        $transports = $this->transportFunctionality->findByFromTo($transports, $from, $to);

        $transports = $this->transportFunctionality->checkDates($transports, $form->get('start_date')->getData(), $form->get('end_date')->getData());
        return $transports;
    }

    /**
     * Find transport by ID.
     * @param $id
     * @return mixed|null
     */
    public function findById($id){
        try{
            $transport = $this->transportRepository->findById($id);
        }catch(\Doctrine\ORM\NoResultException $ex){
            return null;
        }
        foreach($transport->getMaterials() as $business){
            foreach($business->getMaterials() as $material){
                $this->materialFunctionality->findPartsByDate($material, $transport->getDate());
            }
        }
        return $transport;
    }

}