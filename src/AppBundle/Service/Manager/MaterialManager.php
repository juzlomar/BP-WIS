<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Service\Manager;


use AppBundle\Entity\Material;
use AppBundle\Repository\MaterialRepository;
use AppBundle\Repository\ProcessingRepository;
use AppBundle\Repository\WarehouseRepository;
use AppBundle\Service\Functionality\FormFunctionality;
use AppBundle\Service\Functionality\MaterialPartService;
use AppBundle\Service\Functionality\MaterialFunctionality;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;

class MaterialManager
{
    /** @var  FormFunctionality */
    private $formFunctionality;
    /** @var  MaterialFunctionality */
    private $materialFunctionality;
    /** @var  MaterialRepository */
    private $materialRepository;
    /** @var  ProcessingRepository */
    private $processingRepository;
    /** @var WarehouseRepository */
    private  $warehouseRepository;
    /** @var  array */
    private $languages;
    /** @var  array */
    private $reports;
    /** @var  array */
    private $states;

    /**
     * MaterialManager constructor.
     * @param FormFunctionality $formFunctionality
     * @param MaterialFunctionality $materialFunctionality
     * @param MaterialRepository $materialRepository
     * @param ProcessingRepository $processingRepository
     * @param WarehouseRepository $warehouseRepository
     * @param array $languages
     * @param array $reports
     * @param array $states
     */
    public function __construct(FormFunctionality $formFunctionality, MaterialFunctionality $materialFunctionality, MaterialRepository $materialRepository, ProcessingRepository $processingRepository, WarehouseRepository $warehouseRepository, array $languages, array $reports, array $states)
    {
        $this->formFunctionality = $formFunctionality;
        $this->materialFunctionality = $materialFunctionality;
        $this->materialRepository = $materialRepository;
        $this->processingRepository = $processingRepository;
        $this->warehouseRepository = $warehouseRepository;
        $this->languages = $languages;
        $this->reports = $reports;
        $this->states = $states;
    }

    /**
     * Find all plastics, specifications and colors which have materials in the database.
     * @return array
     */
    public function findAllAtributes(){
        $plastics = array();
        $specifications = array();
        $colors = array();
        $attributes = $this->materialRepository->findAllAtributes();
        foreach($attributes as $attribute){
            $plastics[$attribute['plastics']] = $attribute['plastics'];
            $specifications[$attribute['specification']] = $attribute['specification'];
            $colors[$attribute['color']] = $attribute['color'];
        }
        $result = array('plastics' => $plastics, 'specifications' => $specifications, 'colors' => $colors);
        return $result;
    }

    /**
     * Find material which has given properties.
     * @param array $data
     * @return array
     */
    public function findByFilter(array $data){
        $materials = $this->materialRepository->findByFilter(
            $data['plastics'],
            $data['specification'],
            $data['color'],
            $data['warehouse'],
            $data['finalForm'],
            $data['currentForm'],
            $data['wayOfStoring'],
            $data['state']
        );
        return $this->materialFunctionality->findMaterialByPartners($materials, $data['from'], $data['to']);
    }

    /**
     * Find material and all its parts by material ID.
     * @param $id
     * @return array|null
     */
    public function findAllById($id){
        try{
            $material = $this->materialRepository->findAllById($id);
        }catch(\Doctrine\ORM\NoResultException $ex){
            return null;
        }
        $parts = array();
        $weight = 0;
        foreach($material->getParts() as $part){
            $parts[$part->getId()] = $part->getId();
            if ($part->getState() == $this->states['processed'])
                $material->removePart($part);
            else
                $weight =+ $part->getWeightNetto();
        }
        $processing = $this->processingRepository->findByParts($parts);
        $result = array('material' => $material, 'processing' => $processing, 'weight' => $weight);
        return $result;
    }

    /**
     * Find material and its parts which now exists.
     * @param $id
     * @return mixed|null
     */
    public function findCurrentById($id){
        try{
            $material = $this->materialRepository->findCurrentById($id);
        }catch(\Doctrine\ORM\NoResultException $ex){
            return null;
        }
        return $material;
    }

    private function getFormData(array $postData){
        $data = array();
        $data['color'] = $postData['color'];
        $data['color_eng'] = $postData['color_eng'];
        $data['finalForm'] = $postData['finalForm'];
        foreach($postData['parts'] as $key => $form_part) {
            $data['parts'][$key]['weightBrutto'] = $form_part['weightBrutto'];
            $data['parts'][$key]['wayOfStoring'] = $form_part['wayOfStoring'];
            $data['parts'][$key]['state'] = $form_part['state'];
            $data['parts'][$key]['note'] = $form_part['note'];
        }
        $data['_token'] = $postData['_token'];
        return $data;
    }

    /**
     * Validate material data from POST request.
     * @param FormInterface $form
     * @param array $postData
     */
    public function validate(FormInterface $form, array $postData){
        $material = $form->getData();
        $result = $this->getFormData($postData);
        $form->submit($result, false);
        foreach($form->get('parts') as $key => $form_part){
            $part = $form_part->getData();
            if ( $material->getFinalForm()) {
                if (!$this->formFunctionality->canBeFinal($part->getCurrentForm(), $material->getFinalForm())) {
                    $form_part->get('currentForm')->addError( new FormError('Podoba ' . $part->getCurrentForm()->getTitle() . ' se nedá zpracovat na ' . $material->getFinalForm()->getTitle() . '.'));
                }
            }
            if ($part->getWayOfStoring()) {
                if (!$this->formFunctionality->canBeStored($part->getCurrentForm(), $part->getWayOfStoring())) {
                    $form_part->get('wayOfStoring')->addError( new FormError('Podoba ' . $part->getCurrentForm()->getTitle() . ' se nedá uskladnit způsobem ' . $part->getWayOfStoring()->getTitle() . '.'));
                }
            }
        }
    }

    /**
     * Save material.
     * @param Material $material
     */
    public function save(Material $material){
        $this->materialRepository->save($material);
    }

    /**
     * Find material according the data form ReportType.
     * @param array $data
     * @return ArrayCollection|\Doctrine\Common\Collections\Collection
     */
    public function findMaterialToReport(array $data){
        $warehouse = $data['warehouse'];
        $from = $data['start_date'];
        $to = $data['end_date'];
        $type = $data['type'];
        if ($type == $this->reports['processing']){
            $materials = $this->materialRepository->findWarehouseProcessing($warehouse, $from, $to);
        }else{
            $materials = $this->materialRepository->findTransports($from, $to);
            if ($type == $this->reports['bringing']){
                $materials = $this->materialFunctionality->findByWarehouseTo($materials, $warehouse);
            }else{
                $materials = $this->materialFunctionality->findByWarehouseFrom($materials, $warehouse);
            }
        }

        return $materials;
    }

    /**
     * Get current state of materials.
     * @param array $data
     * @return array
     */
    public function getState(array $data){
        $warehouse = $data['warehouse'];
        $type = $data['type'];
        if ($warehouse){
            $materials = $this->materialRepository->findByWarehousesStates($warehouse, array($this->states['expedition'], $this->states['processing']));
        }else{
            $warehouses = $this->warehouseRepository->findAll();
            $materials = $this->materialRepository->findByWarehousesStates($warehouses, array($this->states['expedition'], $this->states['processing']));
        }
        $forms = new ArrayCollection();
        $forms->add($data['form']);
        $materials = $this->materialFunctionality->findByForms($materials, $forms);
        $result = $this->materialFunctionality->sortByPlastics($materials);
        foreach($result as $plasticKey => $plastic){
            if ($type == $this->languages['cz'])
                $specification = $this->materialFunctionality->sortBySpecification($plastic);
            else
                $specification = $this->materialFunctionality->sortBySpecificationEng($plastic);
            foreach($specification as $specKey => $spec){
                if ($type == $this->languages['cz'])
                    $specification[$specKey] = $this->materialFunctionality->sortByColor($spec);
                else
                    $specification[$specKey] = $this->materialFunctionality->sortByColorEng($spec);
            }
            $result[$plasticKey] = $specification;
        }

        return $result;
    }
}