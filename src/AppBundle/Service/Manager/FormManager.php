<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Service\Manager;


use AppBundle\Entity\Form;
use AppBundle\Repository\FormRepository;
use AppBundle\Service\Functionality\FormFunctionality;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;

class FormManager
{
    /** @var  FormFunctionality */
    private $formFunctionality;
    /** @var  FormRepository */
    private $formRepository;

    /**
     * FormManager constructor.
     * @param FormFunctionality $formFunctionality
     * @param FormRepository $formRepository
     */
    public function __construct(FormFunctionality $formFunctionality, FormRepository $formRepository)
    {
        $this->formFunctionality = $formFunctionality;
        $this->formRepository = $formRepository;
    }

    /**
     * Find all saved forms.
     * @return array|\Doctrine\Common\Collections\ArrayCollection
     */
    public function find(){
        return $this->formRepository->findAll();
    }

    /**
     * Find form by title.
     * @param $title
     * @return null|object
     */
    public function findByTitle($title){
        return $this->formRepository->findByTitle($title);
    }

    /**
     * Validate new form.
     * @param FormInterface $formInterface
     */
    public function validate(FormInterface $formInterface){
        $forms = $this->formRepository->findAll();
        foreach($forms as $form1){
            $form1->getFinalForms()->clear();
        }
        foreach($forms as $form1){
            if ( $form1->getFinalForms()->count() == 0 ){
                if ($this->formFunctionality->setFinalForms($form1) === null){
                    $formInterface->addError(new FormError( 'Nesprávné způsoby zpracování.'));
                    return;
                }
            }
        }
    }

    /**
     * Update form.
     * @param Form $form
     */
    public function update(Form $form){
        $this->formRepository->save($form);
    }
}