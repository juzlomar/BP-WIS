<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Service\Manager;


use AppBundle\Entity\BusinessPartner;
use AppBundle\Repository\BusinessPartnerRepository;

class PartnerManager
{
    /** @var  BusinessPartnerRepository */
    private $partnerRepository;

    /**
     * PartnerManager constructor.
     * @param BusinessPartnerRepository $partnerRepository
     */
    public function __construct(BusinessPartnerRepository $partnerRepository)
    {
        $this->partnerRepository = $partnerRepository;
    }

    public function update(BusinessPartner $partner){
        $errors = array();
        foreach($partner->getContacts() as $contact){
            $contact->setPartner($partner);
        }
        $this->partnerRepository->save($partner);
        return $errors;
    }

    public function findById($id){
        return $this->partnerRepository->findById($id);
    }

    public function find(){
        return $this->partnerRepository->findAll();
    }
}