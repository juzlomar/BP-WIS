<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Service\Manager;

use AppBundle\Entity\WayOfStoring;
use AppBundle\Repository\WayOfStoringRepository;


class StoringManager
{
    /** @var  WayOfStoringRepository */
    private $wayOfStoringRepository;

    /**
     * StoringManager constructor.
     * @param WayOfStoringRepository $wayOfStoringRepository
     */
    public function __construct(WayOfStoringRepository $wayOfStoringRepository)
    {
        $this->wayOfStoringRepository = $wayOfStoringRepository;
    }

    public function findWays(){
        return $this->wayOfStoringRepository->findAll();
    }

    public function findWayByTitle($title){
        return $this->wayOfStoringRepository->findByTitle($title);
    }


    public function updateWay(WayOfStoring $wayOfStoring){
        $this->wayOfStoringRepository->save($wayOfStoring);
        return;
    }


}