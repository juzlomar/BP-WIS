<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Repository;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\WayOfStoring;

class WayOfStoringRepository extends EntityRepository
{
    public function save(WayOfStoring $wayOfStoring){
        $this->getEntityManager()->persist($wayOfStoring);
        $this->getEntityManager()->flush();
    }

    public function remove(WayOfStoring $wayOfStoring){
        $this->getEntityManager()->remove($wayOfStoring);
        $this->getEntityManager()->flush();
    }

    public function findByTitle($title){
        return $this->find($title);
    }

    public function findAll()
    {
        return new ArrayCollection(parent::findAll());
    }
}