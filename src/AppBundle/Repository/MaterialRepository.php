<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Form;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Material;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\UnitOfWork;

class MaterialRepository extends EntityRepository
{
    public function save(Material $material){
        $this->getEntityManager()->persist($material);
        $this->getEntityManager()->flush();
    }

    public function persist(Material $material){
        $this->getEntityManager()->persist($material);
    }

    public function remove(Material $material){
        $this->getEntityManager()->remove($material);
        $this->getEntityManager()->flush();
    }

    public function findById($id){
        return $this->find($id);
    }

    public function findCurrentById($id){
        $query = $this->getEntityManager()->createQuery(
            'SELECT m, p
              FROM AppBundle\Entity\Material m
              LEFT JOIN m.parts p
              WHERE m.id = :material
              AND p.state != \'zpracovano\'
              ');
        $query
            ->setParameter('material', $id);
        return $query->getSingleResult();
    }

    public function findAllById($id){
        $query = $this->getEntityManager()->createQuery(
            'SELECT m, b, t, p,  warehouse
              FROM AppBundle\Entity\Material m
              LEFT JOIN m.businesses b
              LEFT JOIN b.transport t
              LEFT JOIN m.parts p
              LEFT JOIN m.warehouse warehouse
              WHERE m.id = :material
              ORDER BY t.date ASC
              ');
        $query
            ->setParameter('material', $id);
        return $query->getSingleResult();
    }

    public function findAll()
    {
        return new ArrayCollection(parent::findAll());
    }


    public function findByFilter(array $plastics, array $specifications, array $colors, $warehouses, $finalForms, $currentForms, $waysOfStoring, array $states){
        $query = $this->getEntityManager()->createQuery(
            'SELECT m, w, final, p, curr, storing, b, t FROM AppBundle\Entity\Material m JOIN m.warehouse w JOIN m.finalForm final JOIN m.parts p JOIN p.currentForm curr JOIN p.wayOfStoring storing
                  JOIN m.businesses b JOIN b.transport t
              WHERE
                m.plastics IN (:plastic) AND
                m.specification IN (:specifications) AND
                m.color IN (:colors) AND
                w IN (:warehouses) AND
                final IN (:finals) AND
                curr IN (:currs) AND
                storing IN (:stores) AND
                p.state IN (:states)'
        );
        $query
            ->setParameter('plastic', array_keys($plastics))
            ->setParameter('specifications' , array_keys($specifications))
            ->setParameter('colors', array_keys($colors))
            ->setParameter('warehouses', $warehouses)
            ->setParameter('finals', $finalForms)
            ->setParameter('currs', $currentForms)
            ->setParameter('stores', $waysOfStoring)
        ->setParameter('states', array_keys($states));
        return new ArrayCollection($query->getResult());

    }

    public function findByWarehousesStates($warehouses, $states){
        $query = $this->getEntityManager()->createQuery(
            'SELECT m, p FROM AppBundle\Entity\Material m JOIN m.parts p JOIN m.warehouse w
              WHERE w IN (:warehouses) AND p.state IN (:states)'
        );
        $query
            ->setParameter('warehouses', $warehouses)
            ->setParameter('states', $states);
        return new ArrayCollection($query->getResult());
    }

    public function findTransports($from, $to){
        $query = $this->getEntityManager()->createQuery(
            'SELECT m, p, b, t, pp, ip, op
            FROM AppBundle\Entity\Material m
            JOIN m.parts p
            JOIN m.businesses b
            JOIN b.transport t
            LEFT JOIN p.inputOfProcessing pp
            LEFT JOIN pp.processing ip
            LEFT JOIN p.outputOfProcessing op
            WHERE t.date BETWEEN :from_date AND :to_date'
        );
        $query
            ->setParameter('from_date', $from->format('Y-m-d'))
            ->setParameter('to_date', $to->format('Y-m-d'));
        return new ArrayCollection($query->getResult());
    }

    public function findWarehouseProcessing($warehouse, $from, $to){
        $query = $this->getEntityManager()->createQuery(
            'SELECT m, p, pp, ip, op
            FROM AppBundle\Entity\Material m
            JOIN m.parts p
            LEFT JOIN p.inputOfProcessing pp
            LEFT JOIN pp.processing ip
            LEFT JOIN p.outputOfProcessing op
            WHERE ( ip.warehouse = :warehouse and ip.date BETWEEN :from_date AND :to_date)
            OR ( op.warehouse = :warehouse and op.date BETWEEN :from_date AND :to_date)'
        );
        $query
            ->setParameter('warehouse', $warehouse)
            ->setParameter('from_date',$from->format('Y-m-d'))
            ->setParameter('to_date', $to->format('Y-m-d'));
        return new ArrayCollection($query->getResult());
    }

    public function findAllAtributes(){
        $query = $this->getEntityManager()->createQuery(
            'SELECT m.plastics, m.specification, m.color FROM AppBundle\Entity\Material m'
        );
        return $query->getResult();
    }
}