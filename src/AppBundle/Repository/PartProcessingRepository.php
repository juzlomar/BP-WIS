<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\PartProcessing;

class PartProcessingRepository extends EntityRepository
{
    public function save(PartProcessing $partProcessing){
        $this->getEntityManager()->persist($partProcessing);
        $this->getEntityManager()->flush();
    }

    public function remove(PartProcessing $partProcessing){
        $this->getEntityManager()->remove($partProcessing);
        $this->getEntityManager()->flush();
    }

    public function findById($id){
        return $this->find($id);
    }

    public function findAll()
    {
        return new ArrayCollection(parent::findAll());
    }

    public function persist(PartProcessing $processing){
        $this->getEntityManager()->persist($processing);
    }
}