<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Address;
use Doctrine\ORM\EntityRepository;

class AddressRepository extends EntityRepository
{
    public function save(Address $address){
        $this->getEntityManager()->persist($address);
        $this->getEntityManager()->flush();
    }

    public function remove(Address $address){
        $this->getEntityManager()->remove($address);
        $this->getEntityManager()->flush();
    }

    public function findById($id){
        return $this->find($id);
    }

    public function findAll()
    {
        return new ArrayCollection(parent::findAll());
    }
}