<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Repository;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Processing;

class ProcessingRepository extends EntityRepository
{
    public function save(Processing $processing){
        $this->getEntityManager()->persist($processing);
        $this->getEntityManager()->flush();
    }

    public function remove(Processing $processing){
        $this->getEntityManager()->remove($processing);
        $this->getEntityManager()->flush();
    }

    public function findById($id){
        $query = $this->getEntityManager()->createQuery(
            'SELECT p, w, wp, ipp, ip, m, op, u
            FROM AppBundle\Entity\Processing p
            LEFT JOIN p.warehouse w
            LEFT JOIN p.wayOfProcessing wp
            LEFT JOIN p.inputs ipp
            LEFT JOIN ipp.part ip
            LEFT JOIN ip.material m
            LEFT JOIN p.outputs op
            LEFT JOIN p.processor u
            WHERE p.id = :id
            ORDER BY m.id'
        );
        $query
            ->setParameter('id', $id);
        return $query->getSingleResult();
    }

    public function findAll()
    {
        return new ArrayCollection(parent::findAll());
    }

    public function findByParts($parts){
        $query = $this->getEntityManager()->createQuery(
            'SELECT p, ip, u, i, o
            FROM AppBundle\Entity\Processing p
            JOIN p.outputs o
            JOIN p.inputs ip
            JOIN ip.part i
            JOIN p.processor u
            WHERE o.id in (:parts)
            OR i.id in (:parts)
            ORDER BY p.date ASC'
        );
        $query
            ->setParameter('parts', $parts);
        return new ArrayCollection($query->getResult());
    }
}