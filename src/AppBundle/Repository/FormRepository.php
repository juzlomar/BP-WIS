<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Repository;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Form;

class FormRepository extends EntityRepository
{
    public function save(Form $form){
        $this->getEntityManager()->persist($form);
        $this->getEntityManager()->flush();
    }

    public function remove(Form $form){
        $this->getEntityManager()->remove($form);
        $this->getEntityManager()->flush();
    }

    public function findByTitle($title){
        return $this->find($title);
    }

    public function findAll()
    {
        return new ArrayCollection(parent::findAll());
    }
}