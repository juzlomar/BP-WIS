<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Repository;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Warehouse;
use Doctrine\ORM\UnitOfWork;

class WarehouseRepository extends EntityRepository
{
    public function save(Warehouse $warehouse){
        $this->getEntityManager()->persist($warehouse);
        $this->getEntityManager()->flush();
    }

    public function remove(Warehouse $warehouse){
        $this->getEntityManager()->remove($warehouse);
        $this->getEntityManager()->flush();
    }

    public function findById($id){
        return $this->find($id);
    }

    public function findAll()
    {
        return new ArrayCollection(parent::findAll());
    }

    public function findActive(){
        return $this->getEntityManager()->createQueryBuilder()->select('w')->from('AppBundle\Entity\Warehouse', 'w')->where('w.isActive = 1');
    }
}