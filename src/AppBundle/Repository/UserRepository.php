<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Repository;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\User;

class UserRepository extends EntityRepository
{
    public function save(User $user){
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }

    public function remove(User $user){
        $this->getEntityManager()->remove($user);
        $this->getEntityManager()->flush();
    }

    public function findByLogin($login){
        return $this->find($login);
    }

    public function findAll()
    {
        return new ArrayCollection(parent::findAll());
    }
}