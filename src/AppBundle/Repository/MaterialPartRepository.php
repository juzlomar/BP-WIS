<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\MaterialPart;

class MaterialPartRepository extends EntityRepository
{
    public function save(MaterialPart $part){
        $this->getEntityManager()->persist($part);
        $this->getEntityManager()->flush();
    }

    public function remove(MaterialPart $part){
        $this->getEntityManager()->remove($part);
        $this->getEntityManager()->flush();
    }

    public function findAll()
    {
        return new ArrayCollection(parent::findAll());
    }

    public function persist(MaterialPart $part){

       $this->getEntityManager()->persist($part);
    }

    public function merge(MaterialPart $part){
        return $this->getEntityManager()->merge($part);
    }

    public function findById($id){
        $query = $this->getEntityManager()->createQuery(
            'SELECT m, p
              FROM AppBundle\Entity\MaterialPart p
              JOIN p.material m
              WHERE p.id = :part
              ');
        $query
            ->setParameter('part', $id);
        return $query->getSingleResult();
    }
}