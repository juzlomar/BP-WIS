<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Business;

class BusinessRepository extends EntityRepository
{
    public function save(Business $business){
        $this->getEntityManager()->persist($business);
        $this->getEntityManager()->flush();
    }

    public function remove(Business $business){
        $this->getEntityManager()->remove($business);
        $this->getEntityManager()->flush();
    }

    public function findById($id){
        return $this->find($id);
    }

    public function findAll()
    {
        return new ArrayCollection(parent::findAll());
    }
}