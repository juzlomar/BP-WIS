<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Transfer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Transport;
use Doctrine\ORM\UnitOfWork;

class TransportRepository extends EntityRepository
{
    public function save(Transport $transport){
        $this->getEntityManager()->persist($transport);
        $this->getEntityManager()->flush();
    }

    public function remove(Transport $transport){
        $this->getEntityManager()->remove($transport);
        $this->getEntityManager()->flush();
    }

    public function findById($id){
        $query = $this->getEntityManager()->createQuery(
            'SELECT t, b, m, p
             FROM AppBundle\Entity\Transport t
             LEFT JOIN t.materials b
             LEFT JOIN b.materials m
             LEFT JOIN m.parts p
             WHERE t.id = :id'
        );
        $query->setParameter('id', $id);
        return $query->getSingleResult();
    }

    public function findAll()
    {
        return new ArrayCollection(parent::findAll());
    }

    public function findBringing(){
        $query = $this->getEntityManager()->createQuery(
            'SELECT t
             FROM AppBundle\Entity\Bringing t');
        return new ArrayCollection($query->getResult());
    }

    public function findTransfer(){
        $query = $this->getEntityManager()->createQuery(
            'SELECT t
             FROM AppBundle\Entity\Transfer t'
        );
        return new ArrayCollection($query->getResult());
    }

    public function findInternalTransport(){
        $query = $this->getEntityManager()->createQuery(
            'SELECT t
             FROM AppBundle\Entity\InternalTransport t'
        );
        return new ArrayCollection($query->getResult());
    }

}