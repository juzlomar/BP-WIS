<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\Repository;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\BusinessPartner;

class BusinessPartnerRepository extends EntityRepository
{
    public function save(BusinessPartner $businessPartner){
        $this->getEntityManager()->persist($businessPartner);
        $this->getEntityManager()->flush();
    }

    public function remove(BusinessPartner $businessPartner){
        $this->getEntityManager()->remove($businessPartner);
        $this->getEntityManager()->flush();
    }

    public function findById($id){
        return $this->find($id);
    }

    public function findAll()
    {
        return new ArrayCollection(parent::findAll());
    }
}