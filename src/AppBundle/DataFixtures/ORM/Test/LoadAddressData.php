<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */


namespace AppBundle\DataFixtures\ORM\Test;


use AppBundle\Entity\Address;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadAddressData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $address1 = new Address();
        $address1->setTown('Mesto');
        $address1->setPsc('12345');
        $address1->setCp('12');
        $address1->setStreet('Ulice');

        $address2 = new Address();
        $address2->setTown('Mesto');
        $address2->setPsc('12345');
        $address2->setCp('12');
        $address2->setStreet('Ulice');

        $address3 = new Address();
        $address3->setTown('Mesto');
        $address3->setPsc('12345');
        $address3->setCp('12');
        $address3->setStreet('Ulice');
        $address4 = new Address();
        $address4->setTown('Mesto');
        $address4->setPsc('12345');
        $address4->setCp('12');
        $address4->setStreet('Ulice');


        $manager->persist($address1);
        $manager->persist($address2);
        $manager->persist($address3);
        $manager->persist($address4);
        $manager->flush();

        $this->addReference('address1', $address1);
        $this->addReference('address2', $address2);
        $this->addReference('address3', $address3);
        $this->addReference('address4', $address4);
    }

    public function getOrder()
    {
        return 4;
    }

}