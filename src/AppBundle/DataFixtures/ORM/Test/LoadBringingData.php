<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */


namespace AppBundle\DataFixtures\ORM\Test;


use AppBundle\Entity\Bringing;
use AppBundle\Entity\Business;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadBringingData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $transport = new Bringing();
        $transport->setPrice('1000');
        $transport->setDate(new \DateTime('2015-01-01'));
        $business = new Business();
        $business->setMaterialPrice('123');
        $business->addMaterial($this->getReference('material-preprava'));
        $transport->setFrom($this->getReference('Dura Blatna'));
        $transport->setTo($this->getReference('Praha'));
        $transport->addMaterial($business);

        $manager->persist($business);
        $manager->persist($transport);
        $manager->flush();
    }

    public function getOrder()
    {
        return 8;
    }

}