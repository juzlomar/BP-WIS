<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\DataFixtures\ORM\Test;


use AppBundle\Entity\Warehouse;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadWarehouseData extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $warehouse1 = new Warehouse();
        $warehouse1->setId(1);
        $warehouse1->setName('Plzen');
        $warehouse1->setAddress($this->getReference('address1'));
        $warehouse1->addWayOfProcessing($this->getReference('processing-drceni'));
        $warehouse1->addWayOfProcessing($this->getReference('processing-regranulace'));
        $warehouse1->setIsActive(true);
        $warehouse2 = new Warehouse();
        $warehouse2->setId(2);
        $warehouse2->setName('Praha - Letnany');
        $warehouse2->setAddress($this->getReference('address2'));
        $warehouse2->addWayOfProcessing($this->getReference('processing-drceni'));
        $warehouse2->setIsActive(true);
        $manager->persist($warehouse1);
        $manager->persist($warehouse2);
        $manager->flush();
        $this->addReference('Plzen', $warehouse1);
        $this->addReference('Praha', $warehouse2);
    }

    public function getOrder()
    {
       return 5;
    }
}