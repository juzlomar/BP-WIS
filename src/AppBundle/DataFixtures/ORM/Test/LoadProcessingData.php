<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\DataFixtures\ORM\Test;


use AppBundle\Entity\PartProcessing;
use AppBundle\Entity\Processing;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadProcessingData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $processing = new Processing();
        $processing->setDate(new \DateTime('2015-01-01'));
        $processing->setWarehouse($this->getReference('Plzen'));
        $processing->setWayOfProcessing($this->getReference('processing-regranulace'));
        $processing->setProcessor($this->getReference('marketa'));

        $part = new PartProcessing();
        $part->setPrice(10);
        $part->setPart($this->getReference('part-zpracovani-nezpracovano'));
        $part->setProcessing($processing);
        $this->getReference('part-zpracovani-nezpracovano')->setInputOfProcessing($part);
        $this->getReference('part-zpracovani-zpracovano')->setOutputOfProcessing($processing);

        $processing->addInput($part);
        $processing->addOutput($this->getReference('part-zpracovani-zpracovano'));

        $manager->persist($processing);
        $manager->persist($part);
        $manager->flush();
    }

    public function getOrder()
    {
        return 12;
    }

}