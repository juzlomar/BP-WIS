<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\DataFixtures\ORM\Test;


use AppBundle\Entity\PartProcessing;
use AppBundle\Entity\Processing;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadShowProcessingData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $processing1 = new Processing();
        $processing1->setDate(new \DateTime('2015-01-02'));
        $processing1->setWarehouse($this->getReference('Praha'));
        $processing1->setWayOfProcessing($this->getReference('processing-drceni'));
        $processing1->setProcessor($this->getReference('marketa'));

        $part1 = new PartProcessing();
        $part1->setPrice(10);
        $part1->setPart($this->getReference('part-show-1'));
        $part1->setProcessing($processing1);
        $this->getReference('part-show-1')->setInputOfProcessing($part1);
        $processing1->addInput($part1);
        $processing1->addOutput($this->getReference('part-show-2'));
        $this->getReference('part-show-2')->setOutputOfProcessing($processing1);
        $this->addReference('processing-show-1', $processing1);

        $processing2 = new Processing();
        $processing2->setDate(new \DateTime('2015-01-04'));
        $processing2->setWarehouse($this->getReference('Plzen'));
        $processing2->setWayOfProcessing($this->getReference('processing-regranulace'));
        $processing2->setProcessor($this->getReference('tomas'));

        $part2 = new PartProcessing();
        $part2->setPrice(10);
        $part2->setPart($this->getReference('part-show-2'));
        $part2->setProcessing($processing2);
        $this->getReference('part-show-2')->setInputOfProcessing($part2);
        $processing2->addInput($part2);
        $processing2->addOutput($this->getReference('part-show-3'));
        $this->getReference('part-show-3')->setOutputOfProcessing($processing2);
        $this->addReference('processing-show-2', $processing2);

        $manager->persist($processing1);
        $manager->persist($part1);
        $manager->persist($processing2);
        $manager->persist($part2);
        $manager->flush();

    }

    public function getOrder()
    {
        return 14;
    }

}