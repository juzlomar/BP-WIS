<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\DataFixtures\ORM\Test;


use AppBundle\Entity\Material;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadMaterialData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $material1 = new Material();
        $material1->setPlastics('PP+PE');
        $material1->setSpecification('Bedynky');
        $material1->setSpecificationEng('Boxes');
        $material1->setColor('cerna');
        $material1->setColorEng('black');
        $material1->setFinalForm($this->getReference('form-drt'));
        $material1->setWarehouse($this->getReference('Plzen'));
        $this->addReference('material-divide', $material1);

        $material2 = new Material();
        $material2->setPlastics('PP');
        $material2->setSpecification('Bedynky');
        $material2->setSpecificationEng('Boxes');
        $material2->setColor('cerna');
        $material2->setColorEng('black');
        $material2->setFinalForm($this->getReference('form-regranulat'));
        $material2->setWarehouse($this->getReference('Plzen'));
        $this->addReference('material-zastaveno', $material2);

        $material3 = new Material();
        $material3->setPlastics('PP');
        $material3->setSpecification('Bedynky');
        $material3->setSpecificationEng('Boxes');
        $material3->setColor('cerna');
        $material3->setColorEng('black');
        $material3->setFinalForm($this->getReference('form-drt'));
        $material3->setWarehouse($this->getReference('Plzen'));
        $this->addReference('material-preprava', $material3);

        $material4 = new Material();
        $material4->setPlastics('PP');
        $material4->setSpecification('Bedynky');
        $material4->setSpecificationEng('Boxes');
        $material4->setColor('cerna');
        $material4->setColorEng('black');
        $material4->setFinalForm($this->getReference('form-regranulat'));
        $material4->setWarehouse($this->getReference('Plzen'));
        $this->addReference('material-zpracovani1', $material4);

        $material5 = new Material();
        $material5->setPlastics('PP');
        $material5->setSpecification('Bedynky');
        $material5->setSpecificationEng('Boxes');
        $material5->setColor('mix');
        $material5->setColorEng('mix');
        $material5->setFinalForm($this->getReference('form-regranulat'));
        $material5->setWarehouse($this->getReference('Plzen'));
        $this->addReference('material-zpracovani2', $material5);

        $manager->persist($material1);
        $manager->persist($material2);
        $manager->persist($material3);
        $manager->persist($material4);
        $manager->persist($material5);
        $manager->flush();
    }

    public function getOrder()
    {
        return 6;
    }
}