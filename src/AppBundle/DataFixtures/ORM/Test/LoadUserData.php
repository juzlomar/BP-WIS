<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\DataFixtures\ORM\Test;


use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function load(ObjectManager $manager)
    {
        $user1 = new User();
        $user1->setName('Marketa Juzlova');
        $user1->setUsername('juzlomar');
        $user1->setEmail('marketa.juzlova@seznam.cz');
        $user1->setEnabled(true);
        $encoder = $this->container->get('security.password_encoder');
        $password = 'heslo';
        $encoded = $encoder->encodePassword($user1, $password);

        $user1->setPassword($encoded);
      //  $user1->setPassword('heslo');
        $user1->setPhoneNumber('123456789');
        $user1->addRole('ROLE_ADMIN');
        $this->setReference('marketa', $user1);

        $user2 = new User();
        $user2->setName('Tomas Simandl');
        $user2->setUsername('simandl');
        $user2->setEmail('tomas.simandl@bp.com');
        $user2->setEnabled(true);
        $encoded = $encoder->encodePassword($user2, $password);
        $user2->setPassword($encoded);
        $user2->setPhoneNumber('123456789');
        $user2->addRole('ROLE_WAREHOUSEMAN');
        $this->setReference('tomas', $user2);

        $user3 = new User();
        $user3->setName('Tareza');
        $user3->setUsername('tereza');
        $user3->setEmail('tereza@bp.com');
        $user3->setEnabled(false);
        $encoded = $encoder->encodePassword($user3, $password);
        $user3->setPassword($encoded);
        $user3->setPhoneNumber('123456789');
        $user3->addRole('ROLE_WAREHOUSEMAN');
        $this->setReference('tereza', $user3);

        $manager->persist($user1);
        $manager->persist($user2);
        $manager->persist($user3);
        $manager->flush();
    }

    public function getOrder()
    {
        return 11;
    }


    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}