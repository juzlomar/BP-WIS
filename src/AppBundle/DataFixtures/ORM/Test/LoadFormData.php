<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\DataFixtures\ORM\Test;


use AppBundle\Entity\Form;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadFormData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $form1 = new Form();
        $form1->setTitle('Drt');
        $form1->setTitleEng('Drt_eng');
        $form1->addFinalForm($form1);

        $form2 = new Form();
        $form2->setTitle('Regranulat');
        $form2->setTitleEng('Regranulat_eng');
        $form2->addFinalForm($form2);

        $form3 = new Form();
        $form3->setTitle('Kusovka');
        $form3->setTitleEng('Kusovka_eng');
        $form3->addFinalForm($form3);

        $form1->addFinalForm($form2);
        $form3->addFinalForm($form1);
        $form3->addFinalForm($form2);

        $form1->addWayOfStoring($this->getReference('storing-bag'));
        $form2->addWayOfStoring($this->getReference('storing-bag'));
        $form3->addWayOfStoring($this->getReference('storing-volne'));
        $form3->addWayOfStoring($this->getReference('storing-bag'));

        $manager->persist($form1);
        $manager->persist($form2);
        $manager->persist($form3);
        $manager->flush();

        $this->addReference('form-drt', $form1);
        $this->addReference('form-regranulat', $form2);
        $this->addReference('form-kusovka', $form3);

    }

    public function getOrder()
    {
        return 2;
    }

}