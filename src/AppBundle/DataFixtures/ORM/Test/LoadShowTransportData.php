<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\DataFixtures\ORM\Test;


use AppBundle\Entity\Bringing;
use AppBundle\Entity\Business;
use AppBundle\Entity\InternalTransport;
use AppBundle\Entity\Transfer;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadShowTransportData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $transport1 = new Bringing();
        $transport1->setPrice('1000');
        $transport1->setDate(new \DateTime('2015-01-01'));
        $business1 = new Business();
        $business1->setMaterialPrice('123');
        $business1->addMaterial($this->getReference('material-show'));
        $transport1->setFrom($this->getReference('Dura Blatna'));
        $transport1->setTo($this->getReference('Praha'));
        $transport1->addMaterial($business1);
        $this->addReference('bringing-show', $transport1);

        $transport2 = new InternalTransport();
        $transport2->setPrice('1000');
        $transport2->setDate(new \DateTime('2015-01-03'));
        $business2 = new Business();
        $business2->setMaterialPrice('123');
        $business2->addMaterial($this->getReference('material-show'));
        $transport2->setFrom($this->getReference('Praha'));
        $transport2->setTo($this->getReference('Plzen'));
        $transport2->addMaterial($business2);
        $this->addReference('internal-show', $transport2);

        $transport3 = new Transfer();
        $transport3->setPrice('1000');
        $transport3->setDate(new \DateTime('2015-01-05'));
        $business3 = new Business();
        $business3->setMaterialPrice('123');
        $business3->addMaterial($this->getReference('material-show'));
        $transport3->setFrom($this->getReference('Plzen'));
        $transport3->setTo($this->getReference('Partner'));
        $transport3->addMaterial($business3);
        $this->addReference('transfer-show', $transport3);

        $manager->persist($business1);
        $manager->persist($transport1);
        $manager->persist($business2);
        $manager->persist($transport2);
        $manager->persist($business3);
        $manager->persist($transport3);
        $manager->flush();
    }

    public function getOrder()
    {
       return 15;
    }

}