<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\DataFixtures\ORM\Test;


use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialPart;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadShowMaterialData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $material1 = new Material();
        $material1->setPlastics('PP+PE');
        $material1->setSpecification('Bedynky');
        $material1->setSpecificationEng('Boxes');
        $material1->setColor('cerna');
        $material1->setColorEng('black');
        $material1->setFinalForm($this->getReference('form-regranulat'));
        $material1->setWarehouse($this->getReference('Plzen'));
        $this->addReference('material-show', $material1);

        $part1 = new MaterialPart();
        $part1->setNumber(1);
        $part1->setWeightNetto(800);
        $part1->setWeightBrutto(821);
        $part1->setCurrentForm($this->getReference('form-kusovka'));
        $part1->setWayOfStoring($this->getReference('storing-volne'));
        $part1->setState('zpracováno');
        $part1->setMaterial($this->getReference('material-show'));
        $this->addReference('part-show-1', $part1);

        $part2 = new MaterialPart();
        $part2->setNumber(1);
        $part2->setWeightNetto(800);
        $part2->setWeightBrutto(821);
        $part2->setCurrentForm($this->getReference('form-drt'));
        $part2->setWayOfStoring($this->getReference('storing-bag'));
        $part2->setState('zpracováno');
        $part2->setMaterial($this->getReference('material-show'));
        $this->addReference('part-show-2', $part2);

        $part3 = new MaterialPart();
        $part3->setNumber(1);
        $part3->setWeightNetto(800);
        $part3->setWeightBrutto(821);
        $part3->setCurrentForm($this->getReference('form-regranulat'));
        $part3->setWayOfStoring($this->getReference('storing-bag'));
        $part3->setState('odvezeno');
        $part3->setMaterial($this->getReference('material-show'));
        $this->addReference('part-show-3', $part3);

        $manager->persist($material1);
        $manager->persist($part1);
        $manager->persist($part2);
        $manager->persist($part3);
        $manager->flush();
    }

    public function getOrder()
    {
        return 13;
    }

}