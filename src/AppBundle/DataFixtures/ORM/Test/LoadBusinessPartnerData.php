<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */


namespace AppBundle\DataFixtures\ORM\Test;


use AppBundle\Entity\BusinessPartner;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadBusinessPartnerData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $partner1 = new BusinessPartner();
        $partner1->setName('Dura Blatna');
        $partner1->setAddress($this->getReference('address3'));
        $this->addReference('Dura Blatna', $partner1);
        $partner2 = new BusinessPartner();
        $partner2->setName('Partner');
        $partner2->setAddress($this->getReference('address4'));
        $this->addReference('Partner', $partner2);

        $manager->persist($partner1);
        $manager->persist($partner2);
        $manager->flush();
    }

    public function getOrder()
    {
       return 6;
    }

}