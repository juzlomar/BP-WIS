<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\DataFixtures\ORM\Test;


use AppBundle\Entity\MaterialPart;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPartData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $part1md = new MaterialPart();
        $part1md->setNumber(1);
        $part1md->setWeightNetto(800);
        $part1md->setWeightBrutto(821);
        $part1md->setCurrentForm($this->getReference('form-drt'));
        $part1md->setWayOfStoring($this->getReference('storing-bag'));
        $part1md->setState('připraveno k expedici');
        $part1md->setMaterial($this->getReference('material-divide'));
        $this->addReference('part-divide-expedice', $part1md);


        $part2md = new MaterialPart();
        $part2md->setNumber(1);
        $part2md->setWeightNetto(800);
        $part2md->setWeightBrutto(821);
        $part2md->setCurrentForm($this->getReference('form-drt'));
        $part2md->setWayOfStoring($this->getReference('storing-bag'));
        $part2md->setState('připraveno ke zpracování');
        $part2md->setMaterial($this->getReference('material-divide'));
        $this->addReference('part-divide-zpracovani', $part2md);

        $part1mz = new MaterialPart();
        $part1mz->setNumber(1);
        $part1mz->setWeightNetto(800);
        $part1mz->setWeightBrutto(821);
        $part1mz->setCurrentForm($this->getReference('form-drt'));
        $part1mz->setWayOfStoring($this->getReference('storing-bag'));
        $part1mz->setState('zastaveno');
        $part1mz->setMaterial($this->getReference('material-zastaveno'));
        $this->addReference('part-zastaveno', $part1mz);

        $part1mp = new MaterialPart();
        $part1mp->setNumber(1);
        $part1mp->setWeightNetto(800);
        $part1mp->setWeightBrutto(821);
        $part1mp->setCurrentForm($this->getReference('form-drt'));
        $part1mp->setWayOfStoring($this->getReference('storing-bag'));
        $part1mp->setState('připraveno k expedici');
        $part1mp->setMaterial($this->getReference('material-preprava'));
        $this->addReference('part-preprava', $part1mp);

        $part1mzp = new MaterialPart();
        $part1mzp->setNumber(1);
        $part1mzp->setWeightNetto(800);
        $part1mzp->setWeightBrutto(821);
        $part1mzp->setCurrentForm($this->getReference('form-drt'));
        $part1mzp->setWayOfStoring($this->getReference('storing-bag'));
        $part1mzp->setState('připraveno ke zpracování');
        $part1mzp->setMaterial($this->getReference('material-zpracovani1'));
        $this->addReference('part-zpracovani', $part1mzp);

        $part1mzp2 = new MaterialPart();
        $part1mzp2->setNumber(1);
        $part1mzp2->setWeightNetto(800);
        $part1mzp2->setWeightBrutto(821);
        $part1mzp2->setCurrentForm($this->getReference('form-drt'));
        $part1mzp2->setWayOfStoring($this->getReference('storing-bag'));
        $part1mzp2->setState('připraveno ke zpracování');
        $part1mzp2->setMaterial($this->getReference('material-zpracovani2'));
        $this->addReference('part-zpracovani-nezpracovano', $part1mzp2);

        $part2mzp2 = new MaterialPart();
        $part2mzp2->setNumber(1);
        $part2mzp2->setWeightNetto(800);
        $part2mzp2->setWeightBrutto(821);
        $part2mzp2->setCurrentForm($this->getReference('form-regranulat'));
        $part2mzp2->setWayOfStoring($this->getReference('storing-bag'));
        $part2mzp2->setState('zpracováno');
        $part2mzp2->setMaterial($this->getReference('material-zpracovani2'));
        $this->addReference('part-zpracovani-zpracovano', $part2mzp2);


        $manager->persist($part1md);
        $manager->persist($part2md);
        $manager->persist($part1mz);
        $manager->persist($part1mp);
        $manager->persist($part1mzp);
        $manager->persist($part1mzp2);
        $manager->persist($part2mzp2);
        $manager->flush();

    }

    public function getOrder()
    {
        return 7;
    }

}