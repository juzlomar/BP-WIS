<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\DataFixtures\ORM\Init;


use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData extends AbstractFixture implements ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function load(ObjectManager $manager)
    {
        $user1 = new User();
        $user1->setName('Admin');
        $user1->setUsername('admin');
        $user1->setEmail('admin@admin.com');
        $user1->setEnabled(true);
        $encoder = $this->container->get('security.password_encoder');
        $password = 'password';
        $encoded = $encoder->encodePassword($user1, $password);

        $user1->setPassword($encoded);
        $user1->setPhoneNumber('123456789');
        $user1->addRole('ROLE_ADMIN');

        $manager->persist($user1);
        $manager->flush();
    }


    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}