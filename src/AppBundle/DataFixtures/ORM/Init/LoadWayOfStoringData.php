<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\DataFixtures\ORM\Init;

use AppBundle\Entity\WayOfStoring;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadWayOfStoringData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $way1 = new WayOfStoring();
        $way1->setTitle('V bagu');

        $way2 = new WayOfStoring();
        $way2->setTitle('Volně');

        $way3 = new WayOfStoring();
        $way3->setTitle('V balíku');

        $way4 = new WayOfStoring();
        $way4->setTitle('V oktabínu');

        $manager->persist($way1);
        $manager->persist($way2);
        $manager->persist($way3);
        $manager->persist($way4);
        $manager->flush();

        $this->addReference('storing-bag', $way1);
        $this->addReference('storing-volne', $way2);
        $this->addReference('storing-balik', $way3);
        $this->addReference('storing-oktabin', $way4);
    }

    public function getOrder()
    {
        return 1;
    }

}