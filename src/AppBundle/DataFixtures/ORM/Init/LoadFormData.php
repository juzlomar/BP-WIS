<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\DataFixtures\ORM\Init;

use AppBundle\Entity\Form;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadFormData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $form2 = new Form();
        $form2->setTitle('Regranulát');
        $form2->setTitleEng('Granulate');
        $form2->addFinalForm($form2);
        $form2->addWayOfStoring($this->getReference('storing-bag'));

        $form3 = new Form();
        $form3->setTitle('Drť');
        $form3->setTitleEng('Brash');
        $form3->addFinalForm($form3);
        $form3->addFinalForm($form2);
        $form3->addWayOfStoring($this->getReference('storing-bag'));

        $form4 = new Form();
        $form4->setTitle('Kusovka');
        $form4->setTitleEng('Unit loads');
        $form4->addFinalForm($form4);
        $form4->addFinalForm($form3);
        $form4->addFinalForm($form2);
        $form4->addWayOfStoring($this->getReference('storing-volne'));
        $form4->addWayOfStoring($this->getReference('storing-oktabin'));

        $form5 = new Form();
        $form5->setTitle('Balík');
        $form5->setTitleEng('Package');
        $form5->addFinalForm($form5);
        $form5->addFinalForm($form2);
        $form5->addWayOfStoring($this->getReference('storing-balik'));

        $form6 = new Form();
        $form6->setTitle('Na volno');
        $form6->setTitleEng('On the loose');
        $form6->addFinalForm($form6);
        $form6->addFinalForm($form5);
        $form6->addFinalForm($form2);
        $form6->addWayOfStoring($this->getReference('storing-volne'));

        $manager->persist($form2);
        $manager->persist($form3);
        $manager->persist($form4);
        $manager->persist($form5);
        $manager->persist($form6);
        $manager->flush();

        $this->addReference('form-regranulat', $form2);
        $this->addReference('form-drt', $form3);
        $this->addReference('form-kusovka', $form4);
        $this->addReference('form-balik', $form5);
        $this->addReference('form-volne', $form6);

    }

    public function getOrder()
    {
        return 2;
    }

}