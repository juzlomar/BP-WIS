<?php
/**
 * This file is part of the BP-WIS package
 *
 * (c) Markéta Jůzlová <marketa.juzlova@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace AppBundle\DataFixtures\ORM\Init;


use AppBundle\Entity\WayOfProcessing;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadWayOfProcessingData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $way1 = new WayOfProcessing();
        $way1->setTitle('Drcení');
        $way1->addEntranceForm($this->getReference('form-kusovka'));
        $way1->addEntranceForm($this->getReference('form-balik'));
        $way1->setOutputForm($this->getReference('form-drt'));

        $way2 = new WayOfProcessing();
        $way2->setTitle('Regranulace');
        $way2->addEntranceForm($this->getReference('form-drt'));
        $way2->setOutputForm($this->getReference('form-regranulat'));

        $way3 = new WayOfProcessing();
        $way3->setTitle('Lisování');
        $way3->addEntranceForm($this->getReference('form-volne'));
        $way3->setOutputForm($this->getReference('form-balik'));

        $manager->persist($way1);
        $manager->persist($way2);
        $manager->persist($way3);
        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }

}