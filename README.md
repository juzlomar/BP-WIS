BP-WIS
======

Waste Web Application Management created as part of bachelor thesis at Czech Technical University in Prague.

This application is under MIT licence. See the complete license in LICENCE.